// describe("<name or title for the spec suite>", function() {
// 	beforeEach(function() {
// 		//gets executed once before each spec in the describe 
// 	});
	
// 	afterEach(function() {
// 		//gets executed once after each spec.
// 	});
	
// 	it("<title of the test case/spec>", function() {
// 		//write you automation code here
// 	});
				  
// });


describe("EmailSuccessValidation", function() {
  
  var originalTimeout;
  beforeEach(function() {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000; 
  });
	
  afterEach(function() {
	jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });
	
  it("Correct email", function() {
	expect(validateRegexEmail("fjkm@jkm.com")).toBeTruthy();
  });
  
  it("Uncorrect email", function() {
	expect(validateRegexEmail("fjkmjkm.com")).toBeFalsy();
  });
				  
});

