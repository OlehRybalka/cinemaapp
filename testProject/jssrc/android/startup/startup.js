//startup.js
var globalhttpheaders = {};
var appConfig = {
    appId: "testProject",
    appName: "testProject",
    appVersion: "1.0.0",
    isturlbase: "https://app-factory-dev.konycloud.com/services",
    isDebug: true,
    isMFApp: true,
    appKey: "363e4961ff73a7916627f873535d2c33",
    appSecret: "a26f5161166701243b5270ccc4dccfd8",
    serviceUrl: "https://100007931.auth.konycloud.com/appconfig",
    svcDoc: {
        "identity_meta": {
            "reusableFacebookLogin2": {
                "success_url": "allow_any"
            },
            "googleAuth": {
                "success_url": "allow_any"
            },
            "CinemaIdentityFacebook": {
                "success_url": "allow_any"
            },
            "reusableLinkedinLogin": {
                "success_url": "allow_any"
            },
            "reusableOffice365Login": {
                "success_url": "allow_any"
            }
        },
        "app_version": "1.0",
        "baseId": "0f41e0d2-63c5-424f-9ecc-cc570d38c0a6",
        "app_default_version": "1.0",
        "login": [{
            "alias": "userstore",
            "type": "basic",
            "prov": "userstore",
            "url": "https://100007931.auth.konycloud.com"
        }, {
            "alias": "reusableOffice365Login",
            "type": "oauth2",
            "prov": "reusableOffice365Login",
            "url": "https://100007931.auth.konycloud.com"
        }, {
            "alias": "reusableFacebookLogin2",
            "type": "oauth2",
            "prov": "reusableFacebookLogin2",
            "url": "https://100007931.auth.konycloud.com"
        }, {
            "alias": "googleAuth",
            "type": "oauth2",
            "prov": "googleAuth",
            "url": "https://100007931.auth.konycloud.com"
        }, {
            "alias": "CinemaIdentityFacebook",
            "type": "oauth2",
            "prov": "CinemaIdentityFacebook",
            "url": "https://100007931.auth.konycloud.com"
        }, {
            "alias": "reusableLinkedinLogin",
            "type": "oauth2",
            "prov": "reusableLinkedinLogin",
            "url": "https://100007931.auth.konycloud.com"
        }],
        "services_meta": {
            "connectDb": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://app-factory-dev.konycloud.com/services/connectDb"
            },
            "myCinemaTest": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://app-factory-dev.konycloud.com/services/myCinemaTest"
            },
            "GetFilmsInfo": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://app-factory-dev.konycloud.com/services/GetFilmsInfo"
            },
            "CinemaApp": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://app-factory-dev.konycloud.com/services/CinemaApp"
            },
            "getMovieInfo": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://app-factory-dev.konycloud.com/services/getMovieInfo"
            },
            "getCinemas": {
                "type": "integsvc",
                "version": "1.0",
                "url": "https://app-factory-dev.konycloud.com/services/getCinemas"
            }
        },
        "selflink": "https://100007931.auth.konycloud.com/appconfig",
        "integsvc": {
            "connectDb": "https://app-factory-dev.konycloud.com/services/connectDb",
            "myCinemaTest": "https://app-factory-dev.konycloud.com/services/myCinemaTest",
            "GetFilmsInfo": "https://app-factory-dev.konycloud.com/services/GetFilmsInfo",
            "CinemaApp": "https://app-factory-dev.konycloud.com/services/CinemaApp",
            "getMovieInfo": "https://app-factory-dev.konycloud.com/services/getMovieInfo",
            "getCinemas": "https://app-factory-dev.konycloud.com/services/getCinemas"
        },
        "service_doc_etag": "000001673D499DF0",
        "appId": "8995ae1e-d642-4a93-8651-6db12beb37f1",
        "identity_features": {
            "reporting_params_header_allowed": true
        },
        "name": "myCinemaTest",
        "reportingsvc": {
            "session": "https://app-factory-dev.konycloud.com/services/IST",
            "custom": "https://app-factory-dev.konycloud.com/services/CMS"
        }
    },
    runtimeAppVersion: "1.0",
    eventTypes: ["FormEntry", "Error", "Crash"],
    testAutomationURL: "http://10.4.169.121:8888/testresources/testProject/",
};
sessionID = "";

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        marginsIncludedInWidgetContainerWeight: true,
        isMVC: true,
        APILevel: 8200
    })
};

function themeCallBack() {
    initializeGlobalVariables();
    applicationController = require("applicationController");
    callAppMenu();
    kony.application.setApplicationInitializationEvents({
        init: applicationController.appInit,
        postappinit: applicationController.postAppInitCallBack,
        showstartupform: function() {
            new kony.mvc.Navigation("frmStart").navigate();
        }
    });
};

function loadResources() {
    globalhttpheaders = {};
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_KonyLogger"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_binarydata"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.ND_binary_util"
    });
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "eventTypes": appConfig.eventTypes,
        "serviceUrl": appConfig.serviceUrl
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}
kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
//This is the entry point for the application.When Locale comes,Local API call will be the entry point.
loadResources();
debugger;