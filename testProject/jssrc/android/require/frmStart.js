define("frmStart", function() {
    return function(controller) {
        function addWidgetsfrmStart() {
            this.setDefaultUnit(kony.flex.DP);
            var flxStart = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "450dp",
                "id": "flxStart",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxStart.setDefaultUnit(kony.flex.DP);
            var txtLogo = new kony.ui.RichText({
                "centerX": "50%",
                "height": "100dp",
                "id": "txtLogo",
                "isVisible": true,
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0ic3e733e52c445",
                "text": "Cinema\n\n",
                "width": "350dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtTagLine = new kony.ui.RichText({
                "centerX": "50%",
                "height": "77dp",
                "id": "txtTagLine",
                "isVisible": true,
                "left": "0dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0aed759b9827145",
                "text": "Let's go to the cinema!",
                "top": "110dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "110dp",
                "width": "75%",
                "zIndex": 1
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var btnSignUp = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btnSignUp",
                "isVisible": true,
                "left": "0dp",
                "onClick": controller.AS_Button_fe70ee30eef64ef7a690f6d762a4f7a0,
                "skin": "CopydefBtnNormal0a8580edbe90140",
                "text": "SIGN UP",
                "top": "0dp",
                "width": "49%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnSignIn = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btnSignIn",
                "isVisible": true,
                "onClick": controller.AS_Button_e4f599c628fd4eb3ba252956b2b50c8b,
                "right": 0,
                "skin": "CopydefBtnNormal0d8b289e539774a",
                "text": "SING IN",
                "top": 0,
                "width": "49%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxButtons.add(btnSignUp, btnSignIn);
            flxStart.add(txtLogo, txtTagLine, flxButtons);
            this.add(flxStart);
        };
        return [{
            "addWidgets": addWidgetsfrmStart,
            "enabledForIdleTimeout": false,
            "id": "frmStart",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0ga83c868cb3f47"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});