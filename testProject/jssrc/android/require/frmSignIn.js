define("frmSignIn", function() {
    return function(controller) {
        function addWidgetsfrmSignIn() {
            this.setDefaultUnit(kony.flex.DP);
            var flxLogin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "55%",
                "clipBounds": true,
                "height": "480dp",
                "id": "flxLogin",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "55dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLogin.setDefaultUnit(kony.flex.DP);
            var txtSignIn = new kony.ui.RichText({
                "centerX": "50.00%",
                "height": "30dp",
                "id": "txtSignIn",
                "isVisible": true,
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0e7b69feb74d940",
                "text": "SIGN IN",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtEnterData = new kony.ui.RichText({
                "centerX": "50.90%",
                "height": "23dp",
                "id": "txtEnterData",
                "isVisible": true,
                "left": "123dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0bfc7eff440604f",
                "text": "enter your data",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFields = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "135dp",
                "id": "flxFields",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "12dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ccbc14ef40854b",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFields.setDefaultUnit(kony.flex.DP);
            var fldEmail = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50.00%",
                "focusSkin": "CopydefTextBoxFocus0c031bbc4fdb545",
                "height": "40dp",
                "id": "fldEmail",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
                "left": "71dp",
                "placeholder": "E-mail",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0fa9137e1fa5c4b",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "277dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoFilter": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
                "placeholderSkin": "CopydefTextBoxPlaceholder0ed1eb79940224b",
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var lblEmailWarning = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblEmailWarning",
                "isVisible": true,
                "skin": "lblWarningSkn",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "1dp",
                "width": "270dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var fldPassword = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "49.97%",
                "focusSkin": "CopydefTextBoxFocus0bf8ad9c7cde94a",
                "height": "40dp",
                "id": "fldPassword",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "75dp",
                "placeholder": "Password",
                "secureTextEntry": true,
                "skin": "CopydefTextBoxNormal0jf097d2ae4494f",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "5dp",
                "width": "277dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoFilter": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
                "placeholderSkin": "defTextBoxPlaceholder",
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var lblCommonWarning = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblCommonWarning",
                "isVisible": true,
                "skin": "lblWarningSkn",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "1dp",
                "width": "270dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            flxFields.add(fldEmail, lblEmailWarning, fldPassword, lblCommonWarning);
            var flxRememberMe = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50.03%",
                "clipBounds": true,
                "height": "31dp",
                "id": "flxRememberMe",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0cadfc1d2c1f147",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRememberMe.setDefaultUnit(kony.flex.DP);
            var chkRememberMe = new kony.ui.CheckBoxGroup({
                "height": "35dp",
                "id": "chkRememberMe",
                "isVisible": true,
                "left": "69dp",
                "masterData": [
                    ["cbg1", "Remember me"]
                ],
                "selectedKeyValues": [
                    ["cbg1", "Remember me"]
                ],
                "selectedKeys": ["cbg1"],
                "skin": "CopyslCheckBoxGroup0c977d52f5a404a",
                "top": "0dp",
                "width": "36.11%",
                "zIndex": 1
            }, {
                "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtForgtPassword = new kony.ui.RichText({
                "height": "27dp",
                "id": "txtForgtPassword",
                "isVisible": true,
                "left": "219dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0j5d55759fd0341",
                "text": "Forgot Password?",
                "top": "1dp",
                "width": "30.48%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRememberMe.add(chkRememberMe, txtForgtPassword);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50.03%",
                "clipBounds": true,
                "height": "208dp",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0e2de83b9d66047",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var btnSignIn = new kony.ui.Button({
                "centerX": "50.00%",
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btnSignIn",
                "isVisible": true,
                "left": "69dp",
                "onClick": controller.AS_Button_eae6d7c88ad64342a4c52ab72cff6a1a,
                "skin": "CopydefBtnNormal0f5d5cf40af6e48",
                "text": "SIGN IN",
                "top": "0dp",
                "width": "260dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtOr = new kony.ui.RichText({
                "centerX": "50.00%",
                "height": "27dp",
                "id": "txtOr",
                "isVisible": true,
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0cfdf0ab332a349",
                "text": "OR",
                "top": "5dp",
                "width": "10.19%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnSignGoogle = new kony.ui.Button({
                "centerX": "50.03%",
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btnSignGoogle",
                "isVisible": true,
                "skin": "CopydefBtnNormal0gdb63fc883734f",
                "text": "Sign in with Google",
                "top": "5dp",
                "width": "260dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnSignFacebook = new kony.ui.Button({
                "centerX": "50.06%",
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btnSignFacebook",
                "isVisible": true,
                "skin": "CopydefBtnNormal0hf0cd9cea0014d",
                "text": "Sign in with Facebook",
                "top": "5dp",
                "width": "260dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxButtons.add(btnSignIn, txtOr, btnSignGoogle, btnSignFacebook);
            var flxNewHere = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "27dp",
                "id": "flxNewHere",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNewHere.setDefaultUnit(kony.flex.DP);
            var txtNewHere = new kony.ui.RichText({
                "height": "27dp",
                "id": "txtNewHere",
                "isVisible": true,
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0a97a291741874d",
                "text": "New here?",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 2, 0],
                "paddingInPixel": false
            }, {});
            var txtSingUp = new kony.ui.RichText({
                "height": "26dp",
                "id": "txtSingUp",
                "isVisible": true,
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0b933a00bfcb04f",
                "text": "Sing Up",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNewHere.add(txtNewHere, txtSingUp);
            flxLogin.add(txtSignIn, txtEnterData, flxFields, flxRememberMe, flxButtons, flxNewHere);
            this.add(flxLogin);
        };
        return [{
            "addWidgets": addWidgetsfrmSignIn,
            "enabledForIdleTimeout": false,
            "id": "frmSignIn",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0gc220e9149db4e"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});