define("CopyFBox0fc526bff657f40", function() {
    return function(controller) {
        CopyFBox0fc526bff657f40 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "45%",
            "id": "CopyFBox0fc526bff657f40",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "width": "100%"
        }, {
            "containerWeight": 100
        }, {});
        CopyFBox0fc526bff657f40.setDefaultUnit(kony.flex.DP);
        var txtSubitemTitle = new kony.ui.RichText({
            "height": "133dp",
            "id": "txtSubitemTitle",
            "isVisible": true,
            "left": "125dp",
            "linkSkin": "defRichTextLink",
            "onClick": controller.AS_RichText_h145ba6d231a4d2b9100052b4dda3009,
            "right": "15dp",
            "skin": "tmpTxtSubitemTitle",
            "text": "RichText",
            "top": "0dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "hExpand": true,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        var txtTime = new kony.ui.RichText({
            "height": "105dp",
            "id": "txtTime",
            "isVisible": true,
            "left": "5dp",
            "linkSkin": "defRichTextLink",
            "right": "10dp",
            "skin": "timesSkn",
            "text": "RichText",
            "top": "134dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "hExpand": true,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        var imgSubitem = new kony.ui.Image2({
            "height": "130dp",
            "id": "imgSubitem",
            "isVisible": true,
            "left": "3dp",
            "skin": "slImage",
            "src": "imagedrag.png",
            "top": "3dp",
            "width": "120dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        CopyFBox0fc526bff657f40.add(txtSubitemTitle, txtTime, imgSubitem);
        return CopyFBox0fc526bff657f40;
    }
})