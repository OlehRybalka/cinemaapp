define("userCopyFBox0fc526bff657f40Controller", {
    //Type your controller code here 
});
define("CopyFBox0fc526bff657f40ControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for txtSubitemTitle **/
    AS_RichText_h145ba6d231a4d2b9100052b4dda3009: function AS_RichText_h145ba6d231a4d2b9100052b4dda3009(eventobject, linktext, attributes, context) {
        var self = this;
        return
    }
});
define("CopyFBox0fc526bff657f40Controller", ["userCopyFBox0fc526bff657f40Controller", "CopyFBox0fc526bff657f40ControllerActions"], function() {
    var controller = require("userCopyFBox0fc526bff657f40Controller");
    var controllerActions = ["CopyFBox0fc526bff657f40ControllerActions"];
    for (var i = 0; i < controllerActions.length; i++) {
        var actions = require(controllerActions[i]);
        for (var key in actions) {
            controller[key] = actions[key];
        }
    }
    return controller;
});
