define("userfrmChosenFilmController", {
    //   data : {},
    //   error : {},
    //   onNavigate: function (context) {
    //     if (context) {
    //       this.data.id = context;
    //       var serviceName = "getCinemas";
    //       var getFilm = "getFilmById";
    //       var getActors = "getActorsByFilmId";
    //       var getImagesId = "getFilmImagesId";
    //       var integrationClient = null;
    //       var params = {
    //         filmId: context,
    //         apiKey: "pol1kh111"
    //       };
    //       var imgParams = {
    //         imgId: null,
    //         apiKey: "pol1kh111"
    //       };
    //       try {
    //         integrationClient = kony.sdk.getCurrentInstance().getIntegrationService(serviceName);
    //         integrationClient.invokeOperation(getFilm, null, params, correctFilmResponse, errorResponse);
    //         integrationClient.invokeOperation(getActors, null, params, correctActorsResponse, errorResponse);
    //         integrationClient.invokeOperation(getImagesId, null, params, correctImgIdResponse, errorResponse);
    //       } catch (e) {
    //         alert("Error: " + e.message);
    //       }
    //     }
    //     function correctFilmResponse (response) {
    //       var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
    //       var i;      
    //       frmChosenFilm.view.genreDescription.text = "";
    //       frmChosenFilm.view.filmTitleLabel.text = response.title;   
    //       frmChosenFilm.view.durationDescription.text = response.duration;
    //       frmChosenFilm.view.ratingDescription.text = response.rating;
    //       frmChosenFilm.view.yearDescription.text = response.year;
    //       frmChosenFilm.view.plotDescription.text = response.description;
    //       for (i = 0; i < response.genre.length; i++) {
    //         frmChosenFilm.view.genreDescription.text += response.genre[i] + "\n";
    //       }
    //     }
    //     function correctActorsResponse (response) {
    //       var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
    //       frmChosenFilm.view.actorsDescription.text = "";
    //       var i;
    //       for (i = 0; i < response.persons.length; i++) {
    //         frmChosenFilm.view.actorsDescription.text += response.persons[i].first_name + " " + response.persons[i].last_name + "\n";
    //       }
    //     }
    //     function correctImgIdResponse (response) {
    //       var i;
    //       var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
    //       frmChosenFilm.view.imgSegmentSlider.removeAll();
    //       for(i = 0; i < response.numberOfElements; i++) {
    //         imgParams.imgId = response.content[i].id;
    //         frmChosenFilm.view.imgSegmentSlider.addDataAt({
    //           filmImg: {src: "http://kino-teatr.ua:8081/services/api/film/image/" + imgParams.imgId + "?apiKey=" + imgParams.apiKey + "&width=300&height=400&ratio=1"}
    //         }, i);
    //       }
    //     }
    //     function errorResponse (error) {
    //       this.error.name = "Response Error";
    //       this.error.message = error.message;
    //       alert(this.error.name + " : " + this.error.message);
    //     }   
    //   },
    //   clearView: function () {
    //     var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);   
    //   },
    //   goToScreenings: function () {
    //     var ntf = new kony.mvc.Navigation("frmScreenings");
    // 	ntf.navigate(this.data);
    //   },
    //   goBack: function () {
    //     var ntf = new kony.mvc.Navigation("frmCatalog");
    //     ntf.navigate();
    //   }
    data: {},
    onNavigate: function(context) {
        var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
        frmChosenFilm.view.sgmMenu.onRowClick = HamburgerMenuNavigation;
        frmChosenFilm.view.imgHomebtn.onTouchStart = Hamburger;
        frmChosenFilm.view.ticketsBtn.onClick = this.goToScreenings;
        if (context) {
            this.data.id = context.filmId;
            this.data.cityId = context.cityId;
        }
        try {
            this.clearView();
            getFilmInfo(this.data.id);
            getFilmImages(this.data.id);
            getActors(this.data.id);
        } catch (e) {
            alert("ERROR : " + e.message);
        }
    },
    clearView: function() {
        var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
        frmChosenFilm.view.genreDescription.text = "";
        frmChosenFilm.view.filmTitleLabel.text = "";
        frmChosenFilm.view.durationDescription.text = "";
        frmChosenFilm.view.ratingDescription.text = "";
        frmChosenFilm.view.yearDescription.text = "";
        frmChosenFilm.view.plotDescription.text = "";
        frmChosenFilm.view.actorsDescription.text = "";
        frmChosenFilm.view.imgSegmentFlex.setVisibility(false);
    },
    goToScreenings: function() {
        var ntf = new kony.mvc.Navigation("frmScreenings");
        ntf.navigate(this.data);
    },
    goBack: function() {
        var ntf = new kony.mvc.Navigation("frmCatalog");
        ntf.navigate();
    }
});
define("frmChosenFilmControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for imgbackbtn **/
    AS_Image_a12ffde51ed84df486b6a2b854517d8b: function AS_Image_a12ffde51ed84df486b6a2b854517d8b(eventobject, x, y) {
        var self = this;
        return Hamburger.call(this);
    },
    /** onRowClick defined for sgmMenu **/
    AS_Segment_jd90994789a6466f9837eccd6a4cb62c: function AS_Segment_jd90994789a6466f9837eccd6a4cb62c(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.menuNavigation.call(this);
    }
});
define("frmChosenFilmController", ["userfrmChosenFilmController", "frmChosenFilmControllerActions"], function() {
    var controller = require("userfrmChosenFilmController");
    var controllerActions = ["frmChosenFilmControllerActions"];
    for (var i = 0; i < controllerActions.length; i++) {
        var actions = require(controllerActions[i]);
        for (var key in actions) {
            controller[key] = actions[key];
        }
    }
    return controller;
});
