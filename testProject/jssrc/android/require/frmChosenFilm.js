define("frmChosenFilm", function() {
    return function(controller) {
        function addWidgetsfrmChosenFilm() {
            this.setDefaultUnit(kony.flex.DP);
            var mainFlexScrollContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "mainFlexScrollContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": false,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            mainFlexScrollContainer.setDefaultUnit(kony.flex.DP);
            var backFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "backFlex",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            backFlex.setDefaultUnit(kony.flex.DP);
            var imgHomebtn = new kony.ui.Image2({
                "height": "41dp",
                "id": "imgHomebtn",
                "isVisible": true,
                "left": "4dp",
                "skin": "slImage",
                "src": "http://www.achems.org/online/mobile/images/white-menu-icon.png",
                "top": "8dp",
                "width": "73dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            backFlex.add(imgHomebtn);
            var imgSegmentFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30%",
                "id": "imgSegmentFlex",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            imgSegmentFlex.setDefaultUnit(kony.flex.DP);
            kony.mvc.registry.add('FBox0c660aa1080ee4d', 'FBox0c660aa1080ee4d', 'FBox0c660aa1080ee4dController');
            var imgSegmentSlider = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "filmImg": "imagedrag.png"
                }, {
                    "filmImg": "imagedrag.png"
                }, {
                    "filmImg": "imagedrag.png"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "imgSegmentSlider",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowSkin": "CopyCopyimgSegmentSliderSkin",
                "rowTemplate": "FBox0c660aa1080ee4d",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_PAGEVIEW,
                "widgetDataMap": {
                    "filmImg": "filmImg"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            imgSegmentFlex.add(imgSegmentSlider);
            var filmTitleFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "filmTitleFlex",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            filmTitleFlex.setDefaultUnit(kony.flex.DP);
            var filmTitleLabel = new kony.ui.Label({
                "id": "filmTitleLabel",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopyCopymainTitleSkin",
                "text": "Film Title",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [4, 2, 4, 8],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            filmTitleFlex.add(filmTitleLabel);
            var ticketsBtnFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "16%",
                "id": "ticketsBtnFlex",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            ticketsBtnFlex.setDefaultUnit(kony.flex.DP);
            var ticketsBtn = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "CopyCopymainBtnFocusSkin",
                "id": "ticketsBtn",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopyCopymainBtnSkin",
                "text": "Сеансы",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 2, 0, 2],
                "paddingInPixel": false
            }, {});
            ticketsBtnFlex.add(ticketsBtn);
            var descriptionFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "descriptionFlex",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopydescriptionFlexSkin1",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            descriptionFlex.setDefaultUnit(kony.flex.DP);
            var genreFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "genreFlex",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            genreFlex.setDefaultUnit(kony.flex.DP);
            var genre = new kony.ui.Label({
                "id": "genre",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopyCopybackSkin",
                "text": "Жанр",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 2, 0, 2],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var genreDescription = new kony.ui.Label({
                "id": "genreDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopymainFilmDescriptionSkin1",
                "text": "Genre description",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [12, 0, 2, 5],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            genreFlex.add(genre, genreDescription);
            var durationFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "durationFlex",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            durationFlex.setDefaultUnit(kony.flex.DP);
            var duration = new kony.ui.Label({
                "id": "duration",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopyCopybackSkin",
                "text": "Длительность",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 2, 2],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var durationDescription = new kony.ui.Label({
                "id": "durationDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopymainFilmDescriptionSkin1",
                "text": "Duration Description",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [12, 0, 2, 5],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            durationFlex.add(duration, durationDescription);
            var ratingFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "ratingFlex",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            ratingFlex.setDefaultUnit(kony.flex.DP);
            var rating = new kony.ui.Label({
                "id": "rating",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopyCopybackSkin",
                "text": "Рейтинг",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 2, 2],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var ratingDescription = new kony.ui.Label({
                "id": "ratingDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopymainFilmDescriptionSkin1",
                "text": "Film Rating",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [12, 0, 2, 5],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            ratingFlex.add(rating, ratingDescription);
            var yearFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "yearFlex",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            yearFlex.setDefaultUnit(kony.flex.DP);
            var year = new kony.ui.Label({
                "id": "year",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopyCopybackSkin",
                "text": "Год",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 2, 2],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var yearDescription = new kony.ui.Label({
                "id": "yearDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopymainFilmDescriptionSkin1",
                "text": "Year Description",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [12, 0, 2, 5],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            yearFlex.add(year, yearDescription);
            var actorsFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "actorsFlex",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            actorsFlex.setDefaultUnit(kony.flex.DP);
            var actors = new kony.ui.Label({
                "id": "actors",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopyCopybackSkin",
                "text": "В ролях",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 2, 2],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var actorsDescription = new kony.ui.Label({
                "id": "actorsDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopymainFilmDescriptionSkin1",
                "text": "Starring actors",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [12, 0, 2, 5],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var flxPersons = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "220dp",
                "id": "flxPersons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPersons.setDefaultUnit(kony.flex.DP);
            flxPersons.add();
            actorsFlex.add(actors, actorsDescription, flxPersons);
            var plotFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "plotFlex",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            plotFlex.setDefaultUnit(kony.flex.DP);
            var plot = new kony.ui.Label({
                "id": "plot",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopyCopybackSkin",
                "text": "Описание",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 2, 2],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var plotDescription = new kony.ui.RichText({
                "id": "plotDescription",
                "isVisible": true,
                "left": "0dp",
                "linkSkin": "defRichTextLink",
                "maxWidth": "100%",
                "skin": "CopymainDescriptionRichText1",
                "text": "Film Description\\n by Rich Text",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [12, 0, 4, 5],
                "paddingInPixel": false
            }, {});
            plotFlex.add(plot, plotDescription);
            descriptionFlex.add(genreFlex, durationFlex, ratingFlex, yearFlex, actorsFlex, plotFlex);
            mainFlexScrollContainer.add(backFlex, imgSegmentFlex, filmTitleFlex, ticketsBtnFlex, descriptionFlex);
            var flxslidemenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxslidemenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-87%",
                "isModalContainer": false,
                "skin": "CopyslFbox0e102deae438446",
                "top": "0dp",
                "width": "87%",
                "zIndex": 1
            }, {}, {});
            flxslidemenu.setDefaultUnit(kony.flex.DP);
            var imgbackbtn = new kony.ui.Image2({
                "centerY": "4%",
                "height": "32dp",
                "id": "imgbackbtn",
                "isVisible": true,
                "onTouchStart": controller.AS_Image_a12ffde51ed84df486b6a2b854517d8b,
                "right": 6,
                "skin": "slImage",
                "src": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Media_Viewer_Icon_-_Previous.svg/665px-Media_Viewer_Icon_-_Previous.svg.png",
                "top": "12dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLogo = new kony.ui.Label({
                "height": "7.08%",
                "id": "lblLogo",
                "isVisible": true,
                "left": "19dp",
                "skin": "CopydefLabel0fff53605cb2648",
                "text": "CinemaApp",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var flxMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90.91%",
                "id": "flxMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0e98f5862135944",
                "top": "62dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMenu.setDefaultUnit(kony.flex.DP);
            var lblWelcomeBack = new kony.ui.Label({
                "centerX": "50.00%",
                "id": "lblWelcomeBack",
                "isVisible": true,
                "left": "19dp",
                "skin": "CopydefLabel0bc13a183070742",
                "text": "Welcome back, User!",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "35dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            kony.mvc.registry.add('CopyFBox0h027fd6b951746', 'CopyFBox0h027fd6b951746', 'CopyFBox0h027fd6b951746Controller');
            var sgmMenu = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "imgMenu": "🎦",
                    "txtMenu": "CATALOG"
                }, {
                    "imgMenu": "♻️",
                    "txtMenu": "HISTORY"
                }, {
                    "imgMenu": " ⚥",
                    "txtMenu": "ABOUT US"
                }, {
                    "imgMenu": "⚙️",
                    "txtMenu": "ACCOUNT"
                }, {
                    "imgMenu": "❎",
                    "txtMenu": "SIGN OUT"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "sgmMenu",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_jd90994789a6466f9837eccd6a4cb62c,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "Copyseg0dc1fb550f3c940",
                "rowTemplate": "CopyFBox0h027fd6b951746",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "104dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "imgMenu": "imgMenu",
                    "txtMenu": "txtMenu"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMenu.add(lblWelcomeBack, sgmMenu);
            flxslidemenu.add(imgbackbtn, lblLogo, flxMenu);
            this.add(mainFlexScrollContainer, flxslidemenu);
        };
        return [{
            "addWidgets": addWidgetsfrmChosenFilm,
            "enabledForIdleTimeout": false,
            "id": "frmChosenFilm",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyCopyfrmChosenFilmSkin"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});