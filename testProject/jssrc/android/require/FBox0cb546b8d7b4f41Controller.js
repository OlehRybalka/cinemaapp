define("userFBox0cb546b8d7b4f41Controller", {
    //Type your controller code here 
});
define("FBox0cb546b8d7b4f41ControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("FBox0cb546b8d7b4f41Controller", ["userFBox0cb546b8d7b4f41Controller", "FBox0cb546b8d7b4f41ControllerActions"], function() {
    var controller = require("userFBox0cb546b8d7b4f41Controller");
    var controllerActions = ["FBox0cb546b8d7b4f41ControllerActions"];
    for (var i = 0; i < controllerActions.length; i++) {
        var actions = require(controllerActions[i]);
        for (var key in actions) {
            controller[key] = actions[key];
        }
    }
    return controller;
});
