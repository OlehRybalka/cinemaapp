define("userfrmCatalogController", {
    //     data: {},
    //     formOnload: function (){ 
    //       var importantData = {};
    //       var cityId;
    //       var apiKey = 'pol1kh111';
    //       var operationName = 'getFilmsinDnipro';
    //       var secondOperationName = 'getFilmPoster';
    //       var cityOperationName = 'getCities';
    //       var film;
    //       var countFilms = 0;
    //       var frmCatalog = _kony.mvc.GetController('frmCatalog', true);
    //       var service = kony.sdk.getCurrentInstance().getIntegrationService('myCinemaTest');
    //       //alert('service');
    //   	service.invokeOperation(operationName, null, getFilmsData, operationSuccessFilm, function(e){
    //         alert(e);
    //       }); 
    //       var getFilmsData = {
    //         'cityId': cityId,
    //         'apiKey': apiKey,
    //         'size': '8'
    //       };
    //       function operationSuccessFilm(responce){
    //         var tmpStr = JSON.stringify(responce);
    //         parseJsonResponse(JSON.parse(tmpStr));
    //         getPoster();
    //         for(var key in importantData){
    //           frmCatalog.view.tabFilmCinema.subTabFilms.sgmFilmCollection.addDataAt(importantData[key], 0);
    //         }
    //         function parseJsonResponse(js){
    //           countFilms = js.films.length;
    //         	for(var i = 0; i < countFilms; i++){
    //             var film = {
    //               id: js.films[i].id,
    //               txtFilm: js.films[i].title,
    //               imgFilm: null,
    //               btnScreening: 'Screenings'
    //             };
    //             frmCatalog.view.tabFilmCinema.subTabFilms.sgmFilmCollection.removeAll();
    //             importantData[js.films[i].id] = film;
    //           }
    //         }
    //         var posterData = {
    //         	'filmId': null,
    //         	'apiKey': apiKey
    //         };
    //         function getPoster() {
    //           for (var key in importantData){
    //             film = importantData[key];
    //             var src ='http://kino-teatr.ua:8081/services/api/film/' + film.id + '/poster?apiKey=' + apiKey + '&width=400&height=400&ratio=1';
    //             film.imgFilm = {src:src};       
    //       	}
    //         }
    //       }
    //       function operationSuccessPoster(response){
    //         var tmpStr = JSON.stringify(response);
    //         parseJsonResponse(tmpStr);
    //         function parseJsonResponse(js){
    //           film.imgFilm = 'src="http://kino-teatr.ua:8081/services/api/film/' + film.id + 'poster?apiKey=' + apiKey + '&width=300&height=400&ratio=1"';
    //         }
    //       }
    //     },
    //     formOnloadCinema: function (){
    //       var importantDataCinema = {};
    //       var cityId;
    //       var apiKey = 'pol1kh111';
    //       var operationNameCinema = 'getCinemasInDnipro';
    //       var frmCatalog = _kony.mvc.GetController('frmCatalog', true);
    //       var service = kony.sdk.getCurrentInstance().getIntegrationService('myCinemaTest');
    //   	service.invokeOperation(operationNameCinema, null, getCinemasData, operationSuccessCinema);
    //       var getCinemasData = {
    //         'cityId': cityId,
    //         'apiKey': apiKey,
    //         'size': '1'
    //       };
    //       function operationSuccessCinema(responce){
    //         var tmpStr = JSON.stringify(responce);
    //         parseJsonResponse(JSON.parse(tmpStr));
    //         for(var key in importantDataCinema){
    //           frmCatalog.view.tabFilmCinema.subTabCinema.sgmCinemaCollection.addDataAt(importantDataCinema[key], 0);
    //         }
    //         function parseJsonResponse(js){
    //           countCinemas = js.content.length;
    //         	for(var i = 0; i < countCinemas; i++){
    //             var cinema = {
    //               id: js.content[i].id,
    //               txtCinemaTitle: js.content[i].name,
    //               btnScreening: 'Screenings'
    //             };
    //             frmCatalog.view.tabFilmCinema.subTabCinema.sgmCinemaCollection.removeAll();
    //             importantDataCinema[js.content[i].id] = cinema;
    //           }
    //         }
    //       }
    //     },
    data: {},
    onNavigate: function(context) {
        if (context) {
            this.data = context;
        }
    },
    formOnload: function() {
        try {
            var frmCatalog = _kony.mvc.GetController('frmCatalog', true);
            frmCatalog.view.sgmFilmCollection.onRowClick = this.getRowFilmData;
            frmCatalog.view.sgmCinemaCollection.onRowClick = this.getRowCinemaData;
            frmCatalog.view.tabFilmCinema.onTabClick = getFilmsAndCinemas;
            frmCatalog.view.sgmMenu.onRowClick = HamburgerMenuNavigation;
            frmCatalog.view.imgHomebtn.onTouchStart = Hamburger;
            getFilmsAndCinemas(parseInt(this.data.cityId));
            frmCatalog.view.listChoose.onSelection = this.getListBox;
        } catch (e) {
            alert("error in formOnLoad: " + e.message);
        }
    },
    getCities: function() {
        var data;
        var self = this;

        function searchData() {
            var request = new kony.net.HttpRequest();
            request.onReadyStateChange = httpCallbackHandler;
            request.open(constants.HTTP_METHOD_GET, "http://kino-teatr.ua:8081/services/api/cities?apiKey=pol1kh111&size=100");
            request.setRequestHeader('Content-type', 'application/json');
            request.send();
        }

        function httpCallbackHandler() {
            if (this.readyState == constants.HTTP_READY_STATE_DONE) {
                var setDataList = [];
                var keyList = [];
                var chosenCity = self.data.cityId;
                for (var i = 0; i < this.response.content.length; i++) {
                    keyList = [this.response.content[i].id, this.response.content[i].name];
                    setDataList.push(keyList);
                    if (chosenCity == this.response.content[i].id) {
                        var temp = [this.response.content[i].id, this.response.content[i].name];
                        setDataList.unshift(temp);
                        setDataList.pop(keyList);
                    }
                }
                self.view.listChoose.masterData = setDataList;
            }
        }
        searchData();
    },
    getListBox: function() {
        try {
            this.data.cityId = parseInt(this.view.listChoose.selectedKeyValue[0]);
            getFilmsAndCinemas(this.data.cityId);
            return this.view.listChoose.selectedKeyValue;
        } catch (err) {
            return null;
        }
    },
    getRowCinemaData: function() {
        try {
            var frmCatalog = _kony.mvc.GetController('frmCatalog', true);
            var segCinemaData = frmCatalog.view.sgmCinemaCollection.data;
            var segCinemaIndex = frmCatalog.view.sgmCinemaCollection.selectedRowIndex;
            var nav = new kony.mvc.Navigation('frmChosenCinema');
            nav.navigate(segCinemaData[segCinemaIndex[1]].id);
            HamburgerFechar();
        } catch (e) {
            alert("error in getRowCinemaData: " + e.message);
        }
    },
    getRowFilmData: function() {
        try {
            var frmCatalog = _kony.mvc.GetController('frmCatalog', true);
            var segFilmData = frmCatalog.view.sgmFilmCollection.data;
            var segFilmIndex = frmCatalog.view.sgmFilmCollection.selectedRowIndex;
            this.data.filmId = segFilmData[segFilmIndex[1]].id;
            var nav = new kony.mvc.Navigation('frmChosenFilm');
            nav.navigate(this.data);
            HamburgerFechar();
        } catch (e) {
            alert("error in getRowFilmData: " + e.message);
        }
    }
});
define("frmCatalogControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for imgbackbtn **/
    AS_Image_a12ffde51ed84df486b6a2b854517d8b: function AS_Image_a12ffde51ed84df486b6a2b854517d8b(eventobject, x, y) {
        var self = this;
        return Hamburger.call(this);
    },
    /** onRowClick defined for sgmMenu **/
    AS_Segment_f3900770e2104b3a81a93c944c7b90ca: function AS_Segment_f3900770e2104b3a81a93c944c7b90ca(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.menuNavigation.call(this);
    },
    /** init defined for frmCatalog **/
    AS_Form_e6736436b20b408ba64b3cd70f78a48c: function AS_Form_e6736436b20b408ba64b3cd70f78a48c(eventobject) {
        var self = this;
        return self.getCities.call(this);
    },
    /** preShow defined for frmCatalog **/
    AS_Form_dd13822d66fe40939fa35c2fd6bc3b06: function AS_Form_dd13822d66fe40939fa35c2fd6bc3b06(eventobject) {
        var self = this;
        return self.formOnload.call(this);
    }
});
define("frmCatalogController", ["userfrmCatalogController", "frmCatalogControllerActions"], function() {
    var controller = require("userfrmCatalogController");
    var controllerActions = ["frmCatalogControllerActions"];
    for (var i = 0; i < controllerActions.length; i++) {
        var actions = require(controllerActions[i]);
        for (var key in actions) {
            controller[key] = actions[key];
        }
    }
    return controller;
});
