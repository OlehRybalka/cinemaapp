define("userFBox0g941565fca2646Controller", {
    //Type your controller code here 
});
define("FBox0g941565fca2646ControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("FBox0g941565fca2646Controller", ["userFBox0g941565fca2646Controller", "FBox0g941565fca2646ControllerActions"], function() {
    var controller = require("userFBox0g941565fca2646Controller");
    var controllerActions = ["FBox0g941565fca2646ControllerActions"];
    for (var i = 0; i < controllerActions.length; i++) {
        var actions = require(controllerActions[i]);
        for (var key in actions) {
            controller[key] = actions[key];
        }
    }
    return controller;
});
