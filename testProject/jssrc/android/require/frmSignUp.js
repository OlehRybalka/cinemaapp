define("frmSignUp", function() {
    return function(controller) {
        function addWidgetsfrmSignUp() {
            this.setDefaultUnit(kony.flex.DP);
            var flxLogup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "45%",
                "clipBounds": true,
                "height": "550dp",
                "id": "flxLogup",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLogup.setDefaultUnit(kony.flex.DP);
            var txtLogo = new kony.ui.RichText({
                "height": "100dp",
                "id": "txtLogo",
                "isVisible": true,
                "left": "0",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0ic3e733e52c445",
                "text": "Cinema",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFields = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "260dp",
                "id": "flxFields",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "1dp",
                "minHeight": "100%",
                "isModalContainer": false,
                "skin": "CopyslFbox0f51d0a628dfa4f",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFields.setDefaultUnit(kony.flex.DP);
            var fldName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50.03%",
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "fldName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 20,
                "placeholder": "Name",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0c66cc35f05f844",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "5dp",
                "width": "277dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoFilter": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
                "placeholderSkin": "CopydefTextBoxPlaceholder0c3d78f921a5843",
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var lblNameWarning = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNameWarning",
                "isVisible": true,
                "left": "0dp",
                "skin": "lblWarningSkn",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "277dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var fldEmail = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50.03%",
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "fldEmail",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
                "left": "0dp",
                "maxTextLength": 20,
                "placeholder": "email",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0c66cc35f05f844",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "1dp",
                "width": "277dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoFilter": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
                "placeholderSkin": "CopydefTextBoxPlaceholder0c3d78f921a5843",
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var lblEmailWarning = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblEmailWarning",
                "isVisible": true,
                "left": "0dp",
                "skin": "lblWarningSkn",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "277dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var fldPassword = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerX": "50%",
                "focusSkin": "CopydefTextBoxFocus0f3cd5966f54143",
                "height": "40dp",
                "id": "fldPassword",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 15,
                "placeholder": "Password",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0g6f133a2a34246",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "1dp",
                "width": "277dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoFilter": false,
                "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
                "placeholderSkin": "CopydefTextBoxPlaceholder0b6b8b040aacb4f",
                "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
            });
            var lblPasswordWarning = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblPasswordWarning",
                "isVisible": true,
                "skin": "lblWarningSkn",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "277dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var ListBoxCities = new kony.ui.ListBox({
                "centerX": "50%",
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "ListBoxCities",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["Key2439571439", "Choose your city"]
                ],
                "onSelection": controller.AS_ListBox_gb7393c879bd4e969bd4c7365ded87d3,
                "skin": "CopydefListBoxNormal0j44d209d8fca4a",
                "top": "1dp",
                "width": "277dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "applySkinsToPopup": true,
                "dropDownImage": "listboxarw.png",
                "placeholder": "Select you city",
                "placeholderSkin": "test",
                "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
            });
            flxFields.add(fldName, lblNameWarning, fldEmail, lblEmailWarning, fldPassword, lblPasswordWarning, ListBoxCities);
            var flxAgreement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "31dp",
                "id": "flxAgreement",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0e44a506081004f",
                "top": "0dp",
                "width": "277dp",
                "zIndex": 1
            }, {}, {});
            flxAgreement.setDefaultUnit(kony.flex.DP);
            var chkAgreement = new kony.ui.CheckBoxGroup({
                "height": "32dp",
                "id": "chkAgreement",
                "isVisible": true,
                "masterData": [
                    ["cbg1", "I agree with"]
                ],
                "selectedKeyValues": [
                    ["cbg1", "I agree with"]
                ],
                "selectedKeys": ["cbg1"],
                "skin": "CopyslCheckBoxGroup0b30f3180e5e748",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtTerms = new kony.ui.RichText({
                "height": "27dp",
                "id": "txtTerms",
                "isVisible": true,
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0e8302e265d4545",
                "text": "Terms of Usage",
                "top": "3dp",
                "width": "130dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAgreement.add(chkAgreement, txtTerms);
            var dtnSignUp = new kony.ui.Button({
                "centerX": "50.00%",
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "dtnSignUp",
                "isVisible": true,
                "left": "90dp",
                "onClick": controller.AS_Button_cbc731501c90450b90180a66efd3ba5a,
                "skin": "CopydefBtnNormal0c69f65aa8d4047",
                "text": "SIGN UP",
                "top": 30,
                "width": "260dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxIsRegistered = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxIsRegistered",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "CopyslFbox0d1fd914ab6d048",
                "top": "15dp",
                "width": "277dp",
                "zIndex": 1
            }, {}, {});
            flxIsRegistered.setDefaultUnit(kony.flex.DP);
            var txtisRegistered = new kony.ui.RichText({
                "height": "27dp",
                "id": "txtisRegistered",
                "isVisible": true,
                "left": "0dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0f6c6d5e177b145",
                "text": "I have",
                "top": "0dp",
                "width": "45%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 2, 0],
                "paddingInPixel": false
            }, {});
            var txtisRegistered2 = new kony.ui.RichText({
                "height": "27dp",
                "id": "txtisRegistered2",
                "isVisible": true,
                "left": "0dp",
                "linkSkin": "defRichTextLink",
                "onClick": controller.AS_RichText_fa78636225b647d497993e4afd677f5e,
                "skin": "CopydefRichTextNormal0a404fe12894947",
                "text": "an Account",
                "top": "0dp",
                "width": "55%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxIsRegistered.add(txtisRegistered, txtisRegistered2);
            flxLogup.add(txtLogo, flxFields, flxAgreement, dtnSignUp, flxIsRegistered);
            this.add(flxLogup);
        };
        return [{
            "addWidgets": addWidgetsfrmSignUp,
            "enabledForIdleTimeout": false,
            "id": "frmSignUp",
            "init": controller.AS_Form_a9d944e3f6e1447aa475d6f04c948587,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "needAppMenu": false,
            "skin": "CopyslForm0cd5882a2211c4c"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});