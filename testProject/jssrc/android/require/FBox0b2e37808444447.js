define("FBox0b2e37808444447", function() {
    return function(controller) {
        FBox0b2e37808444447 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "500dp",
            "id": "FBox0b2e37808444447",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "width": "100%"
        }, {
            "containerWeight": 100
        }, {});
        FBox0b2e37808444447.setDefaultUnit(kony.flex.DP);
        var imgChosenFilm = new kony.ui.Image2({
            "height": "208dp",
            "id": "imgChosenFilm",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "imagedrag.png",
            "top": "5dp",
            "width": "415dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        var txtRating = new kony.ui.RichText({
            "height": "150px",
            "id": "txtRating",
            "isVisible": true,
            "left": "350dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0ce93d0d3c76e47",
            "text": "7.3/10",
            "top": "169dp",
            "width": "150px",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "hExpand": true,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        FBox0b2e37808444447.add(imgChosenFilm, txtRating);
        return FBox0b2e37808444447;
    }
})