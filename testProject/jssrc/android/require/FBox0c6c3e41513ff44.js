define("FBox0c6c3e41513ff44", function() {
    return function(controller) {
        FBox0c6c3e41513ff44 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "height": "preferred",
            "id": "FBox0c6c3e41513ff44",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "width": "100%"
        }, {
            "containerWeight": 100
        }, {});
        FBox0c6c3e41513ff44.setDefaultUnit(kony.flex.DP);
        var cinemaImg = new kony.ui.Image2({
            "id": "cinemaImg",
            "isVisible": true,
            "left": "0dp",
            "onDownloadComplete": controller.AS_Image_cf81f4deedd34ea5bbf278bbb6153ef0,
            "skin": "slImage",
            "src": "imagedrag.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        FBox0c6c3e41513ff44.add(cinemaImg);
        return FBox0c6c3e41513ff44;
    }
})