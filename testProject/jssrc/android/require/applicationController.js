define({
    appInit: function(params) {
        skinsInit();
        kony.mvc.registry.add("flxScreeningsByCinema", "flxScreeningsByCinema", "flxScreeningsByCinemaController");
        kony.mvc.registry.add("flxScreeningsByFilm", "flxScreeningsByFilm", "flxScreeningsByFilmController");
        kony.mvc.registry.add("frmAccountSettings", "frmAccountSettings", "frmAccountSettingsController");
        kony.mvc.registry.add("frmCatalog", "frmCatalog", "frmCatalogController");
        kony.mvc.registry.add("frmChooseSession", "frmChooseSession", "frmChooseSessionController");
        kony.mvc.registry.add("frmChosenCinema", "frmChosenCinema", "frmChosenCinemaController");
        kony.mvc.registry.add("frmChosenFilm", "frmChosenFilm", "frmChosenFilmController");
        kony.mvc.registry.add("frmScreenings", "frmScreenings", "frmScreeningsController");
        kony.mvc.registry.add("frmSignIn", "frmSignIn", "frmSignInController");
        kony.mvc.registry.add("frmSignUp", "frmSignUp", "frmSignUpController");
        kony.mvc.registry.add("frmStart", "frmStart", "frmStartController");
        setAppBehaviors();
    },
    postAppInitCallBack: function(eventObj) {},
    appmenuseq: function() {
        new kony.mvc.Navigation("frmStart").navigate();
    }
});