define("FBox0cb546b8d7b4f41", function() {
    return function(controller) {
        FBox0cb546b8d7b4f41 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "23%",
            "id": "FBox0cb546b8d7b4f41",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "width": "100%"
        }, {
            "containerWeight": 100
        }, {});
        FBox0cb546b8d7b4f41.setDefaultUnit(kony.flex.DP);
        var imgFilm = new kony.ui.Image2({
            "centerY": "50.04%",
            "height": "123dp",
            "id": "imgFilm",
            "imageWhileDownloading": "film2_1.png",
            "isVisible": true,
            "left": "8dp",
            "skin": "slImage",
            "src": "imagedrag.png",
            "top": "0dp",
            "width": "135dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        var txtFilm = new kony.ui.RichText({
            "centerY": "44.70%",
            "height": "104dp",
            "id": "txtFilm",
            "isVisible": true,
            "left": "185dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopyCopydefRichTextNormal0hbfeafc9cedc48",
            "text": "RichText",
            "top": "0dp",
            "width": "43.52%",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "hExpand": true,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        FBox0cb546b8d7b4f41.add(imgFilm, txtFilm);
        return FBox0cb546b8d7b4f41;
    }
})