define("FBox0g941565fca2646", function() {
    return function(controller) {
        FBox0g941565fca2646 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "22%",
            "id": "FBox0g941565fca2646",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "width": "100%"
        }, {
            "containerWeight": 100
        }, {});
        FBox0g941565fca2646.setDefaultUnit(kony.flex.DP);
        var txtCinemaTitle = new kony.ui.RichText({
            "centerX": "50%",
            "centerY": "50%",
            "height": "96dp",
            "id": "txtCinemaTitle",
            "isVisible": true,
            "left": "15dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopyCopydefRichTextNormal0c377cda5dc0848",
            "text": "RichText",
            "top": "19dp",
            "width": "78.70%",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "hExpand": true,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        FBox0g941565fca2646.add(txtCinemaTitle);
        return FBox0g941565fca2646;
    }
})