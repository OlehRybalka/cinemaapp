define("frmChosenCinema", function() {
    return function(controller) {
        function addWidgetsfrmChosenCinema() {
            this.setDefaultUnit(kony.flex.DP);
            var contChosenCinema = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "contChosenCinema",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            contChosenCinema.setDefaultUnit(kony.flex.DP);
            var backFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "backFlex",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            backFlex.setDefaultUnit(kony.flex.DP);
            var imgHomebtn = new kony.ui.Image2({
                "height": "41dp",
                "id": "imgHomebtn",
                "isVisible": true,
                "left": "4dp",
                "skin": "slImage",
                "src": "http://www.achems.org/online/mobile/images/white-menu-icon.png",
                "top": "8dp",
                "width": "73dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            backFlex.add(imgHomebtn);
            var FlexContainerImgSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30%",
                "id": "FlexContainerImgSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            FlexContainerImgSegment.setDefaultUnit(kony.flex.DP);
            kony.mvc.registry.add('FBox0c6c3e41513ff44', 'FBox0c6c3e41513ff44', 'FBox0c6c3e41513ff44Controller');
            var sgmCinema = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "cinemaImg": "imagedrag.png"
                }, {
                    "cinemaImg": "imagedrag.png"
                }, {
                    "cinemaImg": "imagedrag.png"
                }, {
                    "cinemaImg": "imagedrag.png"
                }, {
                    "cinemaImg": "imagedrag.png"
                }, {
                    "cinemaImg": "imagedrag.png"
                }, {
                    "cinemaImg": "imagedrag.png"
                }, {
                    "cinemaImg": "imagedrag.png"
                }, {
                    "cinemaImg": "imagedrag.png"
                }, {
                    "cinemaImg": "imagedrag.png"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "sgmCinema",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_i4dc18eb42c34c7e91d776b40b626acb,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "CopyCopyCopyseg",
                "rowTemplate": "FBox0c6c3e41513ff44",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_PAGEVIEW,
                "widgetDataMap": {
                    "cinemaImg": "cinemaImg"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainerImgSegment.add(sgmCinema);
            var FlexContainerCinemaName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "FlexContainerCinemaName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            FlexContainerCinemaName.setDefaultUnit(kony.flex.DP);
            var txtCinemaTitle = new kony.ui.RichText({
                "id": "txtCinemaTitle",
                "isVisible": true,
                "left": "0dp",
                "linkSkin": "defRichTextLink",
                "skin": "CinemaTitleSkin",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [4, 2, 4, 0],
                "paddingInPixel": false
            }, {});
            FlexContainerCinemaName.add(txtCinemaTitle);
            var FlexContainerCinemaSite = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "FlexContainerCinemaSite",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            FlexContainerCinemaSite.setDefaultUnit(kony.flex.DP);
            var txtCinemaSite = new kony.ui.RichText({
                "id": "txtCinemaSite",
                "isVisible": true,
                "left": "0dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopyCopyCopydefRichTextNormal",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 2, 0, 4],
                "paddingInPixel": false
            }, {});
            FlexContainerCinemaSite.add(txtCinemaSite);
            var FlexContainerCinemaPhone = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "FlexContainerCinemaPhone",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            FlexContainerCinemaPhone.setDefaultUnit(kony.flex.DP);
            var Label0dd939c7390a245 = new kony.ui.Label({
                "id": "Label0dd939c7390a245",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopybackSkin1",
                "text": "Телефон :",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 2, 0, 4],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var txtCinemaPhone = new kony.ui.RichText({
                "id": "txtCinemaPhone",
                "isVisible": true,
                "left": "0dp",
                "linkSkin": "defRichTextLink",
                "skin": "WhiteRichTextSkin110",
                "top": "0dp",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 2, 4, 4],
                "paddingInPixel": false
            }, {});
            FlexContainerCinemaPhone.add(Label0dd939c7390a245, txtCinemaPhone);
            var FlexContainerCinemaAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "FlexContainerCinemaAddress",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            FlexContainerCinemaAddress.setDefaultUnit(kony.flex.DP);
            var LabelCinemaAddress = new kony.ui.Label({
                "id": "LabelCinemaAddress",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopybackSkin1",
                "text": "Адрес :",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 0, 8],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var txtCinemaAddress = new kony.ui.RichText({
                "id": "txtCinemaAddress",
                "isVisible": true,
                "left": "0dp",
                "linkSkin": "defRichTextLink",
                "skin": "WhiteRichTextSkin110",
                "top": "0dp",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 4, 8],
                "paddingInPixel": false
            }, {});
            FlexContainerCinemaAddress.add(LabelCinemaAddress, txtCinemaAddress);
            var ticketsBtnFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "14%",
                "id": "ticketsBtnFlex",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            ticketsBtnFlex.setDefaultUnit(kony.flex.DP);
            var btnScreening = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "CopymainBtnFocusSkin1",
                "id": "btnScreening",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopymainBtnSkin1",
                "text": "Сеансы",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 2, 0, 2],
                "paddingInPixel": false
            }, {});
            ticketsBtnFlex.add(btnScreening);
            var FlexContainerCinemaDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "FlexContainerCinemaDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            FlexContainerCinemaDescription.setDefaultUnit(kony.flex.DP);
            var txtCinemaDescription = new kony.ui.RichText({
                "id": "txtCinemaDescription",
                "isVisible": true,
                "left": "0dp",
                "linkSkin": "defRichTextLink",
                "skin": "WhiteRichTextSkin110Cloudy",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 8, 4, 4],
                "paddingInPixel": false
            }, {});
            FlexContainerCinemaDescription.add(txtCinemaDescription);
            contChosenCinema.add(backFlex, FlexContainerImgSegment, FlexContainerCinemaName, FlexContainerCinemaSite, FlexContainerCinemaPhone, FlexContainerCinemaAddress, ticketsBtnFlex, FlexContainerCinemaDescription);
            var flxslidemenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxslidemenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-87%",
                "isModalContainer": false,
                "skin": "CopyslFbox0e102deae438446",
                "top": "0dp",
                "width": "87%",
                "zIndex": 1
            }, {}, {});
            flxslidemenu.setDefaultUnit(kony.flex.DP);
            var imgbackbtn = new kony.ui.Image2({
                "centerY": "4%",
                "height": "32dp",
                "id": "imgbackbtn",
                "isVisible": true,
                "onTouchStart": controller.AS_Image_a12ffde51ed84df486b6a2b854517d8b,
                "right": 6,
                "skin": "slImage",
                "src": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Media_Viewer_Icon_-_Previous.svg/665px-Media_Viewer_Icon_-_Previous.svg.png",
                "top": "12dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLogo = new kony.ui.Label({
                "height": "7.08%",
                "id": "lblLogo",
                "isVisible": true,
                "left": "19dp",
                "skin": "CopydefLabel0fff53605cb2648",
                "text": "CinemaApp",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var flxMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90.91%",
                "id": "flxMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0e98f5862135944",
                "top": "62dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMenu.setDefaultUnit(kony.flex.DP);
            var lblWelcomeBack = new kony.ui.Label({
                "centerX": "50.00%",
                "id": "lblWelcomeBack",
                "isVisible": true,
                "left": "19dp",
                "skin": "CopydefLabel0bc13a183070742",
                "text": "Welcome back, User!",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "35dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            kony.mvc.registry.add('CopyFBox0e5e5327284e749', 'CopyFBox0e5e5327284e749', 'CopyFBox0e5e5327284e749Controller');
            var sgmMenu = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "imgMenu": "🎦",
                    "txtMenu": "CATALOG"
                }, {
                    "imgMenu": "♻️",
                    "txtMenu": "HISTORY"
                }, {
                    "imgMenu": " ⚥",
                    "txtMenu": "ABOUT US"
                }, {
                    "imgMenu": "⚙️",
                    "txtMenu": "ACCOUNT"
                }, {
                    "imgMenu": "❎",
                    "txtMenu": "SIGN OUT"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "sgmMenu",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_h10bf52c9a044c5b929a8590c552917c,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "Copyseg0dc1fb550f3c940",
                "rowTemplate": "CopyFBox0e5e5327284e749",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "104dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "imgMenu": "imgMenu",
                    "txtMenu": "txtMenu"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMenu.add(lblWelcomeBack, sgmMenu);
            flxslidemenu.add(imgbackbtn, lblLogo, flxMenu);
            this.add(contChosenCinema, flxslidemenu);
        };
        return [{
            "addWidgets": addWidgetsfrmChosenCinema,
            "enabledForIdleTimeout": false,
            "id": "frmChosenCinema",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyCopyCopyslForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});