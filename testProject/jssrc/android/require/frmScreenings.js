define("frmScreenings", function() {
    return function(controller) {
        function addWidgetsfrmScreenings() {
            this.setDefaultUnit(kony.flex.DP);
            var sgmCollection = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "imgSubitem": "imagedrag.png",
                    "txtSubitemTitle": "RichText",
                    "txtTime": "RichText"
                }],
                "groupCells": false,
                "height": "530dp",
                "id": "sgmCollection",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_ddb3ae7fa409484db6a0f3d14e6d8a94,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "CopyCopyCopyCopyseg1",
                "rowTemplate": "flxScreeningsByCinema",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "000000",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "219dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxScreeningsByCinema": "flxScreeningsByCinema",
                    "imgSubitem": "imgSubitem",
                    "txtSubitemTitle": "txtSubitemTitle",
                    "txtTime": "txtTime"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var FlxContChosenItem = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "211dp",
                "id": "FlxContChosenItem",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            FlxContChosenItem.setDefaultUnit(kony.flex.DP);
            var txtChosenItemScrTitle = new kony.ui.RichText({
                "height": "113dp",
                "id": "txtChosenItemScrTitle",
                "isVisible": true,
                "left": "155dp",
                "linkSkin": "defRichTextLink",
                "right": 10,
                "skin": "chosenItemTitle",
                "text": "Название",
                "top": "49dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgChosenItemScr = new kony.ui.Image2({
                "height": "165dp",
                "id": "imgChosenItemScr",
                "isVisible": true,
                "left": "5dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "top": "49dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var calendarDate = new kony.ui.Calendar({
                "calendarIcon": "calbtn.png",
                "dateComponents": ["21", "11", "2018"],
                "dateFormat": "dd/MM/yyyy",
                "day": 21,
                "formattedDate": "21/11/2018",
                "height": "40dp",
                "hour": 12,
                "id": "calendarDate",
                "isVisible": true,
                "left": "177dp",
                "minutes": 39,
                "month": 11,
                "right": 10,
                "seconds": 7,
                "skin": "CopyCopyCopyslCalendar1",
                "top": "170dp",
                "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
                "year": 2018,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgHomebtn = new kony.ui.Image2({
                "height": "41dp",
                "id": "imgHomebtn",
                "isVisible": true,
                "left": "4dp",
                "skin": "slImage",
                "src": "http://www.achems.org/online/mobile/images/white-menu-icon.png",
                "top": "8dp",
                "width": "73dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlxContChosenItem.add(txtChosenItemScrTitle, imgChosenItemScr, calendarDate, imgHomebtn);
            var flxslidemenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxslidemenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-87%",
                "isModalContainer": false,
                "skin": "CopyslFbox0e102deae438446",
                "top": "0dp",
                "width": "87%",
                "zIndex": 1
            }, {}, {});
            flxslidemenu.setDefaultUnit(kony.flex.DP);
            var imgbackbtn = new kony.ui.Image2({
                "centerY": "4%",
                "height": "32dp",
                "id": "imgbackbtn",
                "isVisible": true,
                "onTouchStart": controller.AS_Image_a12ffde51ed84df486b6a2b854517d8b,
                "right": 6,
                "skin": "slImage",
                "src": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Media_Viewer_Icon_-_Previous.svg/665px-Media_Viewer_Icon_-_Previous.svg.png",
                "top": "12dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLogo = new kony.ui.Label({
                "height": "7.08%",
                "id": "lblLogo",
                "isVisible": true,
                "left": "19dp",
                "skin": "CopydefLabel0fff53605cb2648",
                "text": "CinemaApp",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var flxMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90.91%",
                "id": "flxMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0e98f5862135944",
                "top": "62dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMenu.setDefaultUnit(kony.flex.DP);
            var lblWelcomeBack = new kony.ui.Label({
                "centerX": "50.00%",
                "id": "lblWelcomeBack",
                "isVisible": true,
                "left": "19dp",
                "skin": "CopydefLabel0bc13a183070742",
                "text": "Welcome back, User!",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "35dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            kony.mvc.registry.add('CopyFBox0d7073470bd9d47', 'CopyFBox0d7073470bd9d47', 'CopyFBox0d7073470bd9d47Controller');
            var sgmMenu = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "imgMenu": "🎦",
                    "txtMenu": "CATALOG"
                }, {
                    "imgMenu": "♻️",
                    "txtMenu": "HISTORY"
                }, {
                    "imgMenu": " ⚥",
                    "txtMenu": "ABOUT US"
                }, {
                    "imgMenu": "⚙️",
                    "txtMenu": "ACCOUNT"
                }, {
                    "imgMenu": "❎",
                    "txtMenu": "SIGN OUT"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "sgmMenu",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_b5ded837cdbb48238085a3488c5333ed,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "Copyseg0dc1fb550f3c940",
                "rowTemplate": "CopyFBox0d7073470bd9d47",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "104dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "imgMenu": "imgMenu",
                    "txtMenu": "txtMenu"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMenu.add(lblWelcomeBack, sgmMenu);
            flxslidemenu.add(imgbackbtn, lblLogo, flxMenu);
            this.add(sgmCollection, FlxContChosenItem, flxslidemenu);
        };
        return [{
            "addWidgets": addWidgetsfrmScreenings,
            "enabledForIdleTimeout": false,
            "id": "frmScreenings",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "postShow": controller.AS_Form_ab3f4f7283f24db48bdcba3a432d87ab,
            "skin": "CopyCopyCopyCopyslForm1"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});