define("userFBox0c6c3e41513ff44Controller", {
    //Type your controller code here 
});
define("FBox0c6c3e41513ff44ControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onDownloadComplete defined for cinemaImg **/
    AS_Image_cf81f4deedd34ea5bbf278bbb6153ef0: function AS_Image_cf81f4deedd34ea5bbf278bbb6153ef0(eventobject, imagesrc, issuccess, context) {
        var self = this;
        return
    }
});
define("FBox0c6c3e41513ff44Controller", ["userFBox0c6c3e41513ff44Controller", "FBox0c6c3e41513ff44ControllerActions"], function() {
    var controller = require("userFBox0c6c3e41513ff44Controller");
    var controllerActions = ["FBox0c6c3e41513ff44ControllerActions"];
    for (var i = 0; i < controllerActions.length; i++) {
        var actions = require(controllerActions[i]);
        for (var key in actions) {
            controller[key] = actions[key];
        }
    }
    return controller;
});
