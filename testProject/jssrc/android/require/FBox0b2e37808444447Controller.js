define("userFBox0b2e37808444447Controller", {
    //Type your controller code here 
});
define("FBox0b2e37808444447ControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("FBox0b2e37808444447Controller", ["userFBox0b2e37808444447Controller", "FBox0b2e37808444447ControllerActions"], function() {
    var controller = require("userFBox0b2e37808444447Controller");
    var controllerActions = ["FBox0b2e37808444447ControllerActions"];
    for (var i = 0; i < controllerActions.length; i++) {
        var actions = require(controllerActions[i]);
        for (var key in actions) {
            controller[key] = actions[key];
        }
    }
    return controller;
});
