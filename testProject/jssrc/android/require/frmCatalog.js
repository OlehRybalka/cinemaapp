define("frmCatalog", function() {
    return function(controller) {
        function addWidgetsfrmCatalog() {
            this.setDefaultUnit(kony.flex.DP);
            var flxForms = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxForms",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxForms.setDefaultUnit(kony.flex.DP);
            var listChoose = new kony.ui.ListBox({
                "centerX": "78.70%",
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "listChoose",
                "isVisible": true,
                "left": "74dp",
                "skin": "CopyCopydefListBoxNormal1",
                "top": "8dp",
                "width": "162dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "applySkinsToPopup": true,
                "dropDownImage": "listboxarw.png",
                "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
            });
            var tabFilmCinema = new kony.ui.TabPane({
                "activeSkin": "CopyCopytabCanvas",
                "activeTabs": [0],
                "height": "650dp",
                "id": "tabFilmCinema",
                "inactiveSkin": "CopyCopytabCanvasInactive",
                "isVisible": true,
                "layoutType": constants.CONTAINER_LAYOUT_BOX,
                "left": "0dp",
                "top": "65dp",
                "viewConfig": {
                    "collapsibleViewConfig": {
                        "imagePosition": constants.TABPANE_COLLAPSIBLE_IMAGE_POSITION_RIGHT,
                        "tabNameAlignment": constants.TABPANE_COLLAPSIBLE_TABNAME_ALIGNMENT_LEFT,
                        "toggleTabs": false
                    },
                    "pageViewConfig": {
                        "needPageIndicator": true
                    },
                    "tabViewConfig": {
                        "headerPosition": constants.TAB_HEADER_POSITION_TOP,
                        "image1": "tableftarrow.png",
                        "image2": "tabrightarrow.png"
                    },
                },
                "viewType": constants.TABPANE_VIEW_TYPE_TABVIEW,
                "width": "100%",
                "zIndex": 1
            }, {
                "layoutType": constants.CONTAINER_LAYOUT_BOX,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "tabHeaderHeight": 64
            });
            var subTabFilms = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "subTabFilms",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "orientation": constants.BOX_LAYOUT_VERTICAL,
                "skin": "CopyCopyslTab1",
                "tabName": "FIlms",
                "width": "100%"
            }, {
                "layoutType": kony.flex.FLOW_VERTICAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            subTabFilms.setDefaultUnit(kony.flex.PERCENTAGE);
            kony.mvc.registry.add('FBox0cb546b8d7b4f41', 'FBox0cb546b8d7b4f41', 'FBox0cb546b8d7b4f41Controller');
            var sgmFilmCollection = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "imgFilm": "",
                    "txtFilm": ""
                }],
                "groupCells": false,
                "height": "562dp",
                "id": "sgmFilmCollection",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "CopyCopyseg4",
                "rowTemplate": "FBox0cb546b8d7b4f41",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 2,
                "showScrollbars": false,
                "top": "3dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "imgFilm": "imgFilm",
                    "txtFilm": "txtFilm"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            subTabFilms.add(sgmFilmCollection);
            tabFilmCinema.addTab("subTabFilms", "FIlms", null, subTabFilms, null);
            var subTabCinema = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "subTabCinema",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "orientation": constants.BOX_LAYOUT_VERTICAL,
                "skin": "CopyCopyslTab",
                "tabName": "Cinemas",
                "width": "100%"
            }, {
                "layoutType": kony.flex.FREE_FORM,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            subTabCinema.setDefaultUnit(kony.flex.DP);
            kony.mvc.registry.add('FBox0g941565fca2646', 'FBox0g941565fca2646', 'FBox0g941565fca2646Controller');
            var sgmCinemaCollection = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "txtCinemaTitle": ""
                }],
                "groupCells": false,
                "height": 585,
                "id": "sgmCinemaCollection",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "CopyCopyseg3",
                "rowTemplate": "FBox0g941565fca2646",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 2,
                "showScrollbars": false,
                "top": "4dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "txtCinemaTitle": "txtCinemaTitle"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            subTabCinema.add(sgmCinemaCollection);
            tabFilmCinema.addTab("subTabCinema", "Cinemas", null, subTabCinema, null);
            var imgHomebtn = new kony.ui.Image2({
                "height": "41dp",
                "id": "imgHomebtn",
                "isVisible": true,
                "left": "4dp",
                "skin": "slImage",
                "src": "http://www.achems.org/online/mobile/images/white-menu-icon.png",
                "top": "8dp",
                "width": "73dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxForms.add(listChoose, tabFilmCinema, imgHomebtn);
            var flxslidemenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxslidemenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-87%",
                "isModalContainer": false,
                "skin": "CopyCopyslFbox1",
                "top": "0dp",
                "width": "87%",
                "zIndex": 1
            }, {}, {});
            flxslidemenu.setDefaultUnit(kony.flex.DP);
            var imgbackbtn = new kony.ui.Image2({
                "centerY": "4%",
                "height": "32dp",
                "id": "imgbackbtn",
                "isVisible": true,
                "onTouchStart": controller.AS_Image_a12ffde51ed84df486b6a2b854517d8b,
                "right": 6,
                "skin": "slImage",
                "src": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Media_Viewer_Icon_-_Previous.svg/665px-Media_Viewer_Icon_-_Previous.svg.png",
                "top": "12dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLogo = new kony.ui.Label({
                "height": "7.08%",
                "id": "lblLogo",
                "isVisible": true,
                "left": "19dp",
                "skin": "CopyCopydefLabel1",
                "text": "CinemaApp",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            var flxMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90.91%",
                "id": "flxMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyCopyslFbox",
                "top": "62dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMenu.setDefaultUnit(kony.flex.DP);
            var lblWelcomeBack = new kony.ui.Label({
                "centerX": "50.00%",
                "id": "lblWelcomeBack",
                "isVisible": true,
                "left": "19dp",
                "skin": "CopyCopydefLabel2",
                "text": "Welcome back, User!",
                "textStyle": {
                    "letterSpacing": 0,
                    "strikeThrough": false
                },
                "top": "35dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "textCopyable": false
            });
            kony.mvc.registry.add('FBox0f4ac158ac0cb4b', 'FBox0f4ac158ac0cb4b', 'FBox0f4ac158ac0cb4bController');
            var sgmMenu = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "imgMenu": "🎦",
                    "txtMenu": "CATALOG"
                }, {
                    "imgMenu": "♻️",
                    "txtMenu": "HISTORY"
                }, {
                    "imgMenu": " ⚥",
                    "txtMenu": "ABOUT US"
                }, {
                    "imgMenu": "⚙️",
                    "txtMenu": "ACCOUNT"
                }, {
                    "imgMenu": "❎",
                    "txtMenu": "SIGN OUT"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "sgmMenu",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_f3900770e2104b3a81a93c944c7b90ca,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "CopyCopyseg2",
                "rowTemplate": "FBox0f4ac158ac0cb4b",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "104dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "imgMenu": "imgMenu",
                    "txtMenu": "txtMenu"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMenu.add(lblWelcomeBack, sgmMenu);
            flxslidemenu.add(imgbackbtn, lblLogo, flxMenu);
            this.add(flxForms, flxslidemenu);
        };
        return [{
            "addWidgets": addWidgetsfrmCatalog,
            "enabledForIdleTimeout": false,
            "id": "frmCatalog",
            "init": controller.AS_Form_e6736436b20b408ba64b3cd70f78a48c,
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_dd13822d66fe40939fa35c2fd6bc3b06(eventobject);
            },
            "skin": "CopyCopyslForm2"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});