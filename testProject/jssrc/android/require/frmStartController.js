define("userfrmStartController", {
    showSignUp: function() {
        var nav = new kony.mvc.Navigation('frmSignUp');
        nav.navigate();
    },
    showSignIn: function() {
        var nav = new kony.mvc.Navigation('frmSignIn');
        nav.navigate();
    }
});
define("frmStartControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnSignUp **/
    AS_Button_fe70ee30eef64ef7a690f6d762a4f7a0: function AS_Button_fe70ee30eef64ef7a690f6d762a4f7a0(eventobject) {
        var self = this;
        return self.showSignUp.call(this);
    },
    /** onClick defined for btnSignIn **/
    AS_Button_e4f599c628fd4eb3ba252956b2b50c8b: function AS_Button_e4f599c628fd4eb3ba252956b2b50c8b(eventobject) {
        var self = this;
        return self.showSignIn.call(this);
    }
});
define("frmStartController", ["userfrmStartController", "frmStartControllerActions"], function() {
    var controller = require("userfrmStartController");
    var controllerActions = ["frmStartControllerActions"];
    for (var i = 0; i < controllerActions.length; i++) {
        var actions = require(controllerActions[i]);
        for (var key in actions) {
            controller[key] = actions[key];
        }
    }
    return controller;
});
