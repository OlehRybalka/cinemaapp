define("userfrmChosenCinemaController", {
    //   data: {
    //     'testKey': 'pol1kh111'
    //   },
    //   onNavigate: function (context) {
    //     if (context) {
    //       this.data.id = context;
    //       var id = this.data.id;
    //       var self = this;
    //       try {
    //         var service = kony.sdk.getCurrentInstance().getIntegrationService("CinemaApp");
    //         service.invokeOperation(
    //           'getCinemaNameById',
    //           null,
    //           this.data,
    //           operationSuccess, operationFailure);
    //         service.invokeOperation(
    //           'getCinemasPhotosById',
    //           null,
    //           this.data,
    //           function (response) {
    //             var setDataList = [];
    //             var importantData = response.images;
    //             for (var i = 0; i < importantData.length; i++) {
    //               setDataList.push({
    //                 cinemaImg: 'http://kino-teatr.ua:8081/services/api/cinema/image/' + importantData[i].id + '?apiKey=pol1kh111',
    //                 cinemaId: importantData[i].id
    //               });
    //             }
    //             self.view.sgmCinema.widgetDataMap = {
    //               cinemaImg: 'cinemaImg'
    //             };
    //             self.view.sgmCinema.setData(setDataList);
    //           },
    //           function (error) {
    //             alert("Error: " + JSON.stringify(error));
    //           });
    //       }
    //       catch (exception) {
    //         kony.print("Exception: " + exception.message);
    //       }
    //     }
    //     function operationSuccess(response) {
    //       var tmpStr = JSON.stringify(response);
    //       var importantData = parseJsonResponse(JSON.parse(tmpStr));
    //       var formController = _kony.mvc.GetController("frmChosenCinema", true);
    //       formController.view.txtCinemaTitle.text = importantData.name;
    //       formController.view.txtCinemaAddress.text = importantData.address;
    //       formController.view.txtCinemaPhone.text = importantData.phone;
    //       formController.view.txtCinemaSite.text = importantData.site;
    //       formController.view.txtCinemaDescription.text = importantData.description;
    //     }
    //     function operationFailure(error) {
    //       alert("Error: " + JSON.stringify(error));
    //     }
    //     function parseJsonResponse(js) {
    //       return {
    //         name: js.name,
    //         phone: js.phone,
    //         address: js.address,
    //         site: js.site,
    //         description: js.description
    //       };
    //     }
    //   },
    //   invokeTestGetWeatherOperation: function () {
    //   },
    //   segClick: function () {
    //     alert(this.view.sgmCinema.data);
    //   },
    //   showNextForm: function () {
    //     var nav = new kony.mvc.Navigation('frmScreenings');
    //     nav.navigate(this.data);
    //   }
    data: {},
    onNavigate: function(context) {
        var frmChosenCinema = _kony.mvc.GetController("frmChosenCinema", true);
        frmChosenCinema.view.sgmMenu.onRowClick = HamburgerMenuNavigation;
        frmChosenCinema.view.imgHomebtn.onTouchStart = Hamburger;
        frmChosenCinema.view.btnScreening.onClick = this.goToScreenings;
        if (context) {
            this.data.id = context;
        }
        getCinemaInfo(this.data.id);
        getCinemaImages(this.data.id);
    },
    goToScreenings: function() {
        var ntf = new kony.mvc.Navigation("frmScreenings");
        ntf.navigate(this.data);
    },
    goBack: function() {
        var ntf = new kony.mvc.Navigation("frmCatalog");
        ntf.navigate();
    }
});
define("frmChosenCinemaControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for imgbackbtn **/
    AS_Image_a12ffde51ed84df486b6a2b854517d8b: function AS_Image_a12ffde51ed84df486b6a2b854517d8b(eventobject, x, y) {
        var self = this;
        return Hamburger.call(this);
    },
    /** onRowClick defined for sgmCinema **/
    AS_Segment_i4dc18eb42c34c7e91d776b40b626acb: function AS_Segment_i4dc18eb42c34c7e91d776b40b626acb(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.segClick.call(this);
    },
    /** onRowClick defined for sgmMenu **/
    AS_Segment_h10bf52c9a044c5b929a8590c552917c: function AS_Segment_h10bf52c9a044c5b929a8590c552917c(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.menuNavigation.call(this);
    }
});
define("frmChosenCinemaController", ["userfrmChosenCinemaController", "frmChosenCinemaControllerActions"], function() {
    var controller = require("userfrmChosenCinemaController");
    var controllerActions = ["frmChosenCinemaControllerActions"];
    for (var i = 0; i < controllerActions.length; i++) {
        var actions = require(controllerActions[i]);
        for (var key in actions) {
            controller[key] = actions[key];
        }
    }
    return controller;
});
