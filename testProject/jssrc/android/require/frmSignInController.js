define("userfrmSignInController", {
    //Connect to remote DB  
    data: {},
    user: {},
    onNavigate: function() {
        this.view.fldEmail.onEndEditing = this.validateEmail;
        this.view.txtSingUp.onTouchEnd = this.navigateToSignUp;
    },
    validateEmail: function() {
        this.view.lblCommonWarning.text = "";
        var email = this.view.fldEmail.text;
        if (validateRegexEmail(email)) {
            this.view.lblEmailWarning.text = "";
            return true;
        } else if (this.view.fldEmail.text === "") {
            this.view.lblEmailWarning.text = "";
        } else {
            this.view.lblEmailWarning.text = "*You entered incorrect email";
        }
    },
    checkUser: function() {
        var self = this;
        var validateResult = this.validate();
        if (validateResult.status === 'true') {
            searchData(this.user);
        }

        function searchData(data) {
            var request = new kony.net.HttpRequest();
            request.onReadyStateChange = httpCallbackHandler;
            request.open(constants.HTTP_METHOD_POST, "https://cityflora.com.ua/login.php");
            request.setRequestHeader("Content-Type", "application/json");
            var sendData = JSON.stringify(data);
            request.send(sendData);
        }

        function httpCallbackHandler() {
            if (this.readyState == constants.HTTP_READY_STATE_DONE) {
                if (this.response === 'null') {
                    //           alert('This user not exist');
                    self.view.lblCommonWarning.text = "*Wrong email or password";
                    return;
                } else if (this.response) {
                    self.view.lblCommonWarning.text = "";
                    var resp = JSON.parse(this.response);
                    var navData = {};
                    navData.userId = resp.id;
                    navData.cityId = resp.city;
                    alert('Login successufully!');
                    var nav = new kony.mvc.Navigation('frmCatalog');
                    nav.navigate(navData);
                }
            }
        }
    },
    validate: function() {
        this.user.email = this.view.fldEmail.text;
        this.user.password = this.view.fldPassword.text;
        if (this.user.email === null || this.user.password === null) {
            return alert('Please fill all rows');
        }
        this.user.status = 'true';
        return this.user;
    },
    navigateToSignUp: function() {
            var signUpForm = new kony.mvc.Navigation("frmSignUp");
            signUpForm.navigate();
        }
        //  Use kony data storage for auth and registration  
        //   data : {},
        //   signInButton: function () {
        //     this.data.password = this.view.fldPasswordReg.text;
        //     this.data.email = this.view.fldLoginReg.text;
        //     if(!this.validate()){
        //       if(!kony.ds.read(this.data.email)){
        //         return alert('User does not exist!');
        //       } else if (kony.ds.read(this.data.email)[0] === kony.crypto.createHash('sha256',this.data.password)){        
        //         var nav = new kony.mvc.Navigation('frmCatalog');      
        //         nav.navigate(kony.ds.read(this.data.email)[1]);
        //         //alert(kony.ds.read(this.data.email)[1]);
        //       } else {
        //         return alert('password or email incorrect');
        //       }
        //     }
        //   },
        //   validate : function(){
        //     if(this.data.email === null || this.data.password === null){
        //       return alert('Please fill all rows!');
        //     }
        //     return false;
        //   }
        //   Connect to Kony Fabric services
        //   signInButton: function () {
        //     var service = kony.sdk.getCurrentInstance().getIntegrationService("connectDb");
        //     var data = {};
        //     data.email = this.view.fldLoginReg.text;
        //     data.password = this.view.fldPasswordReg.text;
        //     if(data.email === null || data.password === null){
        //       return alert('Please fill all rows');
        //     }  
        //     service.invokeOperation(
        //       'cinemadb_users_get',
        //       null,
        //       {'$filter': 'email eq ' + data.email},
        //       function (response) {
        //         if (response.opstatus === 0) {
        //           if(response.users[0].password === undefined  ){
        //             alert(response.users);
        //             return alert('User does not exist');
        //           }          
        //           if (data.password === response.users[0].password) {
        //             var nav = new kony.mvc.Navigation('frmCatalog');
        //             nav.navigate();
        //           } else {
        //             alert('Incorrect Password');
        //           }
        //         }else{
        //         }
        //       },
        //       function (e) {
        //       });
        //   }
});
define("frmSignInControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnSignIn **/
    AS_Button_eae6d7c88ad64342a4c52ab72cff6a1a: function AS_Button_eae6d7c88ad64342a4c52ab72cff6a1a(eventobject) {
        var self = this;
        return self.checkUser.call(this);
    }
});
define("frmSignInController", ["userfrmSignInController", "frmSignInControllerActions"], function() {
    var controller = require("userfrmSignInController");
    var controllerActions = ["frmSignInControllerActions"];
    for (var i = 0; i < controllerActions.length; i++) {
        var actions = require(controllerActions[i]);
        for (var key in actions) {
            controller[key] = actions[key];
        }
    }
    return controller;
});
