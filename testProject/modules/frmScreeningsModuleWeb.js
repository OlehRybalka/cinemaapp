    
      function createLblTitle(n, title) {
        var lblBasic = 
          {
            "id":"lblTitle" + n,
            "skin":"LblHeader2Yellow",
            "text":title,
            "isVisible":true
          };

        var lblLayout =
          {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
          };
        var lblPsp ={wrapping:constants.WIDGET_TEXT_WORD_WRAP};
        var lbl = new kony.ui.Label(lblBasic, lblLayout, lblPsp);
        return lbl;
      }
      
      function createLblTime(r, n, time, data) {
        var lblBasic = 
          {
            "id":"lblSession" + r + n,
            "skin":"lblSessionSkn",
            "text":time,
            "isVisible":true
          };

        var lblLayout =
          {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
          };

        var lblPsp ={wrapping:constants.WIDGET_TEXT_WORD_WRAP};

        var lbl = new kony.ui.Label(lblBasic, lblLayout, lblPsp);
        lbl.width = "50px";
        lbl.height = "35px";
        lbl.data = data;
        lbl.toolTip = data.prices === null ? "нет цены" : data.prices + " грн.";
        function navigate() {
          navigateToSchemeWeb(lbl);
        }
        lbl.onTouchEnd = navigate;
        return lbl;
      }

function navigateToSchemeWeb(obj) {
  var schemeForm = new kony.mvc.Navigation("frmSchemeWeb");
  var data = obj.data;
  schemeForm.navigate(data);
}
      
      function createLbl3D(r, n, session) {
        var text;
        if (session["3d"]) {
          text = " 3D";
        } else {
          text = "";
        }
          
        var lblBasic = 
          {
            "id":"lbl3D" + r + n,
            "skin":"lbl3DSkn",
            "text":text,
            "isVisible":true
          };

        var lblLayout =
          {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
          };
        var lblPsp ={wrapping:constants.WIDGET_TEXT_WORD_WRAP};
        var lbl = new kony.ui.Label(lblBasic, lblLayout, lblPsp);
        lbl.left = "5px";
        lbl.height = "35px";
        return lbl;
      }
      
      function createTimeContainer(r, n) {
        var FlexbasicConfig = {
          "id": "flxContainerTime" + r + n,
          "top": "0",
          "width": "20%",
          "height": "50px",
          "isVisible": true,
          "clipBounds": true,
          "layoutType": kony.flex.FLOW_HORIZONTAL,
          "contentAlignment": constants.CONTENT_ALIGN_CENTER
        };
        var FlexLayoutConfig = {};
        var FlexPSPConfig = {};
        var flexContainerTime = new kony.ui.FlexContainer(FlexbasicConfig, FlexLayoutConfig, FlexPSPConfig);                                                   
        return flexContainerTime;
      }
      
      function createRow(n) {
        var FlexbasicConfig = {
          "id": "flxSessionsRow" + n,
          "top": "10px",
          "width": "100%",
          "height": "50px",
          "skin": "flxSessionsRow",
          "isVisible": true,
          "clipBounds": true,
          "layoutType": kony.flex.FLOW_HORIZONTAL
        };
        var FlexLayoutConfig = {};
        var FlexPSPConfig = {};
        var flexContainerRow = new kony.ui.FlexContainer(FlexbasicConfig, FlexLayoutConfig, FlexPSPConfig);                                                   
        return flexContainerRow;
      }
      
	  function createCinemaContainer(n) {
        var FlexbasicConfig = {
          "id": "flxCinemaContainer" + n,
          "top": "10px",
          "width": "100%",
          "height": "100px",
          "skin": "slFbox",
          "isVisible": true,
          "layoutType": kony.flex.FLOW_VERTICAL
        };
        var FlexLayoutConfig = {};
        var FlexPSPConfig = {};
        var flexCinemaContainer = new kony.ui.FlexContainer(FlexbasicConfig, FlexLayoutConfig, FlexPSPConfig);                                                   
        flexCinemaContainer.autogrowMode = 1;
        return flexCinemaContainer;
      }

      function buildCinemaContainer(obj, index) {
		var cinemaContainer = createCinemaContainer(index);

        var title = createLblTitle(index, obj.title);
        cinemaContainer.addAt(title, 0);

        var rows = buildRows(obj.sessions);
        for (var i = 0; i < rows.length; i++) {
          cinemaContainer.addAt(rows[i], i+1);
        }
        cinemaContainer.height = kony.flex.USE_PREFERED_SIZE;
        return cinemaContainer;
      }
      
      function buildRows(arr) {
        var rows = [];
        var timesInRow = 5;
        var startInd = 0;
        for (var i = 0; startInd < arr.length; i++) {
          var row = createRow(i);
          rows.push(row);
          
          var endInd = startInd + timesInRow;
          var sessionsInRow = arr.slice(startInd, endInd);
         
          for (var j = 0; j < sessionsInRow.length; j++) {
            var timeContainer = createTimeContainer(i, j);
            var time = createLblTime(i, j, sessionsInRow[j].time.slice(0,5), sessionsInRow[j]);
            var threeD = createLbl3D(i, j, sessionsInRow[j]);

            timeContainer.addAt(time,0);
            timeContainer.addAt(threeD,1);
            row.addAt(timeContainer, j);
          }
          startInd += timesInRow;
        }
        return rows;
      }    
      
      function buildScreening(arr) {
        
        var cinemaContainers = [];
        for (var j = 0; j < arr.length; j++) {
          
          if (arr[j].sessions.length > 0) {
            var cinContainer = buildCinemaContainer(arr[j], j);
            cinemaContainers.push(cinContainer);
          }
        }
        
        return cinemaContainers;
      }
