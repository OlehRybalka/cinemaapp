function validateRegexEmail(email) {
  var re = /^\w+([\._%+-]*\w+)*@\w+([\.-]*\w+)*\.[a-z]{2,}$/;
  if (re.test(email)) {
    return true;
  } else return false;
}

function validateRegexPassword(password) {
  var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}$/;
  if (re.test(password)) {
    return true;
  } else return false;
}

function validateFunction(specialFunction, args) {
  if (specialFunction(args)) console.log("success!");
}