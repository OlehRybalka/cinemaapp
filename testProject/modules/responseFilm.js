var responseFilm = 
    {
    "content": [
        {
            "id": 76648,
            "film_id": 49665,
            "person_id": 3608,
            "profession_id": 2,
            "role": "Винс Маккейн",
            "order": 10
        },
        {
            "id": 76643,
            "film_id": 49665,
            "person_id": 8493,
            "profession_id": 3,
            "role": null,
            "order": 5
        },
        {
            "id": 76646,
            "film_id": 49665,
            "person_id": 9890,
            "profession_id": 2,
            "role": "Ролло Ли",
            "order": 8
        },
        {
            "id": 76642,
            "film_id": 49665,
            "person_id": 9890,
            "profession_id": 3,
            "role": null,
            "order": 4
        },
        {
            "id": 76640,
            "film_id": 49665,
            "person_id": 9890,
            "profession_id": 6,
            "role": null,
            "order": 2
        },
        {
            "id": 76647,
            "film_id": 49665,
            "person_id": 9891,
            "profession_id": 2,
            "role": "Вилла Вестон",
            "order": 9
        },
        {
            "id": 76641,
            "film_id": 49665,
            "person_id": 9892,
            "profession_id": 6,
            "role": null,
            "order": 3
        },
        {
            "id": 76645,
            "film_id": 49665,
            "person_id": 13921,
            "profession_id": 5,
            "role": null,
            "order": 7
        },
        {
            "id": 76650,
            "film_id": 49665,
            "person_id": 13993,
            "profession_id": 2,
            "role": "Студент",
            "order": 12
        },
        {
            "id": 76649,
            "film_id": 49665,
            "person_id": 16017,
            "profession_id": 2,
            "role": "Багси Мэлоун",
            "order": 11
        },
        {
            "id": 76644,
            "film_id": 49665,
            "person_id": 16017,
            "profession_id": 3,
            "role": null,
            "order": 6
        },
        {
            "id": 76639,
            "film_id": 49665,
            "person_id": 16849,
            "profession_id": 1,
            "role": null,
            "order": 1
        }
    ],
    "totalPages": 1,
    "totalElements": 12,
    "number": 0,
    "size": 2000,
    "numberOfElements": 12,
    "first": true,
    "last": true,
    "persons": [
        {
            "id": 13921,
            "first_name_orig": "Goldsmith",
            "last_name_orig": "Jerry",
            "birthdate": "1929-02-10",
            "last_name": "Голдсмит",
            "first_name": "Джерри",
            "rating": 0,
            "rating_sum": 0,
            "rating_votes": 0,
            "num_comments": 0,
            "biography": "<p>&nbsp;Американский композитор и дирижёр, классик киномузыки, автор музыки более чем к 250 телевизионным постановкам и кинолентам.</p>\r\n<p><span style=\"font-size: 12px;\">Голдсмит родился в Лос-Анджелесе, штат Калифорния. Сын художницы Тессы (урожденная Раппапорт) и строительного инженера Морриса Голдсмита. В возрасте шести лет научился играть на пианино, в четырнадцать &mdash; изучал композицию, теорию и контрапункт с учителями Якобом Гимпелем и Марио Кастельнуово-Тедеско. Голдсмит учился в университете Южной Калифорнии, где посещал курсы известного композитора Миклоша Рожа, который и вдохновил Голдсмита на написание музыки к кинофильмам.</span></p>\r\n<p>Был номинирован на 18 премий &laquo;Оскар&raquo; (получил один за фильм &laquo;Омен&raquo;), а также стал обладателем четырёх наград &laquo;Эмми&raquo;.</p>\r\n<p>Джерри Голдсмит скончался 21 июля 2004 года в возрасте 75 лет после длительной борьбы с раком.</p>",
            "url": "https://kino-teatr.ua/person/13921.phtml"
        },
        {
            "id": 16017,
            "first_name_orig": "Palin",
            "last_name_orig": "Michael",
            "birthdate": "1943-05-05",
            "last_name": "Пейлин",
            "first_name": "Майкл",
            "rating": 10,
            "rating_sum": 20,
            "rating_votes": 2,
            "num_comments": 0,
            "biography": "<p>&nbsp;Британский актёр, писатель, телевизионный ведущий, один из участников группы &laquo;Монти Пайтон&raquo;. Командор ордена Британской империи. С июля 2009 года по 2012 год Майкл Пейлин являлся президентом Королевского географического общества.</p>\r\n<p><span style=\"font-size: 12px;\">Родился 5 мая 1943 года в Шеффилде, графство Йоркшир (Великобритания) в семье инженера. Закончил Оксфордский университет с учёной степенью по истории. В Оксфорде познакомился с Терри Джонсом, после этой встречи Палин и Джонс начали вести собственное комедийное шоу на канале BBC. Пейлин любит путешествовать, отчёты помещает на свой сайт &laquo;Palin&rsquo;s Travels&raquo; и DVD. Женат, трое детей.</span></p>",
            "url": "https://kino-teatr.ua/person/16017.phtml"
        },
        {
            "id": 16849,
            "first_name_orig": "Schepisi",
            "last_name_orig": "Fred",
            "birthdate": "1939-12-26",
            "last_name": "Скеписи",
            "first_name": "Фред",
            "rating": 0,
            "rating_sum": 0,
            "rating_votes": 0,
            "num_comments": 0,
            "biography": "<p>&nbsp;Австралийский кинорежиссёр, сценарист, продюсер.</p>\r\n<p><span style=\"font-size: 12px;\">Родился в семье Фредерика Томаса Скеписи и Лоретты Эллен (ур. Хэйр). Рано начал заниматься рекламной деятельностью и уже в 1964 году образует собственную кинокомпанию &laquo;The Film House&raquo;. Кроме рекламы, занимается клипмейкерством. В кинематографе дебютирует в 1973 году, сняв эпизод &laquo;Священник&raquo; в киноальманахе &laquo;Либидо&raquo;. Первая же полнометражная картина &laquo;Прибежище дьявола&raquo; приносит номинацию Австралийского киноинститута. В дальнейшем неоднократно номинировался и награждался как на родине, так и за рубежом. В 2007 году возглавлял жюри XXIX Московского международного кинофестиваля. Часто выступает сценаристом и продюсером собственных картин.</span></p>\r\n<p>Женат на Мэри Скеписи. У них двое детей &mdash; Александра и Николас.</p>",
            "url": "https://kino-teatr.ua/person/16849.phtml"
        },
        {
            "id": 9890,
            "first_name_orig": "Cleese",
            "last_name_orig": "John",
            "birthdate": "1939-11-27",
            "last_name": "Клиз",
            "first_name": "Джон",
            "rating": 0,
            "rating_sum": 0,
            "rating_votes": 0,
            "num_comments": 0,
            "biography": "<p>Английский актёр, сценарист, режиссёр и продюсер.</p>\r\n<p><b>Джон Марвуд Клиз </b>родился 27 октября 1939 года в Уэстон-сьюпер-Мэр, графство Сомерсет, Англия, Великобритания, в семье Мюриэль Эвелин и страховщика Реджинальда Клиза. Будущий актёр посещал Подготовительную школу святого Петра, где занимался боксом и крикетом. Позже <b>Джон </b>обучался на юриста в Колледже Даунинга при Кембриджском университете. Как раз там <b>Клиз </b>начал более серьёзно заниматься игрой на сцене. В 1963 году актёр окончил это заведение.</p>\r\n<p>Дебют <b>Джона </b>в кинематографе состоялся в 1962 году с работы над сценарием к 37 эпизодов сериала &quot;Это была неделя, которая была&quot;. А в 1975 году комедийная труппа <b>Клиза </b>&quot;Монти Пайтон&quot;, которая специализировалась на производстве одноимённого шоу, создала свой самый известный полнометражный фильм &quot;Монти Пайтон и священный Грааль&quot;, где сам актёр писал сценарий и сыграл. В 1989 году он номинировался на &quot;Оскар&quot; как сценарист за работу &quot;Рыбка по имени Ванда&quot;.</p>\r\n<p>Крупные проекты в карьере <b>Джона </b>продолжали появляться в период с 1994 по 2002 годы. Это были &quot;Принцесса Лебедь&quot; (1994), &quot;Джордж из джунглей&quot; (1997), &quot;И целого мира мало&quot; (1999), &quot;Крысиные бега&quot;, &quot;Гарри Поттер и философский камень&quot; (оба 2001), &quot;Гарри Поттер и Тайная комната&quot;, &quot;Умри, но не сейчас&quot; (оба 2002).</p>\r\n<p>В промежутке времени между 2003 и 2008 годами фильмографию <b>Клиза </b>пополнили такие картины, как &quot;Ангелы Чарли 2: Только вперёд&quot; (2003), &quot;Вокруг света за 80 дней&quot;, &quot;Шрек 2&quot; (обе 2004), &quot;Вэлиант: Пернатый спецназ&quot; (2005), &quot;Шрек Третий&quot; (2007), &quot;День, когда Земля остановилась&quot; (2008).</p>\r\n<p>На протяжении 2009-2016 годов актёр принял участие в создании лент &quot;Розовая пантера&quot; (2009), &quot;Планета 51&quot; (обе 2009), &quot;Шрек навсегда&quot; (2010), &quot;Самолеты&quot; (2013), &quot;Всё могу&quot; (2015), &quot;Тролли&quot; (2016), а также написал сценарий к мультфильму &quot;Семейка Крудс&quot; (2013).</p>\r\n<p><b>Джон Клиз </b>был четырежды женат: с актрисами Конни Бут (дочь Синтия) и Барбарой Трэнтем (дочь Камилла), а также с психотерапевтом Элис Эйхельбергер, а сейчас состоит в браке с дизайнером украшений Дженнифер Уэйд.</p>",
            "url": "https://kino-teatr.ua/person/9890.phtml"
        },
        {
            "id": 9891,
            "first_name_orig": "Curtis",
            "last_name_orig": "Jamie Lee",
            "birthdate": "1958-11-22",
            "last_name": "Кёртис",
            "first_name": "Джейми Ли",
            "rating": 10,
            "rating_sum": 20,
            "rating_votes": 2,
            "num_comments": 0,
            "biography": "<p>Американская актриса, режиссёр, продюсер и писательница.</p>\r\n<p><b>Джейми Ли Хаден-Кёртис </b>родилась 22 ноября 1958 года в Лос-Анджелесе, штат Калифорния, США, в актёрской семье Джанет Ли и Тони Кёртиса. У неё есть старшая сестра Келли. В 1962 году родители <b>Джейми Ли </b>развелись, а девочка осталась с мамой. В родном городе <b>Кёртис </b>посещала Школу Уэстлейка, Старшую школу Беверли-Хиллз, после чего ещё была подготовительная школа &quot;Choate Rosemary Hall&quot; в Уоллингфорде, штат Коннектикут. Позже будущая актриса обучалась на соцработника в Тихоокеанском университете в Стоктоне. Проучившись там семестр, <b>Джейми Ли</b> выбрала карьеру в кино.</p>\r\n<p>Дебют <b>Кёртис </b>в кинематографе состоялся в 1977 году с эпизодических ролей в сериалах &quot;Медэксперт Куинси&quot;, &quot;Братья Харди и Нэнси Дрю&quot;, &quot;Лейтенант Коломбо&quot;. Уже через год актриса сыграла в культовом ужастике Джона Карпентера &quot;Хэллоуин&quot; (1978).</p>\r\n<p>Успех к <b>Джейми Ли</b> пришёл в период с 1983 по 1992 годы. Тогда она приняла участие в создании таких картин, как &quot;Поменяться местами&quot; (1983), &quot;Рыбка по имени Ванда&quot; (1988), &quot;Моя девочка&quot; (1991), &quot;Вечно молодой&quot; (1992). Тогда же она снялась в неплохом шоу &quot;Только любовь&quot;, за которое получила первый &quot;Золотой глобус&quot;.</p>\r\n<p>За 1994-2003 годы фильмографию <b>Кёртис </b>пополнили проекты &quot;Правдивая ложь&quot; (1994, второй &quot;Золотой глобус&quot;), &quot;Хэллоуин: 20 лет спустя&quot;, &quot;Вирус&quot; (оба 1998), &quot;Чумовая пятница&quot; (2003).</p>\r\n<p>На протяжении 2010-2016 годов актриса была занята в работе над лентами &quot;Снова ты&quot; (2010), &quot;Со склонов Кокурико&quot; (2011), &quot;Вероника Марс&quot; (2014) и над сериалом &quot;Королевы крика&quot; (23 эпизода).</p>\r\n<p><b>Джейми Ли Кёртис </b>замужем за актёром Кристофером Гестом. У пары есть двое приёмных детей: Энни и Томас.</p>",
            "url": "https://kino-teatr.ua/person/9891.phtml"
        },
        {
            "id": 9892,
            "first_name_orig": "Shamberg",
            "last_name_orig": "Michael",
            "birthdate": null,
            "last_name": "Шамберг",
            "first_name": "Майкл",
            "rating": 0,
            "rating_sum": 0,
            "rating_votes": 0,
            "num_comments": 0,
            "biography": "<p>Продюсер</p>",
            "url": "https://kino-teatr.ua/person/9892.phtml"
        },
        {
            "id": 3608,
            "first_name_orig": "Kline",
            "last_name_orig": "Kevin",
            "birthdate": "1947-10-24",
            "last_name": "Клайн",
            "first_name": "Кевин",
            "rating": 0,
            "rating_sum": 0,
            "rating_votes": 0,
            "num_comments": 0,
            "biography": "<p style=\"text-align: justify;\">Американский актер.</p>\r\n<p style=\"text-align: justify;\">Родился в Сент-Луис, Миссури, США.</p>\r\n<p style=\"text-align: justify;\">Получил солидное актерское и  общегуманитарное образование: изучал  драматическое искусство, музыку и  пантомиму в университете штата  Индианы, окончил Джульярдскую  драматическую школу.</p>\r\n<p style=\"text-align: justify;\">В молодости играл в театре, в том числе  на Бродвее. Дважды был  удостоен престижной премии &quot;Тони&quot;. Придя в кино,  быстро утвердился как  актер большого драматического таланта.</p>\r\n<p style=\"text-align: justify;\">В кино дебютировал ролью в телевизионном фильме &quot;The Time of Your Life&quot; (1976).  Первые удачные роли в кинематографе&nbsp;&mdash; фильмы &quot;Выбор Софи&quot; (1982,  номинация на &quot;Золотой глобус&quot;) и &quot;Большое разочарование&quot; (1983).</p>\r\n<p style=\"text-align: justify;\">Стал  очень популярен, сыграв в эксцентрической комедии &quot;Рыбка по имени  Ванда&quot; (1988, &quot;Оскар&quot; за лучшую мужскую роль второго плана).</p>\r\n<p style=\"text-align: justify;\">Снимался  в таких фильмах как &quot;Январский человек&quot; (1989), &quot;Большой каньон&quot;  (1991), &quot;Чаплин&quot; (19920, &quot;Дейв&quot; (1993), &quot;Французский поцелуй&quot; (1995),  &quot;Сон в летнюю ночь&quot; (1999), &quot;Дикий, дикий Запад&quot; (1999), &quot;Жизнь как дом&quot;  (2001), &quot;Розовая пантера&quot; (2006), &quot;Да, нет, наверное...&quot; (2008) и  другие.</p>\r\n<p>&nbsp;</p>",
            "url": "https://kino-teatr.ua/person/3608.phtml"
        },
        {
            "id": 13993,
            "first_name_orig": "Davenport",
            "last_name_orig": "Jack",
            "birthdate": "1973-03-01",
            "last_name": "Девенпорт",
            "first_name": "Джек",
            "rating": 0,
            "rating_sum": 0,
            "rating_votes": 0,
            "num_comments": 0,
            "biography": "<p>&nbsp;Британский киноактёр.</p>\r\n<p><span style=\"font-size: 12px;\">&nbsp;Известен по ролям второго плана в фильмах &laquo;Пираты Карибского моря&raquo; и &laquo;Талантливый мистер Рипли&raquo;. В Великобритании более известен своей ролью в фильме &laquo;This Life&raquo; и в телесериале &laquo;Любовь на шестерых&raquo;. Также он снимался во многих других голливудских фильмах и заслужил репутацию актёра, играющего высокомерных или неуклюжих персонажей. Недавней его съёмкой была роль ведущего персонажа в телесериале &laquo;Город свингеров&raquo;.</span></p>",
            "url": "https://kino-teatr.ua/person/13993.phtml"
        },
        {
            "id": 8493,
            "first_name_orig": "Jones",
            "last_name_orig": "Terry",
            "birthdate": "1942-10-01",
            "last_name": "Джонс",
            "first_name": "Терри",
            "rating": 0,
            "rating_sum": 0,
            "rating_votes": 0,
            "num_comments": 0,
            "biography": "<p style=\"text-align: justify;\">Британский актер, сценарист, продюсер и режиссер.</p>\r\n<p style=\"text-align: justify;\">Родился в Колвин-Бэй, Уэльс, Великобритания. </p>\r\n<p style=\"text-align: justify;\">Полное имя - <b>Теренс Грехем Перри Джонс</b>. </p>",
            "url": "https://kino-teatr.ua/person/8493.phtml"
        }
    ],
    "professions": [
        {
            "id": 1,
            "name": "Режиссер",
            "names": "Режиссеры",
            "order": 1
        },
        {
            "id": 2,
            "name": "Актер",
            "names": "Актеры",
            "order": 2
        },
        {
            "id": 3,
            "name": "Сценарист",
            "names": "Сценаристы",
            "order": 3
        },
        {
            "id": 5,
            "name": "Композитор",
            "names": "Композиторы",
            "order": 5
        },
        {
            "id": 6,
            "name": "Продюсер",
            "names": "Продюсеры",
            "order": 6
        }
    ]
};