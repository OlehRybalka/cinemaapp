// GET FILMS
var chosenCityId;
if(chosenCityId === undefined){
  chosenCityId = 1;
}

var getFilmsWeb =  function  (form,cityId,cinemaId){
  var countFilms = 0;
  var importantData = [];  

  if(cinemaId) {
    var searchDataFilmWebWithCinema = function(){
      var request = new kony.net.HttpRequest();
      request.onReadyStateChange=httpCallbackHandler;
      request.open(constants.HTTP_METHOD_GET, "http://cors-anywhere.herokuapp.com/http://kino-teatr.ua:8081/services/api/cinema/"+cinemaId+"/shows?apiKey=pol1kh111&size=10&detalization=FULL");
      request.setRequestHeader("From","Dp-144 KONY");
      request.send();
    };
    searchDataFilmWebWithCinema();  

  } else {
    var searchDataFilmWeb = function(){
      var request = new kony.net.HttpRequest();
      request.onReadyStateChange=httpCallbackHandler;
      request.open(constants.HTTP_METHOD_GET, "http://cors-anywhere.herokuapp.com/http://kino-teatr.ua:8081/services/api/city/"+cityId+"/shows?apiKey=pol1kh111&size=10&detalization=FULL");
      request.setRequestHeader("From","Dp-144 KONY");
      request.send();
    };
    searchDataFilmWeb();
  }

  function httpCallbackHandler(){
    if(this.readyState == constants.HTTP_READY_STATE_DONE){ 
      try {
        var resp = JSON.parse(this.response);
        parseJsonResponse(resp);
        getPoster();
        form.view.sgmFilms.setData(importantData);   

      } catch (e) {
        alert("Error in searchData : " + e.message);  
      }
    }
  }

  function parseJsonResponse(js){

    countFilms = js.films.length;
    if(countFilms === 0){
      form.view.lblError.text = 'К сожалению, в данном кинотеатре фильмов нет!';
    } else {
      form.view.lblError.text = '';
    }


    for(var i = 0; i < countFilms; i++){
      var film = {
        id: js.films[i].id,
        cinemaId: cinemaId,
        lblTitle: js.films[i].title,
        lblTitleOrigin: js.films[i].title_orig,
        lblYear: js.films[i].year,
        lblAgeLimit: js.films[i].age_limit + '+',
        lblRating: 'Рейтинг: ' + js.films[i].tmdb_rating + " |",
        lblVotes: 'Голосов: ' + js.films[i].tmdb_votes,
        imgFilm: null,
      };

      if(film.lblAgeLimit === 'null+'){film.lblAgeLimit = '0+';}
      if(js.films[i].tmdb_rating === null){film.lblRating = 'Рейтинг: 0' + " |";}
      if(js.films[i].tmdb_votes === null){film.lblVotes = 'Голосов: 0';}

      form.view.sgmFilms.removeAll();
      importantData.push(film);
    }
  }

  function getPoster() {
    for (var key in importantData){
      film = importantData[key];
      var src ='http://kino-teatr.ua:8081/services/api/film/' + film.id + '/poster?apiKey=pol1kh111&width=400&height=400&ratio=1';
      film.imgFilm = {src:src};       
    }
  }
};

// GET CINEMAS
var getCinemasWeb =  function  (form, cityId){
  var cinema, countCinemas;
  var importantDataCinema = [];
  searchDataCinemaWeb();

  function searchDataCinemaWeb(city){
    var request = new kony.net.HttpRequest();
    request.onReadyStateChange=httpCallbackHandlerCinema;
    request.open(constants.HTTP_METHOD_GET, "http://cors-anywhere.herokuapp.com/http://kino-teatr.ua:8081/services/api/city/"+cityId+"/cinemas?apiKey=pol1kh111&size=1%20HTTP/1.1");
    request.setRequestHeader("From","Dp-144 KONY");
    request.send();
  }

  function httpCallbackHandlerCinema(){
    if(this.readyState == constants.HTTP_READY_STATE_DONE){ 
      try {
        var resp = JSON.parse(this.response);
        parseJsonResponse(resp);
        form.view.sgmCinemas.setData(importantDataCinema);
      } catch (e) {
        //alert("Error in searchDataCinema : " + e.message);
      }
    }
  }

  function parseJsonResponse(js){
    countCinemas = js.content.length;

    for(var i = 0; i < countCinemas; i++){
      var cinema = {
        id: js.content[i].id,
        lblCinema: js.content[i].name,
        lblCinemaAdress: 'Адресс: ' + js.content[i].address,
        lblCinemaPhone: 'Телефон: ' + js.content[i].phone
      };
      importantDataCinema.push(cinema);
    }
  }
};