// var frmSeats = _kony.mvc.GetController("frmSeats", true);
// var sId;

// function getSchemeForAndroid (sessionId) {
//   var request = new kony.net.HttpRequest();
//   sId = sessionId;
//   alert("this is getSchemeForAndroid");
//   request.onReadyStateChange = schemeResponseAndroid;
//   request.open(constants.HTTP_METHOD_GET, "http://kino-teatr.ua:8081/services/api/showtime/" + sessionId + "/schema?apiKey=pol1kh111");
//   request.send();
// }


// function getSchemeForIOS (sessionId) {
//   var request = new kony.net.HttpRequest();
//   sId = sessionId;
//   request.onReadyStateChange = schemeResponseIOS;
//   request.open(constants.HTTP_METHOD_GET, "http://kino-teatr.ua:8081/services/api/showtime/" + sessionId + "/schema?apiKey=pol1kh111");
//   request.send();
// }

// function schemeResponseIOS () {
//   if (this.readyState == constants.HTTP_READY_STATE_DONE) {
//     //if (this.response !== "Showtime with id '" + sId + "' not found") {
//     try {
//       frmSeats.view.seatsLayoutFlex.removeAll();
//       frmSeats.view.seatsLayoutFlex.width = kony.os.deviceInfo().screenWidth;
//       var rows = this.response.sectors[0].rows;
//       var seat;
//       var rowWidth;
//       for (var i = 0; i < rows.length; i++) {
//         var seats = this.response.sectors[0].rows[i].places;
//         var rowStr = "ряд " + (i + 1) + " ";
//         var rowId = rowNumberCreate (i, rowStr);
//         var rowFlex = flexCreate(i);
//         frmSeats.view.seatsLayoutFlex.add(rowFlex);
//         frmSeats.view[rowFlex.id].add(rowId); 
//         for (var j = 0; j < seats.length; j++) {
//           seat = seatCreate(seats[j].number, seats[j].ids[0]);
//           var seatFree = checkSeatStatus(seats[j].status, seat);
//           selectSeatHandler(seat, seatFree);
//           frmSeats.view[rowFlex.id].add(seat); 
//         }
//         rowWidth = getRowWidth (Number(seat.width), Number(seats.length), Number(rowId.width), Number(seat.left), Number(seat.right));
//         frmSeats.view[rowFlex.id].width = rowWidth;
//         mainFlexWidthCorrection(rowWidth);
//       }
//       alignRows(rows.length);
//       seatsShowSessionInfo(this.response.site_name, this.response.date);
//       frmSeats.view.goBackLbl.onTouchStart = goBackToChooseSession;
//     } catch (e) {
//       //alert ("Error - " + e.message);
//     }
//   }
// //   } else {
// //     frmSeats.view.cinemaTitle.text = "К сожалению на данный сеанс невозможно купить билеты. Обратитесь в кассу кинотеатра";
// //     frmSeats.view.cinemaTitle.contentAlignment = constants.CONTENT_ALIGN_CENTER;
// //     frmSeats.view.FilmTitle.setVisibility(false);
// //     frmSeats.view.sessionTime.setVisibility(false);
// //     frmSeats.view.schemeFlexScroll.setVisibility(false);
// //   }
// }


// function schemeResponseAndroid () {
//   if (this.readyState == constants.HTTP_READY_STATE_DONE) {
//     try {
//       frmSeats.view.seatsLayoutFlex.removeAll();
//       frmSeats.view.seatsLayoutFlex.width = kony.os.deviceInfo().screenWidth;
//       alert("Your device : " + kony.os.deviceInfo().name);
//       alert(this.response);
//       var rows = this.response.sectors[0].rows;
//       var seat;
//       var rowWidth;
//       var rowFlex;
//       for (var i = 0; i < rows.length; i++) {
//         var seats = this.response.sectors[0].rows[i].places;
//         var rowStr = "ряд " + (i + 1) + " ";
//         var rowId = rowNumberCreate (i, rowStr);
//         rowFlex = flexCreate(i);
//         frmSeats.view.seatsLayoutFlex.add(rowFlex);
//         frmSeats.view[rowFlex.id].add(rowId); 
//         for (var j = 0; j < seats.length; j++) {
//           seat = seatCreate(seats[j].number, seats[j].ids[0]);
//           var seatFree = checkSeatStatus(seats[j].status, seat);
//           //selectSeatHandler(seat, seatFree);
//           frmSeats.view[rowFlex.id].add(seat); 
//         }
//         rowWidth = getRowWidth (Number(seat.width), Number(seats.length), Number(rowId.width), Number(seat.left), Number(seat.right));
//         //frmSeats.view[rowFlex.id].width = rowWidth;
//         //mainFlexWidthCorrection(rowWidth);
//       }
//       //alignRows(rows.length);
//       seatsShowSessionInfo(this.response.site_name, this.response.date);
//     } catch (e) {
//       alert ("Error - " + e.message);
//     }
//   }
// }


// function seatCreate (seatNumber, seatId) {
//   var LabelbasicConfig = {
//     "id":  "seatLabel" + seatId,
//     "isVisible": true,
//     "skin": "PurpleBorders2",
//     "text": seatNumber,
//   };
//   var LabelLayoutConfig= {
//     "left" : "1",
//     "right" : "1",
//     "width" : "30",
//     "height" : "30",
//     "widthInPixel": true,
//     "heightInPixel": true,
//     "contentAlignment": constants.CONTENT_ALIGN_CENTER,
//     "marginInPixel": true,
//     "paddingInPixel": true,
//   };
//   var LabelPSPConfig = {};
//   var seat = new kony.ui.Label(LabelbasicConfig, LabelLayoutConfig, LabelPSPConfig);
//   if (kony.os.deviceInfo().name == "android" || kony.os.deviceInfo().name == "android Simulator") {     
//     seat.width = 10;
//     seat.height = 100;
//     seat.text = seatNumber;
//     seat.isVisible = true;
//   }
//   return seat;
// }


// function flexCreate (flexId) {
//   var FlexbasicConfig = {
//     "id": "rowFlex" + flexId,
//     "height": "80px",
//     "zIndex": 1,
//     "isVisible": true,
//     "skin" : "slFbox",
//     "layoutType" : kony.flex.FLOW_HORIZONTAL,
//   };
//   var FlexLayoutConfig = {};
//   var FlexPSPConfig = {};
//   var flexContainer = new kony.ui.FlexContainer(FlexbasicConfig, FlexLayoutConfig, FlexPSPConfig);
//   if (kony.os.deviceInfo().name == "android" || kony.os.deviceInfo().name == "android Simulator") {
//     flexContainer.width = 100;
//     flexContainer.layoutType = kony.flex.FLOW_HORIZONTAL;
//     flexContainer.isVisible = true;
//   }
//   return flexContainer;
// }


// function rowNumberCreate (rowId, rowNum) {
//   var LabelbasicConfig = {
//     "id":  "rowNum" + rowId,
//     "isVisible": true,
//     "skin": "rowIdSkin",
//     "text": rowNum,
//   };
//   var LabelLayoutConfig= {
//     "width" : "40",
//     "height" : "25",
//     "heightInPixel": true,
//     "contentAlignment": constants.CONTENT_ALIGN_CENTER,
//     "marginInPixel": true,
//     "paddingInPixel": true,
//   };
//   var LabelPSPConfig = {};
//   var seat = new kony.ui.Label(LabelbasicConfig, LabelLayoutConfig, LabelPSPConfig);
//   if (kony.os.deviceInfo().name == "android" || kony.os.deviceInfo().name == "android Simulator") {
//     seat.height = 100;
//     seat.text = rowNum;
//     seat.contentAlignment = constants.CONTENT_ALIGN_CENTER;
//   } 
//   return seat;
// }


// function getRowWidth (seatWidth, numberOfSeats, rowIdWidth, seatLeft, seatRight) {
//   var rowWidth;
//   if (kony.os.deviceInfo().name == "android" || kony.os.deviceInfo().name == "android Simulator") {
//     rowWidth = numberOfSeats * seatWidth + rowIdWidth;
//   } else {
//     rowWidth = numberOfSeats * (seatWidth + seatLeft + seatRight) + rowIdWidth;
//   }      
//   return rowWidth;
// }


// function alignRows (numberOfRows) {
//   for (var i = 0; i < numberOfRows; i++) {
//     if (frmSeats.view["rowFlex"+i].width < frmSeats.view.seatsLayoutFlex.width) {
//       frmSeats.view["rowFlex"+i].left = (frmSeats.view.seatsLayoutFlex.width - frmSeats.view["rowFlex"+i].width) / 2;
//     }
//   }
// }


// function mainFlexWidthCorrection (rowWidth) {
//   if (rowWidth >= kony.os.deviceInfo().screenWidth && rowWidth >= frmSeats.view.seatsLayoutFlex.width && kony.os.deviceInfo().name == "iPhone") {
//     frmSeats.view.seatsLayoutFlex.width = rowWidth + 30;
//     frmSeats.view.screenFlex.width = rowWidth  + 30;
//   } else if (rowWidth >= kony.os.deviceInfo().screenWidth && rowWidth >= frmSeats.view.seatsLayoutFlex.width && kony.os.deviceInfo().name == "android") {
//     frmSeats.view.seatsLayoutFlex.width = rowWidth + 2;
//     frmSeats.view.screenFlex.width = rowWidth  + 2;
//   }
// }


// function selectSeatHandler(seat, seatFree) {
//   var doubletp={fingers :1, taps :2};
//   var longPress = {pressDuration : 1};
//   seat.addGestureRecognizer(1, doubletp, onDoubleTap) ;
//   seat.addGestureRecognizer(3, longPress, goBackToChooseSession) ;
//   function onDoubleTap() {
//     if (seat.skin == "PurpleBorders2" && seatFree) {
//       seat.skin = "seatSelect1";
//     } else if (seatFree) {
//       seat.skin = "PurpleBorders2";
//     }
//   }
// }

// function checkSeatStatus(status, seat) {
//   if (status !== "0") {
//     seat.skin = "seatReserved1";
//     return false;
//   } else {
//     return true;
//   }
// }

// function seatsShowSessionInfo (cinema, time) {
//   frmSeats.view.FilmTitle.text = "Film Title";
//   frmSeats.view.cinemaTitle.text = cinema;
//   //frmSeats.view.cinemaTitle.contentAlignment = constants.CONTENT_ALIGN_RIGHT;
//   frmSeats.view.sessionTime.text = time;
//   //frmSeats.view.FilmTitle.setVisibility(true);
//   //frmSeats.view.sessionTime.setVisibility(true);
//   //frmSeats.view.schemeFlexScroll.setVisibility(true);
// }

// function goBackToChooseSession() {
//   var ntf = new kony.mvc.Navigation("frmChooseSession");
//   ntf.navigate();
// }