var apiKey = "pol1kh111";

function getRequestParams() {
  var frm = _kony.mvc.GetController("frmScreenings", true);
  
  var prevform = frm.getPrevForm();
  var params = {
    request: null,
    successFunc: null,
    date: frm.getSearchDate()
  };
  if (prevform === "frmChosenFilm") {
    params.request = "http://kino-teatr.ua:8081/services/api/city/" + frm.data.cityId + "/film/" + frm.data.id + "/shows?apiKey=" + 
                      apiKey + "&size=10000&date=" + params.date + "&detalization=FULL";
    params.successFunc = "successFilm";
  } else {
    params.request = "http://kino-teatr.ua:8081/services/api/cinema/" + frm.data.id + "/shows?apiKey=" + 
                      apiKey + "&size=10000&date=" + params.date + "&detalization=FULL";
    params.successFunc = "successCinema";
  }
  return params;
}


/* Fill up HEAD */

function sendRequestHead() {
  var frm = _kony.mvc.GetController("frmScreenings", true);
  var params = getRequestParams();

  function searchData() {
    try {
      var request = new kony.net.HttpRequest();
      request.onReadyStateChange=httpCallbackHandler;
      request.open(constants.HTTP_METHOD_GET, params.request);
      request.setRequestHeader("From","Dp-144 KONY");
      request.send();
    } catch(e) {
      alert("Error: " + e.message);
    }
  }
    
  function httpCallbackHandler() {
    if(this.readyState == constants.HTTP_READY_STATE_DONE){
      var response = this.response;
      frm.view.txtChosenItemScrTitle.text = "";
      frm.view.imgChosenItemScr.isVisible = false;
      
      if (response.numberOfElements > 0) {
        if (params.successFunc === "successFilm") {
          frm.successFilm(response);
        } else {
          frm.successCinema(response);
        }
        sendRequestScreenings();
        
        frm.view.calendarDate.onSelection = sendRequestScreenings;
      } else {
        frm.view.txtChosenItemScrTitle.text = "Нет сеансов";
        frm.view.txtChosenItemScrTitle.left = "15dp";
      }
    }
  }
	
  searchData();    
}


/** Fill up SCREENINGS */

function sendRequestScreenings() {
  var frm = _kony.mvc.GetController("frmScreenings", true);
  frm.view.sgmCollection.removeAll();
  var params = getRequestParams();
    
  function searchData() {
    try {
      var request = new kony.net.HttpRequest();
      request.onReadyStateChange=httpCallbackHandler;
      request.open(constants.HTTP_METHOD_GET, params.request);
      request.setRequestHeader("From","Dp-144 KONY");
      request.send();
    } catch(e) {
      alert("Error: " + e.message);
    }
  }
    
  function httpCallbackHandler() {
    if(this.readyState == constants.HTTP_READY_STATE_DONE){   
      var response = this.response;
      if (response.content.length > 0) {   
      	frm.fillUpSegment(response, params);
      } else {
        var data = [];
        data.push(
        { txtSubitemTitle: "Нет сеансов" }
        );
        frm.view.sgmCollection.setData(data);
      }
    }
  }
    
  searchData();
}


/** Fill up SCREENINGS by film */

function parseFilmResponse(resp) {
  var data = [];
  for (var c = 0; c < resp.cinemas.length; c++) {
    var sessions = getSessionsByCinema(resp.cinemas[c].id, resp);
    
	sessions = filterAvailableTime(sessions);
    
    if (sessions.length > 0 && sessions !== undefined) {
      data.push({
           sessions: sessions,
           title: resp.films[0].title || null,
           txtSubitemTitle: resp.cinemas[c].name,
           txtTime: getSessionsText(sessions)
      });
    }
  }
  return data;
}

function getSessionsByCinema(sinemaId, resp) {
  
  var sessions = [];
  var halls = getHalls(resp.halls, sinemaId);
  for (var k = 0; k < halls.length; k++) {
    var sessionsByHall = getSessionsByHalls(resp.content, halls[k].id);
    for (var i = 0; i < sessionsByHall.length; i++) {
      sessionsByHall[i].hallName = halls[k].name;
    }
    sessions = sessions.concat(sessionsByHall);
  }
  return sessions;
}

function filterAvailableTime(arr) {
  var frm = _kony.mvc.GetController("frmScreenings", true);
  var availSessions = arr.filter(function(elem) {
    return (+getDateFromStr(frm.getSearchDate(), elem.time) > +new Date());
  });
  return availSessions;
}

function getHalls(arr, id) {
  arr = arr.filter(function(elem) {
    return elem.cinema_id === id;
  });
  var hallsList = arr.map(function(elem) {
    return {
      id: elem.id,
      name: elem.name
    };
  });
  return hallsList;
}

function getSessionsByHalls(arr, hall) {
  var timesByHall = arr.filter(function(elem) {
    return elem.hall_id === hall;
  });
  var timeListByHall = timesByHall.reduce(concatTimes, []);
  return timeListByHall;
}


/** Fill up SCREENINGS by cinema */

function parseCinemaResponse(resp) {
  var data = [];
  for (var c = 0; c < resp.films.length; c++) {
    var sessions = getSessionsByFilm(resp.films[c].id, resp);
    
    sessions = filterAvailableTime(sessions);
    
    if (sessions.length > 0 && sessions !== undefined) {
       data.push({
            sessions: sessions,
            title: resp.cinemas[0].name || null,
            txtSubitemTitle: resp.films[c].title,
            txtTime: getSessionsText(sessions),
         	imgSubitem: {src:"http://kino-teatr.ua:8081/services/api/film/"+ resp.films[c].id + "/poster?apiKey=" + apiKey + "&width=300&height=400&ratio=1"}
       });
    }
  }
  return data;
}
      
function getSessionsByFilm(filmId, resp) {

  var timesByFilm = resp.content.filter(function(elem) {
    return elem.film_id == filmId;
  });
  var timesByFilmWithHall = addHallName(timesByFilm, resp);
  var timeListByFilm = timesByFilmWithHall.reduce(concatTimes, []);
  
  return timeListByFilm;
}
    
function addHallName(cont, resp) {
  for (var i = 0; i < cont.length; i++) {
    var hallName = getHallName(cont[i].hall_id, resp);
    for (var j = 0; j < cont[i].times.length; j++) {
      cont[i].times[j].hallName = hallName;
    }
  }
  return cont;
}

function getSessionsText(arr) {
  arr = arr.sort(sortByTime);
  var timeText = arr.reduce(function(text, elem) {
    return text + elem.time.slice(0, 5) + " ";
  }, "");
  return timeText;
}

function sortByTime(a, b) {
  var first = a.time.slice(0, 2) + a.time.slice(3, 6);
  var second = b.time.slice(0, 2) + b.time.slice(3, 6);
  if (first > second) return 1;
  if (first < second) return -1;
}

function concatTimes(timeList, elem) {
  var times = elem.times;
  return timeList.concat(times);
}

function getHallName(hallId, resp) {
  for (var i = 0; i < resp.halls.length; i++) {
    if (resp.halls[i].id === hallId) {
      return resp.halls[i].name;
    }
  }
}

function getDateFromStr(day, time) {
  return new Date(day.slice(0,4), day.slice(5,7)-1, day.slice(8), time.slice(0,2), time.slice(3,5), time.slice(6));
}

function getCinemaName(resp, id) {
    for (var i = 0; i < resp.length; i++) {
      if (resp[i].id === id) {
        return resp[i].name;
      }
    }
  }
