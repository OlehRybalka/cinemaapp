function HamburgerMenuNavigation (){
  var getForm = kony.application.getCurrentForm().id;
  var chosenForm = _kony.mvc.GetController(getForm, true);
  var segMenuData = chosenForm.view.flxslidemenu.flxMenu.sgmMenu.data;
  var segMenuIndex = chosenForm.view.flxslidemenu.flxMenu.sgmMenu.selectedRowIndex;

  var obj = ['frmCatalog', 'frmHistory', 'frmAboutUs', 'frmAccountSettings', 'frmStart'];
  for(var i = 0; i < segMenuData.length; i++){
    for(var j = 0; j < obj.length; j++){
      segMenuData[i].frmName = obj[i];
    }
  }

  var nav = new kony.mvc.Navigation(segMenuData[segMenuIndex[1]].frmName);
  nav.navigate();
  HamburgerFechar();
}