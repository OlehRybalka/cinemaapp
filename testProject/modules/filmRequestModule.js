function getFilmInfo (filmId) {   
  var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
  searchData();

  function searchData () {
    var request = new kony.net.HttpRequest();
    request.onReadyStateChange = httpCallbackHandler;
    request.open(constants.HTTP_METHOD_GET, "http://kino-teatr.ua:8081/services/api/film/" + filmId + "?apiKey=pol1kh111");
    request.send();
  }

  function httpCallbackHandler () {
    if (this.readyState == constants.HTTP_READY_STATE_DONE) {
      if (this.response != "Film with id '" + filmId + "' not found") {
        frmChosenFilm.view.filmTitleLabel.text = this.response.title;   
        frmChosenFilm.view.durationDescription.text = this.response.duration + " мин.";
        frmChosenFilm.view.ratingDescription.text = this.response.rating.toFixed(1);
        frmChosenFilm.view.yearDescription.text = "" + this.response.year;
        frmChosenFilm.view.plotDescription.text = this.response.description;
        for (var i = 0; i < this.response.genres.length; i++) {
          frmChosenFilm.view.genreDescription.text += this.response.genres[i].name + "\n";
        }
        frmChosenFilm.view.ticketsBtnFlex.setVisibility(true);
  		frmChosenFilm.view.descriptionFlex.setVisibility(true);
      } else {
        filmNotFound();
      }
    }
  }  
}



function getFilmImages (filmId) {
    var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
  	searchData();

  function searchData () {
    var request = new kony.net.HttpRequest();
    request.onReadyStateChange = httpCallbackHandler;
    request.open(constants.HTTP_METHOD_GET, "http://kino-teatr.ua:8081/services/api/film/" + filmId + "/images?apiKey=pol1kh111");
    request.send();    
  }

  function httpCallbackHandler () {
    if (this.readyState == constants.HTTP_READY_STATE_DONE) {
      if (this.response != "Film with id '" + filmId + "' not found") {
        if (this.response.numberOfElements !== 0) {
          var imgObj = {};
          var imgArr = [];
          for (var i = 0; i < this.response.numberOfElements; i++) {
            imgObj.src = "http://kino-teatr.ua:8081/services/api/film/image/" + this.response.content[i].id + "?apiKey=pol1kh111&width=300&height=400&ratio=1";
            imgArr.push({filmImg: {src: imgObj.src}});  
          }
          frmChosenFilm.view.imgSegmentSlider.setData(imgArr);
          frmChosenFilm.view.imgSegmentFlex.setVisibility(true);
        } else {
          frmChosenFilm.view.imgSegmentFlex.setVisibility(false);
        }
      } else {
        filmNotFound();
      }
    }
  }  
}



function getActors (filmId) {
  var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
  searchData();

  function searchData () {
    var request = new kony.net.HttpRequest();
    request.onReadyStateChange = httpCallbackHandler;
    request.open(constants.HTTP_METHOD_GET, "http://kino-teatr.ua:8081/services/api/film/" + filmId + "/persons?apiKey=pol1kh111&size=100&detalization=FULL");
    request.send();
  }

  function httpCallbackHandler () {
    if (this.readyState == constants.HTTP_READY_STATE_DONE) {
      if (this.response != "Film with id '" + filmId + "' not found") {
        var resp = this.response;
        if (!resp) resp = {};
        resp.persons = resp.persons || [];
        resp.content = resp.content || [];
        resp.professions = resp.professions || [];

        resp = addProfessionId(resp);
        var persons = [];
        var nomProf = 5;
        for (var i = 0; i <= nomProf; i++) {
          var profId = i + 1;
          var personsByProf = filterByProfession(resp.persons, profId) || [];
          personsByProf = formatToSegmentData(personsByProf);
          persons.push(personsByProf);
        }

        frmChosenFilm.fillUpPersons(persons);

      } else {
        filmNotFound();
      }
    }
  }  
}

function formatToSegmentData(arr) {
  if (arr.length > 0) {
    return arr.map(function(elem) {
      return {
        id: elem.id,
        imgPerson: {src:"http://kino-teatr.ua:8081/services/api/person/" + elem.id + "/photo?apiKey=pol1kh111&width=300&height=400&ratio=1"},
        lblPersonName: elem.first_name + " " + elem.last_name,
        lblPersonOriginName: elem.last_name_orig + " " + elem.first_name_orig,
        lblPersonRaiting: "Рейтинг: " + elem.rating + "/10"
      };
    });
  } else {
    return [{
      lblPersonName: "Данных нет"
    }];
  }
}
  
function filterByProfession(persons, professionId) {
  var filteredData = [];
  filteredData = persons.filter(function(elem) {
    return +elem.profession_id === professionId;
  });
  return filteredData;
}

function addProfessionId(resp) {
  var professionIdByPersonId = getProfessionIdByPersonId(resp.content);
  if (resp.persons.length > 0) {
    resp.persons.forEach(function(person) {
      person.profession_id = professionIdByPersonId[person.id];
    });
  }
  return resp;
}

function getProfessionIdByPersonId(content) {
  var professionIdByPersonId = {};
  if (content.length > 0) {
    professionIdByPersonId = content.reduce(function(professions, elem) {
      professions[elem.person_id] = elem.profession_id;
      return professions;
    }, {});
  }
  return professionIdByPersonId;
}

function filmNotFound () {
  var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
  frmChosenFilm.view.filmTitleLabel.text = "К сожалению, фильм не найден.";
  frmChosenFilm.view.ticketsBtnFlex.setVisibility(false);
  frmChosenFilm.view.descriptionFlex.setVisibility(false);
}