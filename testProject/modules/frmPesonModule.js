
function requestAPI(url, httpCallbackHandler){
  var request = new kony.net.HttpRequest();
  request.onReadyStateChange = function() {
    if (this.readyState == constants.HTTP_READY_STATE_DONE) {
      httpCallbackHandler(this.response);
    }
  };      
  request.open(constants.HTTP_METHOD_GET, url);
  request.send();
}

function getPersonPhotos(id) {
  var frmPerson = _kony.mvc.GetController("frmPerson", true);
  var url = "http://kino-teatr.ua:8081/services/api/person/" + id + "/photos?apiKey=pol1kh111";
  var httpCallbackHandler = function (resp) {                
    if (resp.numberOfElements !== 0) {
      var personPhotos = [];
      resp.content.forEach(function(elem) {
        personPhotos.push({imgPerson: {src: "http://kino-teatr.ua:8081/services/api/person/photo/" + elem.id + "?apiKey=pol1kh111&width=300&height=400&ratio=1"}});
      });

      frmPerson.fillUpSlider(personPhotos);
    } else {
      frmPerson.hideSlider();
    }
  };
  
  requestAPI(url, httpCallbackHandler);
}

function getPersonInfo(id) {
  var frmPerson = _kony.mvc.GetController("frmPerson", true);
  
  var url = "http://kino-teatr.ua:8081/services/api/person/" + id + "?apiKey=pol1kh111";
  var httpCallbackHandler = function (resp) {
    if (resp != "Person with id '" + id + "' not found") {
      frmPerson.fillUpPersonInfo(resp);
    } else {
      frmPerson.personNotFound();
    }
  };
  
  requestAPI(url, httpCallbackHandler);
}

function searchDataByFilm (frmPerson, id, rowIndex) {
  var url = "http://kino-teatr.ua:8081/services/api/film/" + id + "?apiKey=pol1kh111";
  
  var httpCallbackHandler = function(resp) {
    if (resp !== "Film with id '" + id + "' not found") {
      var data = {
        lblFilm: resp.title,
        filmId: id
      };
      frmPerson.view.segFilms.addDataAt(data, rowIndex);
    }      
  };
  
  requestAPI(url, httpCallbackHandler);
}

function getFilmsByPerson(id) {
  var frmPerson = _kony.mvc.GetController("frmPerson", true);  
  frmPerson.view.segFilms.removeAll();
  var url = "http://kino-teatr.ua:8081/services/api/person/" + id + "/trailers?apiKey=pol1kh111";
  
  var httpCallbackHandler = function (resp) {
    if (resp.numberOfElements !== 0) {
      var filmsIds = [];
      var rowIndex = 0;
      for (var i = 0; i < resp.content.length; i++) {
        var film_id = resp.content[i].film_id; 
        if (filmsIds.indexOf(film_id) == -1) {
          filmsIds.push(film_id);
          searchDataByFilm(frmPerson, film_id, rowIndex++);
        }
      }        
    }
  };
  
  requestAPI(url, httpCallbackHandler);
}

