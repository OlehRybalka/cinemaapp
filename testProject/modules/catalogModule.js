var getFilmsAndCinemas = function  (cityId){
  var frmCatalog = _kony.mvc.GetController('frmCatalog', true);
  var film;
  var countFilms = 0;
  var importantData = [];  var importantDataCinema = [];
  var cinema;
  
  searchDataCinema();
  searchData();
  
  function searchData(){
    var request = new kony.net.HttpRequest();
    request.onReadyStateChange=httpCallbackHandler;
    request.open(constants.HTTP_METHOD_GET, "http://kino-teatr.ua:8081/services/api/city/" + cityId + "/shows?apiKey=pol1kh111&size=10&detalization=FULL");
    request.setRequestHeader("From","Dp-144 KONY");
    request.send();
  }

  function httpCallbackHandler(){
    if(this.readyState == constants.HTTP_READY_STATE_DONE){ 
      try {
      parseJsonResponse(this.response);
      getPoster();
   	  frmCatalog.view.sgmFilmCollection.setData(importantData);
      
      } catch (e) {
      	//alert("Error in searchData : " + e.message);  
      }
      
      function parseJsonResponse(js){
        countFilms = js.films.length;
        for(var i = 0; i < countFilms; i++){
          var film = {
            id: js.films[i].id,
            txtFilm: js.films[i].title,
            imgFilm: null,
          };
          importantData.push(film);
        }
      }

      function getPoster() {
        for (var key in importantData){
          film = importantData[key];
          var src ='http://kino-teatr.ua:8081/services/api/film/' + film.id + '/poster?apiKey=pol1kh111&width=400&height=400&ratio=1';
          film.imgFilm = {src:src};       
        }
      }
    }
  }
  

  function searchDataCinema(){
    var request = new kony.net.HttpRequest();
    request.onReadyStateChange=httpCallbackHandlerCinema;
    request.open(constants.HTTP_METHOD_GET, "http://kino-teatr.ua:8081/services/api/city/" + cityId + "/cinemas?apiKey=pol1kh111&size=1%20HTTP/1.1");
    request.setRequestHeader("From","Dp-144 KONY");
    request.send();
  }

  function httpCallbackHandlerCinema(){
    if(this.readyState == constants.HTTP_READY_STATE_DONE){ 
      try {
      parseJsonResponse(this.response);
      frmCatalog.view.sgmCinemaCollection.setData(importantDataCinema);   
      } catch (e) {
        //alert("Error in searchDataCinema : " + e.message);
      }
        
      function parseJsonResponse(js){
        countCinemas = js.content.length;

        for(var i = 0; i < countCinemas; i++){
          var cinema = {
            id: js.content[i].id,
            txtCinemaTitle: js.content[i].name,
          };
          importantDataCinema.push(cinema);
        }
      }
    }
  }
};




