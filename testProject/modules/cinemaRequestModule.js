function getCinemaInfo (cinemaId) {
  var frmChosenCinema = _kony.mvc.GetController("frmChosenCinema", true);
  searchData();

  function searchData () {

    var request = new kony.net.HttpRequest();
    request.onReadyStateChange=httpCallbackHandler;
    request.open(constants.HTTP_METHOD_GET, "http://kino-teatr.ua:8081/services/api/cinema/" + cinemaId + "?apiKey=pol1kh111");
    request.send();
  }

  function httpCallbackHandler(){
    if(this.readyState == constants.HTTP_READY_STATE_DONE){
      if (this.response != "Cinema with id '" + cinemaId + "' not found") {
        frmChosenCinema.view.txtCinemaTitle.text = this.response.name;
        frmChosenCinema.view.txtCinemaSite.text = this.response.site;
        frmChosenCinema.view.txtCinemaPhone.text = this.response.phone;
        frmChosenCinema.view.txtCinemaAddress.text = this.response.address;
        frmChosenCinema.view.txtCinemaDescription.text = this.response.description;
        cinemaFound();
      } else {
        cinemaNotFound();
      }
    }
  }
}

function getCinemaImages (cinemaId) {
  var frmChosenCinema = _kony.mvc.GetController("frmChosenCinema", true);
  searchData();

  function searchData(){
	var request = new kony.net.HttpRequest();
    request.onReadyStateChange = httpCallbackHandler;
    request.open(constants.HTTP_METHOD_GET, "http://kino-teatr.ua:8081/services/api/cinema/" + cinemaId + "/images?apiKey=pol1kh111");
    request.send();
  }

  function httpCallbackHandler(){
    if(this.readyState == constants.HTTP_READY_STATE_DONE){
      if (this.response.totalElements !== 0) {
        var imgObj = {};
        var imgArr = [];
        for (var i = 0; i < this.response.totalElements; i++) {
          imgObj.src = "http://kino-teatr.ua:8081/services/api/cinema/image/" + this.response.content[i].id + "?apiKey=pol1kh111";
          imgArr.push({cinemaImg: {src: imgObj.src}});
        }
        frmChosenCinema.view.sgmCinema.setData(imgArr);
        frmChosenCinema.view.FlexContainerImgSegment.setVisibility(true);
      } else {
        frmChosenCinema.view.FlexContainerImgSegment.setVisibility(false);
        //cinemaNotFound();
      }
    }
  }
}

function cinemaNotFound () {
  var frmChosenCinema = _kony.mvc.GetController("frmChosenCinema", true);
  frmChosenCinema.view.txtCinemaTitle.text = "К сожалению, кинотеатр не найден.";
  frmChosenCinema.view.FlexContainerImgSegment.setVisibility(false);
  frmChosenCinema.view.FlexContainerCinemaSite.setVisibility(false);
  frmChosenCinema.view.FlexContainerCinemaPhone.setVisibility(false);
  frmChosenCinema.view.FlexContainerCinemaAddress.setVisibility(false);
  frmChosenCinema.view.ticketsBtnFlex.setVisibility(false);
  frmChosenCinema.view.FlexContainerCinemaDescription.setVisibility(false);
}

function cinemaFound () {
  var frmChosenCinema = _kony.mvc.GetController("frmChosenCinema", true);
  frmChosenCinema.view.FlexContainerCinemaSite.setVisibility(true);
  frmChosenCinema.view.FlexContainerCinemaPhone.setVisibility(true);
  frmChosenCinema.view.FlexContainerCinemaAddress.setVisibility(true);
  frmChosenCinema.view.ticketsBtnFlex.setVisibility(true);
  frmChosenCinema.view.FlexContainerCinemaDescription.setVisibility(true);
}