function HamburgerWeb() {
  var frmCatalogWeb = _kony.mvc.GetController('frmCatalogWeb', true);

  if (frmCatalogWeb.view.flxCinemas.left == "70%") {
    HamburgerFecharWeb();
  } else {
    HamburgerAbrirWeb();
  }

}

function HamburgerAbrirWeb() {
  var frmCatalogWeb = _kony.mvc.GetController('frmCatalogWeb', true);

  try {
    frmCatalogWeb.view.flxCinemas.animate(kony.ui.createAnimation({
      "100": {
        "left": "70%",
        "stepConfig": {
          "timingFunction": kony.anim.LINEAR
        }
      }
    }), {
      "delay": 0,
      "iterationCount": 1,
      "fillMode": kony.anim.FILL_MODE_FORWARDS,
      "duration": 0.5
    }, {
      "animationEnd": function() {}
    });
  } catch (e) {}
}

function HamburgerFecharWeb() {
  var frmCatalogWeb = _kony.mvc.GetController('frmCatalogWeb', true);

  try {

    frmCatalogWeb.view.flxCinemas.animate(kony.ui.createAnimation({
      "100": {
        "left": "100%",
        "stepConfig": {
          "timingFunction": kony.anim.LINEAR
        }
      }
    }), {
      "delay": 0,
      "iterationCount": 1,
      "fillMode": kony.anim.FILL_MODE_FORWARDS,
      "duration": 0.5
    }, {
      "animationEnd": function() {}
    });
  } catch (e) {}
}