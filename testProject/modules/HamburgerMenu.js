function Hamburger() {
    var pForm = kony.application.getCurrentForm();

    if (pForm.flxslidemenu.left == "0%") {
        HamburgerFechar();
    } else {
        HamburgerAbrir();
    }

}

function HamburgerAbrir() {
    var pForm = kony.application.getCurrentForm();

    try {

        pForm.flxslidemenu.animate(kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": function() {}
        });

//         if (kony.os.deviceInfo().name == "iPhone" || kony.os.deviceInfo().name == "iPhone Simulator" || kony.os.deviceInfo().name == "iPad" || kony.os.deviceInfo().name == "iPad Simulator") {
//             pForm.flxConteudo.setEnabled(true);
//         } else {
//             pForm.flxConteudo.setEnabled(false);
//         }
    } catch (e) {}

}

function HamburgerFechar() {

    var pForm = kony.application.getCurrentForm();

    try {

        pForm.flxslidemenu.animate(kony.ui.createAnimation({
            "100": {
                "left": "-87%",
                "stepConfig": {
                    "timingFunction": kony.anim.LINEAR
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.3
        }, {
            "animationEnd": function() {}
        });

        if (kony.os.deviceInfo().name == "iPhone" || kony.os.deviceInfo().name == "iPhone Simulator" || kony.os.deviceInfo().name == "iPad" || kony.os.deviceInfo().name == "iPad Simulator") {
            pForm.flxConteudo.setEnabled(true);
        } else {
            pForm.flxConteudo.setEnabled(true);
        }

    } catch (e) {}

}