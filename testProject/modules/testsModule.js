function Test (spec, test, func, exp) {
  this.lblSpecName = spec;
  this.lblTestName = test;
  this.lblResult = null;
  
  this.test = function() {
    if (JSON.stringify(func) == JSON.stringify(exp)) {
      this.lblResult = "Test is passed";
    } else {
      this.lblResult = "Test is failed";
    }
  };
}

function getTestResults() {
  var testList = [];

  var test1 = new Test("Email validation", "Email is valid", validateRegexEmail("email@gmail.com"), true);
  test1.test();
  testList.push(test1);
  
  var test2 = new Test("Email validation", "Email is not valid", validateRegexEmail("emailgmail"), false);
  test2.test();
  testList.push(test2);
  
  var test3 = new Test("Password validation", "Password is valid", validateRegexPassword("Shgkhl215665"), true);
  test3.test();
  testList.push(test3);
  
  var test4 = new Test("Password validation", "Password is not valid", validateRegexPassword("566"), false);
  test4.test();
  testList.push(test4);
  
  var test5 = new Test("Filter response by profession id", "Correct filter", filterByProfession(
  	[
        {
            "id": 928,
            "first_name_orig": "De Niro",
            "last_name_orig": "Robert",
            "birthdate": "1943-08-17",
            "last_name": "Де Ниро",
            "first_name": "Роберт",
            "rating": 9.77778,
            "rating_sum": 88,
            "rating_votes": 9,
            "num_comments": 1,
            "biography": "<p style=\"text-align: justify;\">Американский актер, режиссер и продюсер.</p>\r\n<p style=\"text-align: justify;\">Полное имя -<span style=\"font-weight: bold;\"> Роберт Марио Де Ниро-младший. </span><b>Де Ниро</b> считается одним из талантливейших актеров своего поколения, прежде всего за его многолетнее сотрудничество с режиссером <b>Мартином Скорсезе</b>. Сам он считает себя итало-американцем, хотя итальянские корни имеет лишь его дедушка по отцовской линии. В 1966 году <b>Роберт</b> дебютирует в фильме <b>Брайана Де Пальма</b> &quot;Свадебная вечеринка&quot;. </p>\r\n<p style=\"text-align: justify;\">Однако первой заметной ролью актера стало участие в фильме, посвященному бейсболу &mdash; &quot;Bang the Drum Slowly&quot; в 1973 году. В этом же году на экраны выходит фильм &quot;Злые улицы&quot;. Этот фильм стал первой совместной работой <b>Роберта</b> с с <b>Мартином Скорсезе</b>. В 1974 году <b>Де Ниро</b> получает премию &quot;Оскар&quot; за лучшую мужскую роль второго плана&raquo;в фильме <b>Френсиса Форда Копполы</b> &quot;Крестный отец 2&quot; по мотивам книги <b>Марио Пузо</b>.</p>\r\n<p style=\"text-align: justify;\">Очередная совместная работа с <b>Мартином Скорсезе</b> получила название &quot;Raging Bull&quot;. Эта роль принесла актеру &quot;Оскар&quot; в номинации лучшая мужская роль. </p>\r\n<p style=\"text-align: justify;\">Вскоре <b>Де Ниро</b> получает очередную номинацию &quot;Оскар&quot; за лучшую мужскую роль в фильме <b>Пенни Маршалла </b>&quot;Пробуждение&quot;(1990). В 2002 году в качестве режиссера <b>Роберт</b> приступает к съемкам эпоса &quot;The Good Shepherd&quot;. </p>\r\n<p style=\"text-align: justify;\">В 2003 году за заслуги перед киноиндустрией <b>Роберт </b>получает почетнейшую премию &quot;Оскар&quot; за вклад в историю кино. </p>",
            "url": "https://kino-teatr.ua/person/928.phtml",
            "profession_id": 2
        },
        {
            "id": 50,
            "first_name_orig": "Phoenix",
            "last_name_orig": " Joaquin",
            "birthdate": "1974-10-28",
            "last_name": "Феникс",
            "first_name": "Хоакин",
            "rating": 9.33333,
            "rating_sum": 28,
            "rating_votes": 3,
            "num_comments": 0,
            "biography": "<p>Американский актёр, продюсер и сценарист.</p>\r\n<p><b>Хоакин Рафаэль Феникс</b> родился 28 октября 1974 года в Сан-Хуане, Пуэрто-Рико, в семье Арлин Шэрон Феникс и Джона Ли Баттома. Родители будущего актёра поначалу были миссионерами в секте &quot;Дети Бога&quot;, но позже ушли из организации, перебрались в Лос-Анджелес, штат Калифорния. Мать стала работать в отделе кастинга &quot;NBC&quot;, а отец садовником. У <b>Хоакина </b>были один брат и три сестры. С ранних лет <b>Феникс </b>принимал участие во многих конкурсах и выступал на улицах. Там его заприметили и начали приглашать в рекламы и маленькие шоу. Немного позже юный актёр решил бросить профессию и отправился в путешествие с отцом по Мексике и Южной Америке.</p>\r\n<p>Дебют <b>Хоакина </b>в кинематографе состоялся в 1982 году с эпизодической роли в сериале &quot;Семь невест для семерых братьев&quot;.</p>\r\n<p>Первые большие фильмы в карьере <b>Феникса </b>появились уже на протяжении 1997-2003 годов. Тогда актёр принял весомое участие в картинах &quot;Поворот&quot; (1997), &quot;8 миллиметров&quot; (1999), &quot;Перо маркиза де Сада&quot; (2000), &quot;Гладиатор&quot; (2000, номинация на &quot;Золотой глобус&quot; и &quot;Оскар&quot;), &quot;Знаки&quot; (2002), &quot;Братец медвежонок&quot; (2003).</p>\r\n<p>Особенно активным для <b>Хоакина </b>стал 2004 год. Тогда его фильмография пополнилась лентами &quot;Таинственный лес&quot;, &quot;Отель &quot;Руанда&quot;&quot;, &quot;Команда 49: Огненная лестница&quot;.</p>\r\n<p>Период с 2005 по 2008 годы запомнился для <b>Феникса </b>неплохими проектами, среди которых стоит отметить &quot;Переступить черту&quot; (2005, &quot;Золотой глобус&quot; за &quot;Лучшую мужскую роль (комедия или мюзикл)&quot;, номинация на &quot;Оскар&quot;), &quot;Земляне&quot; (2005), &quot;Хозяева ночи&quot; (2007), &quot;Любовники&quot; (2008).</p>\r\n<p>За 2012-2015 годы актёр сыграл в таких фильмах, как &quot;Мастер&quot; (2012, номинация на &quot;Оскар&quot;), &quot;Она&quot; (2013), &quot;Врождённый порок&quot; (2014). За эти три картины он получил номинации на &quot;Золотой глобус&quot;. Кроме того, в этот период ещё появилась лента &quot;Иррациональный человек&quot; (2015).</p>",
            "url": "https://kino-teatr.ua/person/50.phtml",
            "profession_id": 2
        },
        {
            "id": 9178,
            "first_name_orig": "Silver",
            "last_name_orig": "Scott",
            "birthdate": null,
            "last_name": "Сильвер",
            "first_name": "Скотт",
            "rating": 0,
            "rating_sum": 0,
            "rating_votes": 0,
            "num_comments": 0,
            "biography": "<p>Сценарист, режиссер и продюсер.</p>",
            "url": "https://kino-teatr.ua/person/9178.phtml",
            "profession_id": 3
        },
        {
            "id": 13066,
            "first_name_orig": "Beetz",
            "last_name_orig": "Zazie",
            "birthdate": null,
            "last_name": "Битц",
            "first_name": "Зази",
            "rating": 8.5,
            "rating_sum": 17,
            "rating_votes": 2,
            "num_comments": 0,
            "biography": "<p>&nbsp;Актриса</p>",
            "url": "https://kino-teatr.ua/person/13066.phtml",
            "profession_id": 2
        },
        {
            "id": 1915,
            "first_name_orig": "Phillips",
            "last_name_orig": "Todd",
            "birthdate": "1970-12-20",
            "last_name": "Филлипс",
            "first_name": "Тодд",
            "rating": 4.66667,
            "rating_sum": 14,
            "rating_votes": 3,
            "num_comments": 0,
            "biography": "<p style=\"text-align: justify;\">Американский режиссер, сценарист, продюсер и актер.</p>\r\n<p style=\"text-align: justify;\">Родился в Бруклине, Нью-Йорк, США. Настоящее имя&nbsp;&mdash; <b>Тодд Бунцль</b>.</p>\r\n<p style=\"text-align: justify;\">Учился в&nbsp;Школе кино при&nbsp;Нью-Йоркском университете. Начал свою карьеру с&nbsp;фотографирования панк-рок-групп, затем снимал документальные фильмы. В&nbsp;1994&nbsp;году основал Подпольный нью-йоркский кинофестиваль, а&nbsp;в&nbsp;1998&nbsp;году снял документальный фильм про&nbsp;различные братства&nbsp;&mdash; &quot;Frat House&quot;, который удостоился гран-при кинофестиваля в&nbsp;Сандэнсе. На&nbsp;фестивале <b>Тодд</b> познакомился с&nbsp;<b>Айваном Рейтманом</b>, под&nbsp;опекой которого снял два своих первых полнометражных фильма&nbsp;&mdash; &quot;Дорожное приключение&quot; (2000) и &quot;Старая закалка&quot; (2003). Затем была сценарная работа над&nbsp;фильмом &quot;Борат&quot; (2006), за&nbsp;которую <b>Тодда</b> номинировали на &quot;Оскар&quot;.</p>\r\n<p style=\"text-align: justify;\">Снимал такие фильмы, как &quot;Убойная парочка: Старски и&nbsp;Хатч&quot; (2004), &quot;Школа негодяев&quot; (2006), &quot;Мужчины&quot; (2008) и другие.</p>",
            "url": "https://kino-teatr.ua/person/1915.phtml",
            "profession_id": 1
        },
        {
            "id": 3789,
            "first_name_orig": "Conroy",
            "last_name_orig": "Frances",
            "birthdate": "1953-11-13",
            "last_name": "Конрой",
            "first_name": "Фрэнсис",
            "rating": 9,
            "rating_sum": 9,
            "rating_votes": 1,
            "num_comments": 0,
            "biography": "<p style=\"text-align: justify;\">Американская актриса.</p>\r\n<p style=\"text-align: justify;\">Родилась в Монро, Джорджия, США.</p>\r\n<p style=\"text-align: justify;\">Изучала драматическое искусство в театре Плэйхаус и Джульярдской школе в Нью-Йорке.</p>\r\n<p style=\"text-align: justify;\">Актерскую карьера начала в 1970-х годах с участия в различных театральных компаниях. В кино одну из первых своих ролей <b>Конрой</b> исполнила в 1979 году в фильме <b>Вуди Аллена </b>&quot;Манхэттен&quot;. Спустя год она дебютировала на Бродвее в пьесе&nbsp; <b>Эдварда Олби</b> &quot;Дама из Дюбука&quot;. Последующие годы <b>Конрой</b> оставалась в основном активна  в театре, где появилась во многих знаменитых пьесах, четыре раза номинировалась на премию &quot;Drama Desk&quot; и один раз на &quot;Тони&quot;.</p>\r\n<p style=\"text-align: justify;\">В 1992 году завязала дружбу с известным драматургом <b>Артуром Миллером</b>,  благодаря чему появилась во многих его постановках в театре, в кино и  на телевидении.</p>\r\n<p style=\"text-align: justify;\">Помимо театра у <b>Конрой</b> довольно много появлялась на телевидении и в  кино. Наиболее известной телевизионной работой актрисы стала роль Рут  Фишер в сериале НВО &quot;Клиент всегда мертв&quot;, в котором она снималась с 2001 по 2005 год. Эта роль принесла <b>Конрой </b>четыре номинации на &quot;Эмми&quot; и премию &quot;Золотой глобус&quot;, в номинации лучшая актриса в драматическом сериале.</p>\r\n<p style=\"text-align: justify;\">Снималась в таких фильмах как &quot;Влюбленные&quot; (1984), &quot;Отпетые мошенники&quot; (1988), &quot;Другая женщина&quot; (1988), &quot;Билли Батгейт&quot; (1991), &quot;Запах женщины&quot; (1992), &quot;Не спящие в Сиэтле&quot; (1993), &quot;Госпожа горничная&quot; (2002), &quot;Женщина-кошка&quot; (2004), &quot;Авиатор&quot; (2004), &quot;Продавщица&quot; (2005), &quot;Сломанные цветы&quot; (2005), &quot;Плетеный человек&quot; (2006), &quot;Замерзшая из Майами&quot; (2009), &quot;Любовь случается&quot; (2009), &quot;Стоун&quot; (2010), &quot;Убежище&quot; (2010) и другие. </p>",
            "url": "https://kino-teatr.ua/person/3789.phtml",
            "profession_id": 2
        }
    ], 
    1), 
                       [{
                         "id": 1915,
                         "first_name_orig": "Phillips",
                         "last_name_orig": "Todd",
                         "birthdate": "1970-12-20",
                         "last_name": "Филлипс",
                         "first_name": "Тодд",
                         "rating": 4.66667,
                         "rating_sum": 14,
                         "rating_votes": 3,
                         "num_comments": 0,
                         "biography": "<p style=\"text-align: justify;\">Американский режиссер, сценарист, продюсер и актер.</p>\r\n<p style=\"text-align: justify;\">Родился в Бруклине, Нью-Йорк, США. Настоящее имя&nbsp;&mdash; <b>Тодд Бунцль</b>.</p>\r\n<p style=\"text-align: justify;\">Учился в&nbsp;Школе кино при&nbsp;Нью-Йоркском университете. Начал свою карьеру с&nbsp;фотографирования панк-рок-групп, затем снимал документальные фильмы. В&nbsp;1994&nbsp;году основал Подпольный нью-йоркский кинофестиваль, а&nbsp;в&nbsp;1998&nbsp;году снял документальный фильм про&nbsp;различные братства&nbsp;&mdash; &quot;Frat House&quot;, который удостоился гран-при кинофестиваля в&nbsp;Сандэнсе. На&nbsp;фестивале <b>Тодд</b> познакомился с&nbsp;<b>Айваном Рейтманом</b>, под&nbsp;опекой которого снял два своих первых полнометражных фильма&nbsp;&mdash; &quot;Дорожное приключение&quot; (2000) и &quot;Старая закалка&quot; (2003). Затем была сценарная работа над&nbsp;фильмом &quot;Борат&quot; (2006), за&nbsp;которую <b>Тодда</b> номинировали на &quot;Оскар&quot;.</p>\r\n<p style=\"text-align: justify;\">Снимал такие фильмы, как &quot;Убойная парочка: Старски и&nbsp;Хатч&quot; (2004), &quot;Школа негодяев&quot; (2006), &quot;Мужчины&quot; (2008) и другие.</p>",
                         "url": "https://kino-teatr.ua/person/1915.phtml",
                         "profession_id": 1
                       }]
                      );
  test5.test();
  testList.push(test5);
  
  var test6 = new Test("Add profession id to respons", "Correct adding", addProfessionId(
    {
      "content": [
        {
          "id": 82049,
          "film_id": 49999,
          "person_id": 50,
          "profession_id": 2,
          "role": null,
          "order": 5
        },
        {
          "id": 82051,
          "film_id": 49999,
          "person_id": 928,
          "profession_id": 2,
          "role": null,
          "order": 7
        },
        {
          "id": 82045,
          "film_id": 49999,
          "person_id": 1915,
          "profession_id": 1,
          "role": null,
          "order": 1
        },
        {
          "id": 82052,
          "film_id": 49999,
          "person_id": 3789,
          "profession_id": 2,
          "role": null,
          "order": 8
        },
        {
          "id": 82048,
          "film_id": 49999,
          "person_id": 9178,
          "profession_id": 3,
          "role": null,
          "order": 4
        },
        {
          "id": 82050,
          "film_id": 49999,
          "person_id": 13066,
          "profession_id": 2,
          "role": null,
          "order": 6
        }
      ],
      "totalPages": 1,
      "totalElements": 8,
      "number": 0,
      "size": 100,
      "numberOfElements": 8,
      "first": true,
      "last": true,
      "persons": [
        {
          "id": 928,
          "first_name_orig": "De Niro",
          "last_name_orig": "Robert",
          "birthdate": "1943-08-17",
          "last_name": "Де Ниро",
          "first_name": "Роберт",
          "rating": 9.77778,
          "rating_sum": 88,
          "rating_votes": 9,
          "num_comments": 1,
          "biography": "<p style=\"text-align: justify;\">Американский актер, режиссер и продюсер.</p>\r\n<p style=\"text-align: justify;\">Полное имя -<span style=\"font-weight: bold;\"> Роберт Марио Де Ниро-младший. </span><b>Де Ниро</b> считается одним из талантливейших актеров своего поколения, прежде всего за его многолетнее сотрудничество с режиссером <b>Мартином Скорсезе</b>. Сам он считает себя итало-американцем, хотя итальянские корни имеет лишь его дедушка по отцовской линии. В 1966 году <b>Роберт</b> дебютирует в фильме <b>Брайана Де Пальма</b> &quot;Свадебная вечеринка&quot;. </p>\r\n<p style=\"text-align: justify;\">Однако первой заметной ролью актера стало участие в фильме, посвященному бейсболу &mdash; &quot;Bang the Drum Slowly&quot; в 1973 году. В этом же году на экраны выходит фильм &quot;Злые улицы&quot;. Этот фильм стал первой совместной работой <b>Роберта</b> с с <b>Мартином Скорсезе</b>. В 1974 году <b>Де Ниро</b> получает премию &quot;Оскар&quot; за лучшую мужскую роль второго плана&raquo;в фильме <b>Френсиса Форда Копполы</b> &quot;Крестный отец 2&quot; по мотивам книги <b>Марио Пузо</b>.</p>\r\n<p style=\"text-align: justify;\">Очередная совместная работа с <b>Мартином Скорсезе</b> получила название &quot;Raging Bull&quot;. Эта роль принесла актеру &quot;Оскар&quot; в номинации лучшая мужская роль. </p>\r\n<p style=\"text-align: justify;\">Вскоре <b>Де Ниро</b> получает очередную номинацию &quot;Оскар&quot; за лучшую мужскую роль в фильме <b>Пенни Маршалла </b>&quot;Пробуждение&quot;(1990). В 2002 году в качестве режиссера <b>Роберт</b> приступает к съемкам эпоса &quot;The Good Shepherd&quot;. </p>\r\n<p style=\"text-align: justify;\">В 2003 году за заслуги перед киноиндустрией <b>Роберт </b>получает почетнейшую премию &quot;Оскар&quot; за вклад в историю кино. </p>",
          "url": "https://kino-teatr.ua/person/928.phtml"
        },
        {
          "id": 50,
          "first_name_orig": "Phoenix",
          "last_name_orig": " Joaquin",
          "birthdate": "1974-10-28",
          "last_name": "Феникс",
          "first_name": "Хоакин",
          "rating": 9.33333,
          "rating_sum": 28,
          "rating_votes": 3,
          "num_comments": 0,
          "biography": "<p>Американский актёр, продюсер и сценарист.</p>\r\n<p><b>Хоакин Рафаэль Феникс</b> родился 28 октября 1974 года в Сан-Хуане, Пуэрто-Рико, в семье Арлин Шэрон Феникс и Джона Ли Баттома. Родители будущего актёра поначалу были миссионерами в секте &quot;Дети Бога&quot;, но позже ушли из организации, перебрались в Лос-Анджелес, штат Калифорния. Мать стала работать в отделе кастинга &quot;NBC&quot;, а отец садовником. У <b>Хоакина </b>были один брат и три сестры. С ранних лет <b>Феникс </b>принимал участие во многих конкурсах и выступал на улицах. Там его заприметили и начали приглашать в рекламы и маленькие шоу. Немного позже юный актёр решил бросить профессию и отправился в путешествие с отцом по Мексике и Южной Америке.</p>\r\n<p>Дебют <b>Хоакина </b>в кинематографе состоялся в 1982 году с эпизодической роли в сериале &quot;Семь невест для семерых братьев&quot;.</p>\r\n<p>Первые большие фильмы в карьере <b>Феникса </b>появились уже на протяжении 1997-2003 годов. Тогда актёр принял весомое участие в картинах &quot;Поворот&quot; (1997), &quot;8 миллиметров&quot; (1999), &quot;Перо маркиза де Сада&quot; (2000), &quot;Гладиатор&quot; (2000, номинация на &quot;Золотой глобус&quot; и &quot;Оскар&quot;), &quot;Знаки&quot; (2002), &quot;Братец медвежонок&quot; (2003).</p>\r\n<p>Особенно активным для <b>Хоакина </b>стал 2004 год. Тогда его фильмография пополнилась лентами &quot;Таинственный лес&quot;, &quot;Отель &quot;Руанда&quot;&quot;, &quot;Команда 49: Огненная лестница&quot;.</p>\r\n<p>Период с 2005 по 2008 годы запомнился для <b>Феникса </b>неплохими проектами, среди которых стоит отметить &quot;Переступить черту&quot; (2005, &quot;Золотой глобус&quot; за &quot;Лучшую мужскую роль (комедия или мюзикл)&quot;, номинация на &quot;Оскар&quot;), &quot;Земляне&quot; (2005), &quot;Хозяева ночи&quot; (2007), &quot;Любовники&quot; (2008).</p>\r\n<p>За 2012-2015 годы актёр сыграл в таких фильмах, как &quot;Мастер&quot; (2012, номинация на &quot;Оскар&quot;), &quot;Она&quot; (2013), &quot;Врождённый порок&quot; (2014). За эти три картины он получил номинации на &quot;Золотой глобус&quot;. Кроме того, в этот период ещё появилась лента &quot;Иррациональный человек&quot; (2015).</p>",
          "url": "https://kino-teatr.ua/person/50.phtml"
        },
        {
          "id": 9178,
          "first_name_orig": "Silver",
          "last_name_orig": "Scott",
          "birthdate": null,
          "last_name": "Сильвер",
          "first_name": "Скотт",
          "rating": 0,
          "rating_sum": 0,
          "rating_votes": 0,
          "num_comments": 0,
          "biography": "<p>Сценарист, режиссер и продюсер.</p>",
          "url": "https://kino-teatr.ua/person/9178.phtml"
        },
        {
          "id": 13066,
          "first_name_orig": "Beetz",
          "last_name_orig": "Zazie",
          "birthdate": null,
          "last_name": "Битц",
          "first_name": "Зази",
          "rating": 8.5,
          "rating_sum": 17,
          "rating_votes": 2,
          "num_comments": 0,
          "biography": "<p>&nbsp;Актриса</p>",
          "url": "https://kino-teatr.ua/person/13066.phtml"
        },
        {
          "id": 1915,
          "first_name_orig": "Phillips",
          "last_name_orig": "Todd",
          "birthdate": "1970-12-20",
          "last_name": "Филлипс",
          "first_name": "Тодд",
          "rating": 4.66667,
          "rating_sum": 14,
          "rating_votes": 3,
          "num_comments": 0,
          "biography": "<p style=\"text-align: justify;\">Американский режиссер, сценарист, продюсер и актер.</p>\r\n<p style=\"text-align: justify;\">Родился в Бруклине, Нью-Йорк, США. Настоящее имя&nbsp;&mdash; <b>Тодд Бунцль</b>.</p>\r\n<p style=\"text-align: justify;\">Учился в&nbsp;Школе кино при&nbsp;Нью-Йоркском университете. Начал свою карьеру с&nbsp;фотографирования панк-рок-групп, затем снимал документальные фильмы. В&nbsp;1994&nbsp;году основал Подпольный нью-йоркский кинофестиваль, а&nbsp;в&nbsp;1998&nbsp;году снял документальный фильм про&nbsp;различные братства&nbsp;&mdash; &quot;Frat House&quot;, который удостоился гран-при кинофестиваля в&nbsp;Сандэнсе. На&nbsp;фестивале <b>Тодд</b> познакомился с&nbsp;<b>Айваном Рейтманом</b>, под&nbsp;опекой которого снял два своих первых полнометражных фильма&nbsp;&mdash; &quot;Дорожное приключение&quot; (2000) и &quot;Старая закалка&quot; (2003). Затем была сценарная работа над&nbsp;фильмом &quot;Борат&quot; (2006), за&nbsp;которую <b>Тодда</b> номинировали на &quot;Оскар&quot;.</p>\r\n<p style=\"text-align: justify;\">Снимал такие фильмы, как &quot;Убойная парочка: Старски и&nbsp;Хатч&quot; (2004), &quot;Школа негодяев&quot; (2006), &quot;Мужчины&quot; (2008) и другие.</p>",
          "url": "https://kino-teatr.ua/person/1915.phtml"
        },
        {
          "id": 3789,
          "first_name_orig": "Conroy",
          "last_name_orig": "Frances",
          "birthdate": "1953-11-13",
          "last_name": "Конрой",
          "first_name": "Фрэнсис",
          "rating": 9,
          "rating_sum": 9,
          "rating_votes": 1,
          "num_comments": 0,
          "biography": "<p style=\"text-align: justify;\">Американская актриса.</p>\r\n<p style=\"text-align: justify;\">Родилась в Монро, Джорджия, США.</p>\r\n<p style=\"text-align: justify;\">Изучала драматическое искусство в театре Плэйхаус и Джульярдской школе в Нью-Йорке.</p>\r\n<p style=\"text-align: justify;\">Актерскую карьера начала в 1970-х годах с участия в различных театральных компаниях. В кино одну из первых своих ролей <b>Конрой</b> исполнила в 1979 году в фильме <b>Вуди Аллена </b>&quot;Манхэттен&quot;. Спустя год она дебютировала на Бродвее в пьесе&nbsp; <b>Эдварда Олби</b> &quot;Дама из Дюбука&quot;. Последующие годы <b>Конрой</b> оставалась в основном активна  в театре, где появилась во многих знаменитых пьесах, четыре раза номинировалась на премию &quot;Drama Desk&quot; и один раз на &quot;Тони&quot;.</p>\r\n<p style=\"text-align: justify;\">В 1992 году завязала дружбу с известным драматургом <b>Артуром Миллером</b>,  благодаря чему появилась во многих его постановках в театре, в кино и  на телевидении.</p>\r\n<p style=\"text-align: justify;\">Помимо театра у <b>Конрой</b> довольно много появлялась на телевидении и в  кино. Наиболее известной телевизионной работой актрисы стала роль Рут  Фишер в сериале НВО &quot;Клиент всегда мертв&quot;, в котором она снималась с 2001 по 2005 год. Эта роль принесла <b>Конрой </b>четыре номинации на &quot;Эмми&quot; и премию &quot;Золотой глобус&quot;, в номинации лучшая актриса в драматическом сериале.</p>\r\n<p style=\"text-align: justify;\">Снималась в таких фильмах как &quot;Влюбленные&quot; (1984), &quot;Отпетые мошенники&quot; (1988), &quot;Другая женщина&quot; (1988), &quot;Билли Батгейт&quot; (1991), &quot;Запах женщины&quot; (1992), &quot;Не спящие в Сиэтле&quot; (1993), &quot;Госпожа горничная&quot; (2002), &quot;Женщина-кошка&quot; (2004), &quot;Авиатор&quot; (2004), &quot;Продавщица&quot; (2005), &quot;Сломанные цветы&quot; (2005), &quot;Плетеный человек&quot; (2006), &quot;Замерзшая из Майами&quot; (2009), &quot;Любовь случается&quot; (2009), &quot;Стоун&quot; (2010), &quot;Убежище&quot; (2010) и другие. </p>",
          "url": "https://kino-teatr.ua/person/3789.phtml"
        }
      ],
      "professions": [
        {
          "id": 1,
          "name": "Режиссер",
          "names": "Режиссеры",
          "order": 1
        },
        {
          "id": 2,
          "name": "Актер",
          "names": "Актеры",
          "order": 2
        },
        {
          "id": 3,
          "name": "Сценарист",
          "names": "Сценаристы",
          "order": 3
        },
        {
          "id": 6,
          "name": "Продюсер",
          "names": "Продюсеры",
          "order": 6
        }
      ]
    }
  ), 
                       {
    "content": [
      {
        "id": 82049,
        "film_id": 49999,
        "person_id": 50,
        "profession_id": 2,
        "role": null,
        "order": 5
      },
      {
        "id": 82051,
        "film_id": 49999,
        "person_id": 928,
        "profession_id": 2,
        "role": null,
        "order": 7
      },
      {
        "id": 82045,
        "film_id": 49999,
        "person_id": 1915,
        "profession_id": 1,
        "role": null,
        "order": 1
      },
      {
        "id": 82052,
        "film_id": 49999,
        "person_id": 3789,
        "profession_id": 2,
        "role": null,
        "order": 8
      },
      {
        "id": 82048,
        "film_id": 49999,
        "person_id": 9178,
        "profession_id": 3,
        "role": null,
        "order": 4
      },
      {
        "id": 82050,
        "film_id": 49999,
        "person_id": 13066,
        "profession_id": 2,
        "role": null,
        "order": 6
      }
    ],
    "totalPages": 1,
    "totalElements": 8,
    "number": 0,
    "size": 100,
    "numberOfElements": 8,
    "first": true,
    "last": true,
    "persons": [
      {
        "id": 928,
        "first_name_orig": "De Niro",
        "last_name_orig": "Robert",
        "birthdate": "1943-08-17",
        "last_name": "Де Ниро",
        "first_name": "Роберт",
        "rating": 9.77778,
        "rating_sum": 88,
        "rating_votes": 9,
        "num_comments": 1,
        "biography": "<p style=\"text-align: justify;\">Американский актер, режиссер и продюсер.</p>\r\n<p style=\"text-align: justify;\">Полное имя -<span style=\"font-weight: bold;\"> Роберт Марио Де Ниро-младший. </span><b>Де Ниро</b> считается одним из талантливейших актеров своего поколения, прежде всего за его многолетнее сотрудничество с режиссером <b>Мартином Скорсезе</b>. Сам он считает себя итало-американцем, хотя итальянские корни имеет лишь его дедушка по отцовской линии. В 1966 году <b>Роберт</b> дебютирует в фильме <b>Брайана Де Пальма</b> &quot;Свадебная вечеринка&quot;. </p>\r\n<p style=\"text-align: justify;\">Однако первой заметной ролью актера стало участие в фильме, посвященному бейсболу &mdash; &quot;Bang the Drum Slowly&quot; в 1973 году. В этом же году на экраны выходит фильм &quot;Злые улицы&quot;. Этот фильм стал первой совместной работой <b>Роберта</b> с с <b>Мартином Скорсезе</b>. В 1974 году <b>Де Ниро</b> получает премию &quot;Оскар&quot; за лучшую мужскую роль второго плана&raquo;в фильме <b>Френсиса Форда Копполы</b> &quot;Крестный отец 2&quot; по мотивам книги <b>Марио Пузо</b>.</p>\r\n<p style=\"text-align: justify;\">Очередная совместная работа с <b>Мартином Скорсезе</b> получила название &quot;Raging Bull&quot;. Эта роль принесла актеру &quot;Оскар&quot; в номинации лучшая мужская роль. </p>\r\n<p style=\"text-align: justify;\">Вскоре <b>Де Ниро</b> получает очередную номинацию &quot;Оскар&quot; за лучшую мужскую роль в фильме <b>Пенни Маршалла </b>&quot;Пробуждение&quot;(1990). В 2002 году в качестве режиссера <b>Роберт</b> приступает к съемкам эпоса &quot;The Good Shepherd&quot;. </p>\r\n<p style=\"text-align: justify;\">В 2003 году за заслуги перед киноиндустрией <b>Роберт </b>получает почетнейшую премию &quot;Оскар&quot; за вклад в историю кино. </p>",
        "url": "https://kino-teatr.ua/person/928.phtml",
        "profession_id": 2
      },
      {
        "id": 50,
        "first_name_orig": "Phoenix",
        "last_name_orig": " Joaquin",
        "birthdate": "1974-10-28",
        "last_name": "Феникс",
        "first_name": "Хоакин",
        "rating": 9.33333,
        "rating_sum": 28,
        "rating_votes": 3,
        "num_comments": 0,
        "biography": "<p>Американский актёр, продюсер и сценарист.</p>\r\n<p><b>Хоакин Рафаэль Феникс</b> родился 28 октября 1974 года в Сан-Хуане, Пуэрто-Рико, в семье Арлин Шэрон Феникс и Джона Ли Баттома. Родители будущего актёра поначалу были миссионерами в секте &quot;Дети Бога&quot;, но позже ушли из организации, перебрались в Лос-Анджелес, штат Калифорния. Мать стала работать в отделе кастинга &quot;NBC&quot;, а отец садовником. У <b>Хоакина </b>были один брат и три сестры. С ранних лет <b>Феникс </b>принимал участие во многих конкурсах и выступал на улицах. Там его заприметили и начали приглашать в рекламы и маленькие шоу. Немного позже юный актёр решил бросить профессию и отправился в путешествие с отцом по Мексике и Южной Америке.</p>\r\n<p>Дебют <b>Хоакина </b>в кинематографе состоялся в 1982 году с эпизодической роли в сериале &quot;Семь невест для семерых братьев&quot;.</p>\r\n<p>Первые большие фильмы в карьере <b>Феникса </b>появились уже на протяжении 1997-2003 годов. Тогда актёр принял весомое участие в картинах &quot;Поворот&quot; (1997), &quot;8 миллиметров&quot; (1999), &quot;Перо маркиза де Сада&quot; (2000), &quot;Гладиатор&quot; (2000, номинация на &quot;Золотой глобус&quot; и &quot;Оскар&quot;), &quot;Знаки&quot; (2002), &quot;Братец медвежонок&quot; (2003).</p>\r\n<p>Особенно активным для <b>Хоакина </b>стал 2004 год. Тогда его фильмография пополнилась лентами &quot;Таинственный лес&quot;, &quot;Отель &quot;Руанда&quot;&quot;, &quot;Команда 49: Огненная лестница&quot;.</p>\r\n<p>Период с 2005 по 2008 годы запомнился для <b>Феникса </b>неплохими проектами, среди которых стоит отметить &quot;Переступить черту&quot; (2005, &quot;Золотой глобус&quot; за &quot;Лучшую мужскую роль (комедия или мюзикл)&quot;, номинация на &quot;Оскар&quot;), &quot;Земляне&quot; (2005), &quot;Хозяева ночи&quot; (2007), &quot;Любовники&quot; (2008).</p>\r\n<p>За 2012-2015 годы актёр сыграл в таких фильмах, как &quot;Мастер&quot; (2012, номинация на &quot;Оскар&quot;), &quot;Она&quot; (2013), &quot;Врождённый порок&quot; (2014). За эти три картины он получил номинации на &quot;Золотой глобус&quot;. Кроме того, в этот период ещё появилась лента &quot;Иррациональный человек&quot; (2015).</p>",
        "url": "https://kino-teatr.ua/person/50.phtml",
        "profession_id": 2
      },
      {
        "id": 9178,
        "first_name_orig": "Silver",
        "last_name_orig": "Scott",
        "birthdate": null,
        "last_name": "Сильвер",
        "first_name": "Скотт",
        "rating": 0,
        "rating_sum": 0,
        "rating_votes": 0,
        "num_comments": 0,
        "biography": "<p>Сценарист, режиссер и продюсер.</p>",
        "url": "https://kino-teatr.ua/person/9178.phtml",
        "profession_id": 3
      },
      {
        "id": 13066,
        "first_name_orig": "Beetz",
        "last_name_orig": "Zazie",
        "birthdate": null,
        "last_name": "Битц",
        "first_name": "Зази",
        "rating": 8.5,
        "rating_sum": 17,
        "rating_votes": 2,
        "num_comments": 0,
        "biography": "<p>&nbsp;Актриса</p>",
        "url": "https://kino-teatr.ua/person/13066.phtml",
        "profession_id": 2
      },
      {
        "id": 1915,
        "first_name_orig": "Phillips",
        "last_name_orig": "Todd",
        "birthdate": "1970-12-20",
        "last_name": "Филлипс",
        "first_name": "Тодд",
        "rating": 4.66667,
        "rating_sum": 14,
        "rating_votes": 3,
        "num_comments": 0,
        "biography": "<p style=\"text-align: justify;\">Американский режиссер, сценарист, продюсер и актер.</p>\r\n<p style=\"text-align: justify;\">Родился в Бруклине, Нью-Йорк, США. Настоящее имя&nbsp;&mdash; <b>Тодд Бунцль</b>.</p>\r\n<p style=\"text-align: justify;\">Учился в&nbsp;Школе кино при&nbsp;Нью-Йоркском университете. Начал свою карьеру с&nbsp;фотографирования панк-рок-групп, затем снимал документальные фильмы. В&nbsp;1994&nbsp;году основал Подпольный нью-йоркский кинофестиваль, а&nbsp;в&nbsp;1998&nbsp;году снял документальный фильм про&nbsp;различные братства&nbsp;&mdash; &quot;Frat House&quot;, который удостоился гран-при кинофестиваля в&nbsp;Сандэнсе. На&nbsp;фестивале <b>Тодд</b> познакомился с&nbsp;<b>Айваном Рейтманом</b>, под&nbsp;опекой которого снял два своих первых полнометражных фильма&nbsp;&mdash; &quot;Дорожное приключение&quot; (2000) и &quot;Старая закалка&quot; (2003). Затем была сценарная работа над&nbsp;фильмом &quot;Борат&quot; (2006), за&nbsp;которую <b>Тодда</b> номинировали на &quot;Оскар&quot;.</p>\r\n<p style=\"text-align: justify;\">Снимал такие фильмы, как &quot;Убойная парочка: Старски и&nbsp;Хатч&quot; (2004), &quot;Школа негодяев&quot; (2006), &quot;Мужчины&quot; (2008) и другие.</p>",
        "url": "https://kino-teatr.ua/person/1915.phtml",
        "profession_id": 1
      },
      {
        "id": 3789,
        "first_name_orig": "Conroy",
        "last_name_orig": "Frances",
        "birthdate": "1953-11-13",
        "last_name": "Конрой",
        "first_name": "Фрэнсис",
        "rating": 9,
        "rating_sum": 9,
        "rating_votes": 1,
        "num_comments": 0,
        "biography": "<p style=\"text-align: justify;\">Американская актриса.</p>\r\n<p style=\"text-align: justify;\">Родилась в Монро, Джорджия, США.</p>\r\n<p style=\"text-align: justify;\">Изучала драматическое искусство в театре Плэйхаус и Джульярдской школе в Нью-Йорке.</p>\r\n<p style=\"text-align: justify;\">Актерскую карьера начала в 1970-х годах с участия в различных театральных компаниях. В кино одну из первых своих ролей <b>Конрой</b> исполнила в 1979 году в фильме <b>Вуди Аллена </b>&quot;Манхэттен&quot;. Спустя год она дебютировала на Бродвее в пьесе&nbsp; <b>Эдварда Олби</b> &quot;Дама из Дюбука&quot;. Последующие годы <b>Конрой</b> оставалась в основном активна  в театре, где появилась во многих знаменитых пьесах, четыре раза номинировалась на премию &quot;Drama Desk&quot; и один раз на &quot;Тони&quot;.</p>\r\n<p style=\"text-align: justify;\">В 1992 году завязала дружбу с известным драматургом <b>Артуром Миллером</b>,  благодаря чему появилась во многих его постановках в театре, в кино и  на телевидении.</p>\r\n<p style=\"text-align: justify;\">Помимо театра у <b>Конрой</b> довольно много появлялась на телевидении и в  кино. Наиболее известной телевизионной работой актрисы стала роль Рут  Фишер в сериале НВО &quot;Клиент всегда мертв&quot;, в котором она снималась с 2001 по 2005 год. Эта роль принесла <b>Конрой </b>четыре номинации на &quot;Эмми&quot; и премию &quot;Золотой глобус&quot;, в номинации лучшая актриса в драматическом сериале.</p>\r\n<p style=\"text-align: justify;\">Снималась в таких фильмах как &quot;Влюбленные&quot; (1984), &quot;Отпетые мошенники&quot; (1988), &quot;Другая женщина&quot; (1988), &quot;Билли Батгейт&quot; (1991), &quot;Запах женщины&quot; (1992), &quot;Не спящие в Сиэтле&quot; (1993), &quot;Госпожа горничная&quot; (2002), &quot;Женщина-кошка&quot; (2004), &quot;Авиатор&quot; (2004), &quot;Продавщица&quot; (2005), &quot;Сломанные цветы&quot; (2005), &quot;Плетеный человек&quot; (2006), &quot;Замерзшая из Майами&quot; (2009), &quot;Любовь случается&quot; (2009), &quot;Стоун&quot; (2010), &quot;Убежище&quot; (2010) и другие. </p>",
        "url": "https://kino-teatr.ua/person/3789.phtml",
        "profession_id": 2
      }
    ],
    "professions": [
      {
        "id": 1,
        "name": "Режиссер",
        "names": "Режиссеры",
        "order": 1
      },
      {
        "id": 2,
        "name": "Актер",
        "names": "Актеры",
        "order": 2
      },
      {
        "id": 3,
        "name": "Сценарист",
        "names": "Сценаристы",
        "order": 3
      },
      {
        "id": 6,
        "name": "Продюсер",
        "names": "Продюсеры",
        "order": 6
      }
    ]
  }
                      );
  test6.test();
  testList.push(test6);
  
  var test7 = new Test("Get profession Id by person Id", "Correct getting", getProfessionIdByPersonId(
  [
        {
            "id": 82049,
            "film_id": 49999,
            "person_id": 50,
            "profession_id": 2,
            "role": null,
            "order": 5
        },
        {
            "id": 82051,
            "film_id": 49999,
            "person_id": 928,
            "profession_id": 2,
            "role": null,
            "order": 7
        },
        {
            "id": 82045,
            "film_id": 49999,
            "person_id": 1915,
            "profession_id": 1,
            "role": null,
            "order": 1
        },
        {
            "id": 82052,
            "film_id": 49999,
            "person_id": 3789,
            "profession_id": 2,
            "role": null,
            "order": 8
        },
        {
            "id": 82048,
            "film_id": 49999,
            "person_id": 9178,
            "profession_id": 3,
            "role": null,
            "order": 4
        },
        {
            "id": 82050,
            "film_id": 49999,
            "person_id": 13066,
            "profession_id": 2,
            "role": null,
            "order": 6
        }
    ]
  ), 
  {
    50: 2,
    928: 2,
    1915: 1,
    3789: 2,
    9178: 3,
    13066: 2
  }
                      );
  test7.test();
  testList.push(test7);   

  var test8 = new Test("Format an array to segment data", "Correct format", formatToSegmentData(
    [{
      "id": 1915,
      "first_name_orig": "Phillips",
      "last_name_orig": "Todd",
      "birthdate": "1970-12-20",
      "last_name": "Филлипс",
      "first_name": "Тодд",
      "rating": 4.66667,
      "rating_sum": 14,
      "rating_votes": 3,
      "num_comments": 0,
      "biography": "<p style=\"text-align: justify;\">Американский режиссер, сценарист, продюсер и актер.</p>\r\n<p style=\"text-align: justify;\">Родился в Бруклине, Нью-Йорк, США. Настоящее имя&nbsp;&mdash; <b>Тодд Бунцль</b>.</p>\r\n<p style=\"text-align: justify;\">Учился в&nbsp;Школе кино при&nbsp;Нью-Йоркском университете. Начал свою карьеру с&nbsp;фотографирования панк-рок-групп, затем снимал документальные фильмы. В&nbsp;1994&nbsp;году основал Подпольный нью-йоркский кинофестиваль, а&nbsp;в&nbsp;1998&nbsp;году снял документальный фильм про&nbsp;различные братства&nbsp;&mdash; &quot;Frat House&quot;, который удостоился гран-при кинофестиваля в&nbsp;Сандэнсе. На&nbsp;фестивале <b>Тодд</b> познакомился с&nbsp;<b>Айваном Рейтманом</b>, под&nbsp;опекой которого снял два своих первых полнометражных фильма&nbsp;&mdash; &quot;Дорожное приключение&quot; (2000) и &quot;Старая закалка&quot; (2003). Затем была сценарная работа над&nbsp;фильмом &quot;Борат&quot; (2006), за&nbsp;которую <b>Тодда</b> номинировали на &quot;Оскар&quot;.</p>\r\n<p style=\"text-align: justify;\">Снимал такие фильмы, как &quot;Убойная парочка: Старски и&nbsp;Хатч&quot; (2004), &quot;Школа негодяев&quot; (2006), &quot;Мужчины&quot; (2008) и другие.</p>",
      "url": "https://kino-teatr.ua/person/1915.phtml",
      "profession_id": 1
    }]), 
                       [{
                         "id": 1915,
                         "imgPerson": {src:"http://kino-teatr.ua:8081/services/api/person/1915/photo?apiKey=pol1kh111&width=300&height=400&ratio=1"},
                         "lblPersonName": "Тодд Филлипс",
                         "lblPersonOriginName": "Todd Phillips",
                         "lblPersonRaiting": "Рейтинг: 4.66667/10"
                       }]);
  test8.test();
  testList.push(test8);
  
  var test9 = new Test("Format an array to segment data", "Result for empty array", formatToSegmentData([]), 
                       [{
                         "lblPersonName": "Данных нет"
                       }]);
  test9.test();
  testList.push(test9);
  
  var responseSessionsByFilm = {
    "content": [
        {
            "id": 91843313,
            "begin": "2018-12-27",
            "end": "2018-12-27",
            "times": [
                {
                    "id": 192959582,
                    "time": "16:15:00",
                    "prices": "80-140",
                    "3d": true,
                    "purchase_allowed": false
                },
                {
                    "id": 192959583,
                    "time": "18:10:00",
                    "prices": "95-170",
                    "3d": true,
                    "purchase_allowed": false
                },
                {
                    "id": 192959584,
                    "time": "20:05:00",
                    "prices": "95-170",
                    "3d": true,
                    "purchase_allowed": false
                }
            ],
            "film_id": 49244,
            "hall_id": 111
        },
        {
            "id": 91843615,
            "begin": "2018-12-27",
            "end": "2018-12-27",
            "times": [
                {
                    "id": 192960314,
                    "time": "17:15:00",
                    "prices": "80-140",
                    "3d": true,
                    "purchase_allowed": false
                }
            ],
            "film_id": 49244,
            "hall_id": 112
        },
        {
            "id": 91842144,
            "begin": "2018-12-27",
            "end": "2019-01-02",
            "times": [
                {
                    "id": 192956714,
                    "time": "10:05:00",
                    "prices": "45, 270",
                    "3d": false,
                    "purchase_allowed": false
                },
                {
                    "id": 192956715,
                    "time": "13:50:00",
                    "prices": "45, 270",
                    "3d": false,
                    "purchase_allowed": false
                },
                {
                    "id": 192956716,
                    "time": "16:15:00",
                    "prices": "45, 270",
                    "3d": false,
                    "purchase_allowed": false
                },
                {
                    "id": 192956717,
                    "time": "18:10:00",
                    "prices": "45, 270",
                    "3d": false,
                    "purchase_allowed": false
                },
                {
                    "id": 192956718,
                    "time": "12:30:00",
                    "prices": "45, 270",
                    "3d": true,
                    "purchase_allowed": false
                }
            ],
            "film_id": 49244,
            "hall_id": 136
        },
        {
            "id": 91843489,
            "begin": "2018-12-27",
            "end": "2018-12-27",
            "times": [
                {
                    "id": 192960077,
                    "time": "18:10:00",
                    "prices": "85 95",
                    "3d": true,
                    "purchase_allowed": true
                }
            ],
            "film_id": 49244,
            "hall_id": 220
        },
        {
            "id": 91839665,
            "begin": "2018-12-27",
            "end": "2018-12-27",
            "times": [
                {
                    "id": 192951631,
                    "time": "17:00:00",
                    "prices": "80-140",
                    "3d": true,
                    "purchase_allowed": false
                },
                {
                    "id": 192951632,
                    "time": "18:55:00",
                    "prices": "95-170",
                    "3d": true,
                    "purchase_allowed": false
                }
            ],
            "film_id": 49244,
            "hall_id": 588
        },
        {
            "id": 91843139,
            "begin": "2018-12-27",
            "end": "2018-12-27",
            "times": [
                {
                    "id": 192959140,
                    "time": "15:50:00",
                    "prices": "80-140",
                    "3d": true,
                    "purchase_allowed": false
                },
                {
                    "id": 192959141,
                    "time": "17:45:00",
                    "prices": "80-140",
                    "3d": false,
                    "purchase_allowed": false
                },
                {
                    "id": 192959142,
                    "time": "19:40:00",
                    "prices": "95-170",
                    "3d": true,
                    "purchase_allowed": false
                },
                {
                    "id": 192959143,
                    "time": "21:35:00",
                    "prices": "95-170",
                    "3d": true,
                    "purchase_allowed": false
                },
                {
                    "id": 192959144,
                    "time": "23:30:00",
                    "prices": "50-150",
                    "3d": true,
                    "purchase_allowed": false
                }
            ],
            "film_id": 49244,
            "hall_id": 592
        }
    ],
    "totalPages": 1,
    "totalElements": 6,
    "number": 0,
    "size": 2000,
    "numberOfElements": 6,
    "first": true,
    "last": true,
    "films": [
        {
            "id": 49244,
            "title": "Гринч",
            "title_orig": "The Grinch",
            "title_alt": null,
            "premiere_ukraine": "2018-12-20",
            "premiere_world": "2018-11-08",
            "duration": 86,
            "year": 2018,
            "age_limit": 1,
            "budget": 75,
            "budget_ccy": "usd",
            "rating": 8.2384615,
            "votes": 130,
            "tmdb_rating": 6.2,
            "tmdb_votes": 463,
            "imdb_id": "tt2709692",
            "total_shows": 130187,
            "url": "https://kino-teatr.ua/film/49244.phtml",
            "studio_ids": [
                49,
                1442
            ],
            "genre_ids": [
                16,
                6,
                22,
                23
            ],
            "country_ids": [
                16,
                4,
                6,
                12
            ]
        }
    ],
    "halls": [
        {
            "id": 112,
            "name": "Зал 2",
            "cinema_id": 100,
            "3d": false
        },
        {
            "id": 592,
            "name": "Зал 6",
            "cinema_id": 295,
            "3d": true
        },
        {
            "id": 136,
            "name": "Большой (3D)",
            "cinema_id": 108,
            "3d": true
        },
        {
            "id": 220,
            "name": "Зал 4",
            "cinema_id": 137,
            "3d": true
        },
        {
            "id": 588,
            "name": "Зал 2",
            "cinema_id": 295,
            "3d": true
        },
        {
            "id": 111,
            "name": "Зал 1",
            "cinema_id": 100,
            "3d": true
        }
    ],
    "cinemas": [
        {
            "id": 100,
            "city_id": 5,
            "name": "Мультиплекс IMAX Dafi",
            "site": "http://multiplex.ua/",
            "phone": "0 800 505 333",
            "latitude": "48.425356",
            "longitude": "35.021582",
            "address": "бульв. Звездный, 1а, 2-й этаж ТРЦ «ДАФИ»",
            "notice": null,
            "num_photos": 5,
            "description": null,
            "url": "https://kino-teatr.ua/cinema/100.phtml"
        },
        {
            "id": 295,
            "city_id": 5,
            "name": "Мультиплекс ТРЦ Караван",
            "site": "http://www.multiplex.ua/",
            "phone": "0 800 505 333",
            "latitude": "48.529313",
            "longitude": "35.030797",
            "address": "пгт. ЮБИЛЕЙНЫЙ, ул. НИЖНЕДНЕПРОВСКАЯ, 17.",
            "notice": null,
            "num_photos": 0,
            "description": null,
            "url": "https://kino-teatr.ua/cinema/295.phtml"
        },
        {
            "id": 137,
            "city_id": 5,
            "name": "Мост-кино",
            "site": "http://cinema.dp.ua",
            "phone": "(056) 375-85-00, (067) 818-80-75",
            "latitude": "48.465893",
            "longitude": "35.051773",
            "address": "ул. Глинки, 2. 2 эт. ТРЦ Мост-сити",
            "notice": "<p><b>С 11 по 15 октября</b><br />\r\nв кинотеатре<b> Мост-кино </b>пройдет<br />\r\n<b>ФЕСТИВАЛЬ НОВОЕ НЕМЕЦКОЕ КИНО</b></p>",
            "num_photos": 6,
            "description": null,
            "url": "https://kino-teatr.ua/cinema/137.phtml"
        },
        {
            "id": 108,
            "city_id": 5,
            "name": "Правда-кино",
            "site": "http://www.pravda-kino.dp.ua",
            "phone": "373-60-00",
            "latitude": "48.517642",
            "longitude": "35.080504",
            "address": "просп. им. Газеты Правда, 86",
            "notice": null,
            "num_photos": 7,
            "description": null,
            "url": "https://kino-teatr.ua/cinema/108.phtml"
        }
    ],
    "genres": [
        {
            "id": 16,
            "name": "анимация"
        },
        {
            "id": 6,
            "name": "комедия"
        },
        {
            "id": 22,
            "name": "семейный"
        },
        {
            "id": 23,
            "name": "фэнтези"
        }
    ],
    "studios": [
        {
            "id": 49,
            "name": "Universal Pictures"
        },
        {
            "id": 1442,
            "name": "Illumination Entertainment"
        }
    ],
    "countries": [
        {
            "id": 16,
            "name": "Китай"
        },
        {
            "id": 4,
            "name": "США"
        },
        {
            "id": 6,
            "name": "Франция"
        },
        {
            "id": 12,
            "name": "Япония"
        }
    ]
};
  
  var test10 = new Test("Get hall name by id", "Correct name", getHallName(111, responseSessionsByFilm), "Зал 1");
  test10.test();
  testList.push(test10);

  var test11 = new Test("getDateFromStr", "Correct sessions", getDateFromStr("2018-12-27", "16:15:00"), new Date(2018,11,27,16,15,0) );
  test11.test();
  testList.push(test11);
  
  var test12 = new Test("Get cinema name by id", "Correct name", getCinemaName(responseSessionsByFilm.cinemas, 100), "Мультиплекс IMAX Dafi");
  test12.test();
  testList.push(test12);
  
  var test13 = new Test("Add hall name", "Correct adding", addHallName(responseSessionsByFilm.content, responseSessionsByFilm), 
                       [
        {
            "id": 91843313,
            "begin": "2018-12-27",
            "end": "2018-12-27",
            "times": [
                {
                    "id": 192959582,
                    "time": "16:15:00",
                    "prices": "80-140",
                    "3d": true,
                    "purchase_allowed": false,
                    "hallName": "Зал 1"
                },
                {
                    "id": 192959583,
                    "time": "18:10:00",
                    "prices": "95-170",
                    "3d": true,
                    "purchase_allowed": false,
                    "hallName": "Зал 1"
                },
                {
                    "id": 192959584,
                    "time": "20:05:00",
                    "prices": "95-170",
                    "3d": true,
                    "purchase_allowed": false,
                    "hallName": "Зал 1"
                }
            ],
            "film_id": 49244,
            "hall_id": 111
        },
        {
            "id": 91843615,
            "begin": "2018-12-27",
            "end": "2018-12-27",
            "times": [
                {
                    "id": 192960314,
                    "time": "17:15:00",
                    "prices": "80-140",
                    "3d": true,
                    "purchase_allowed": false,
                    "hallName": "Зал 2"
                }
            ],
            "film_id": 49244,
            "hall_id": 112
        },
        {
            "id": 91842144,
            "begin": "2018-12-27",
            "end": "2019-01-02",
            "times": [
                {
                    "id": 192956714,
                    "time": "10:05:00",
                    "prices": "45, 270",
                    "3d": false,
                    "purchase_allowed": false,
                    "hallName": "Большой (3D)"
                },
                {
                    "id": 192956715,
                    "time": "13:50:00",
                    "prices": "45, 270",
                    "3d": false,
                    "purchase_allowed": false,
                    "hallName": "Большой (3D)"
                },
                {
                    "id": 192956716,
                    "time": "16:15:00",
                    "prices": "45, 270",
                    "3d": false,
                    "purchase_allowed": false,
                    "hallName": "Большой (3D)"
                },
                {
                    "id": 192956717,
                    "time": "18:10:00",
                    "prices": "45, 270",
                    "3d": false,
                    "purchase_allowed": false,
                    "hallName": "Большой (3D)"
                },
                {
                    "id": 192956718,
                    "time": "12:30:00",
                    "prices": "45, 270",
                    "3d": true,
                    "purchase_allowed": false,
                    "hallName": "Большой (3D)"
                }
            ],
            "film_id": 49244,
            "hall_id": 136
        },
        {
            "id": 91843489,
            "begin": "2018-12-27",
            "end": "2018-12-27",
            "times": [
                {
                    "id": 192960077,
                    "time": "18:10:00",
                    "prices": "85 95",
                    "3d": true,
                    "purchase_allowed": true,
                    "hallName": "Зал 4"
                }
            ],
            "film_id": 49244,
            "hall_id": 220
        },
        {
            "id": 91839665,
            "begin": "2018-12-27",
            "end": "2018-12-27",
            "times": [
                {
                    "id": 192951631,
                    "time": "17:00:00",
                    "prices": "80-140",
                    "3d": true,
                    "purchase_allowed": false,
                    "hallName": "Зал 2"
                },
                {
                    "id": 192951632,
                    "time": "18:55:00",
                    "prices": "95-170",
                    "3d": true,
                    "purchase_allowed": false,
                    "hallName": "Зал 2"
                }
            ],
            "film_id": 49244,
            "hall_id": 588
        },
        {
            "id": 91843139,
            "begin": "2018-12-27",
            "end": "2018-12-27",
            "times": [
                {
                    "id": 192959140,
                    "time": "15:50:00",
                    "prices": "80-140",
                    "3d": true,
                    "purchase_allowed": false,
                    "hallName": "Зал 6"
                },
                {
                    "id": 192959141,
                    "time": "17:45:00",
                    "prices": "80-140",
                    "3d": false,
                    "purchase_allowed": false,
                    "hallName": "Зал 6"
                },
                {
                    "id": 192959142,
                    "time": "19:40:00",
                    "prices": "95-170",
                    "3d": true,
                    "purchase_allowed": false,
                    "hallName": "Зал 6"
                },
                {
                    "id": 192959143,
                    "time": "21:35:00",
                    "prices": "95-170",
                    "3d": true,
                    "purchase_allowed": false,
                    "hallName": "Зал 6"
                },
                {
                    "id": 192959144,
                    "time": "23:30:00",
                    "prices": "50-150",
                    "3d": true,
                    "purchase_allowed": false,
                    "hallName": "Зал 6"
                }
            ],
            "film_id": 49244,
            "hall_id": 592
        }
    ]
                       );
  test13.test();
  testList.push(test13);
  
  var test14 = new Test("Get halls by cinema id", "Correct halls", getHalls(responseSessionsByFilm.halls, 100), 
                       [
    {
      id: 112,
      name: "Зал 2"
    },
    {
      id: 111,
      name: "Зал 1"
    }
  ]
                      );
  test14.test();
  testList.push(test14);
  
  
  return testList;
}

