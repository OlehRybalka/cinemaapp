define(function() {

	return {
      
      createLblTitle: function(n, title) {
        var lblBasic = 
          {
            "id":"lblTitle" + n,
            "skin":"LblHeader2Yellow",
            "text":title,
            "isVisible":true
          };

        var lblLayout =
          {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
          };

        var lblPsp ={wrapping:constants.WIDGET_TEXT_WORD_WRAP};

        var lbl = new kony.ui.Label(lblBasic, lblLayout, lblPsp);
//         lbl.width = "100%";
//         lbl.height = "35px";
        return lbl;
      },
      
      createLblTime: function(r, n, time, data) {
        var lblBasic = 
          {
            "id":"lblSession" + r + n,
            "skin":"lblSessionSkn",
            "text":time,
            "isVisible":true
          };

        var lblLayout =
          {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
          };

        var lblPsp ={wrapping:constants.WIDGET_TEXT_WORD_WRAP};

        var lbl = new kony.ui.Label(lblBasic, lblLayout, lblPsp);
        lbl.width = "50px";
        lbl.height = "35px";
        lbl.data = data;
        return lbl;
      },
      
      createLbl3D: function(r, n, session) {
        console.log("in 3d");
        console.log('session["3d"]' +session["3d"]);
        var text;
        if (session["3d"]) {
          text = " 3D";
        } else {
          text = "";
        }
          
        var lblBasic = 
          {
            "id":"lbl3D" + r + n,
            "skin":"lbl3DSkn",
            "text":text,
            "isVisible":true
          };

        var lblLayout =
          {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
          };

        var lblPsp ={wrapping:constants.WIDGET_TEXT_WORD_WRAP};

        var lbl = new kony.ui.Label(lblBasic, lblLayout, lblPsp);
        lbl.left = "5px";
        lbl.height = "35px";
        return lbl;
      },
      
      createTimeContainer: function(r, n) {
        var FlexbasicConfig = {
          "id": "flxContainerTime" + r + n,
          "top": "0",
          "width": "20%",
          "height": "50px",
          "isVisible": true,
          "clipBounds": true,
          "layoutType": kony.flex.FLOW_HORIZONTAL,
          "contentAlignment": constants.CONTENT_ALIGN_CENTER
        };
        var FlexLayoutConfig = {};
        var FlexPSPConfig = {};
        var flexContainerTime = new kony.ui.FlexContainer(FlexbasicConfig, FlexLayoutConfig, FlexPSPConfig);                                                   
        return flexContainerTime;
      },
      
      createRow: function(n) {
        var FlexbasicConfig = {
          "id": "flxSessionsRow" + n,
          "top": "10px",
          "width": "100%",
          "height": "50px",
          "skin": "flxSessionsRow",
          "isVisible": true,
          "clipBounds": true,
          "layoutType": kony.flex.FLOW_HORIZONTAL
        };
        var FlexLayoutConfig = {};
        var FlexPSPConfig = {};
        var flexContainerRow = new kony.ui.FlexContainer(FlexbasicConfig, FlexLayoutConfig, FlexPSPConfig);                                                   
        return flexContainerRow;
      },
      
	  createCinemaContainer: function(n) {
        console.log("in createCinemaContainer");
        var FlexbasicConfig = {
          "id": "flxCinemaContainer" + n,
          "top": "10px",
          "width": "100%",
//           "height": "50px",
          "skin": "slFbox",
          "isVisible": true,
//           "clipBounds": true,
          "layoutType": kony.flex.FLOW_VERTICAL
        };
        var FlexLayoutConfig = {};
        var FlexPSPConfig = {};
        var flexCinemaContainer = new kony.ui.FlexContainer(FlexbasicConfig, FlexLayoutConfig, FlexPSPConfig);                                                   
        flexCinemaContainer.height = kony.flex.USE_PREFERED_SIZE;
        return flexCinemaContainer;
      },
      
      createRows: function(arr) {
        var rows = [];
        var timesInRow = 5;
        var startInd = 0;
        for (var i = 0; startInd < arr.length; i++) {
          var row = this.createRow(i);
          rows.push(row);
          
          var endInd = startInd + timesInRow;
          var sessionsInRow = arr.slice(startInd, endInd);
         
          for (var j = 0; j < sessionsInRow.length; j++) {
            var timeContainer = this.createTimeContainer(i, j);
            var time = this.createLblTime(i, j, sessionsInRow[j].time.slice(0,5), sessionsInRow[j]);
            var threeD = this.createLbl3D(i, j, sessionsInRow[j]);

            timeContainer.addAt(time,0);
            timeContainer.addAt(threeD,1);
            row.addAt(timeContainer, j);
          }
          startInd += timesInRow;
        }
        return rows;
      },      
      
      buildScreening: function(arr) {
        console.log("in buildScreening");
        var cinemaContainers = [];
        for (var j = 0; j < arr.length; j++) {
          var cinemaContainer = this.createCinemaContainer(j);
          cinemaContainers.push(cinemaContainer);
          
          var title = this.createLblTitle(j, arr[j].title);
          console.log(title);
          cinemaContainer.addAt(title, 0);

          var rows = this.createRows(arr[j].sessions);
          console.log(rows);
          for (var i = 0; i < rows.length; i++) {
            cinemaContainer.addAt(rows[i], i+1);
          }
        }
        this.view.flxComScreenings.height = kony.flex.USE_PREFERED_SIZE;
        return cinemaContainers;
      }
      
//       buildScreening: function(arr) {
//         console.log("in buildScreening");
//         console.log(arr);
//         var rows = [];
//         var timesInRow = 5;
//         var startInd = 0;
        
//         for (var i = 0; startInd < arr.length; i++) {
//           var row = this.createRow(i);
//           rows.push(row);
          
//           var endInd = startInd + timesInRow;
//           var sessionsInRow = arr.slice(startInd, endInd);
         
//           for (var j = 0; j < sessionsInRow.length; j++) {
//             var timeContainer = this.createTimeContainer(i, j);
//             var time = this.createLblTime(i, j, sessionsInRow[j].time.slice(0,5), sessionsInRow[j]);
//             var threeD = this.createLbl3D(i, j, sessionsInRow[j]);

//             timeContainer.addAt(time,0);
//             timeContainer.addAt(threeD,1);
//             row.addAt(timeContainer, j);
//           }
//           startInd += timesInRow;
//         }
//         console.log(rows);
//         return rows;
//       }
	};
});