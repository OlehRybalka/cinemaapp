define(function() {

  return {  

    navigateToFrmCatalogWeb : function(){
      var frmCatalogWeb = _kony.mvc.GetController('frmCatalogWeb', true);
      chosenCityId = this.getListBox()[0];
      getCinemasWeb(frmCatalogWeb, chosenCityId);
      getFilmsWeb(frmCatalogWeb, chosenCityId);
    },

    getCities: function () {
      var data;
      var self = this;
      self.view.flxNavigation.flxHome.onTouchEnd = self.goToCatalog;
      
      function searchData(){
        var request = new kony.net.HttpRequest();
        request.onReadyStateChange=httpCallbackHandler;
        request.open(constants.HTTP_METHOD_GET, "http://cors-anywhere.herokuapp.com/http://kino-teatr.ua:8081/services/api/cities?apiKey=pol1kh111&size=100");
        request.setRequestHeader('Content-type','application/json');
        request.send();
      }

      function httpCallbackHandler(){
        if(this.readyState == constants.HTTP_READY_STATE_DONE){   
          var setDataList = [];
          var response = JSON.parse(this.response);
          data = response.content;       

          for (var i = 0; i < data.length; i++) {
            var keyList = [data[i].id, data[i].name];
            setDataList.push(keyList);

            if (chosenCityId == data[i].id) {
              var temp = [data[i].id, data[i].name];
              setDataList.unshift(temp);
              setDataList.pop(keyList);
            }
          }
          self.view.lbxCitiesKA.masterData = setDataList; 
          var selectedCity = self.view.lbxCitiesKA.selectedKeyValue[1];
          self.view.lblCity.text = selectedCity;
        }
      }
      searchData();   

    },

    getListBox: function () {
      try {
        return this.view.lbxCitiesKA.selectedKeyValue;
      } catch (err) {
        return null;
      }
    },
    
    goToCatalog: function() {
      var frm = new kony.mvc.Navigation("frmCatalogWeb");
      frm.navigate(chosenCityId);
    }
  };
});