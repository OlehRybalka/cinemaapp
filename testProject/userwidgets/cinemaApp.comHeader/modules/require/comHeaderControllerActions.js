define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxAbout **/
    AS_FlexContainer_e62b8f1ebe0949cbb01dfbb2d27d0697: function AS_FlexContainer_e62b8f1ebe0949cbb01dfbb2d27d0697(eventobject) {
        var self = this;
    },
    /** onSelection defined for lbxCitiesKA **/
    AS_ListBox_bbc9a8e3e5744597a1f015f213c3bad6: function AS_ListBox_bbc9a8e3e5744597a1f015f213c3bad6(eventobject) {
        var self = this;
        return self.navigateToFrmCatalogWeb.call(this);
    },
    /** preShow defined for comHeader **/
    AS_FlexContainer_a605838cefca49408a28139b04dd70a7: function AS_FlexContainer_a605838cefca49408a28139b04dd70a7(eventobject) {
        var self = this;
        return self.getCities.call(this);
    }
});