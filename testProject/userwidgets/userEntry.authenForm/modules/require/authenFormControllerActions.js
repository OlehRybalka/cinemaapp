define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnSignUpKA **/
    AS_Button_jc60e71ac9054d0c97f279a72da4ca20: function AS_Button_jc60e71ac9054d0c97f279a72da4ca20(eventobject) {
        var self = this;
        self.sendNewUser.call(this);
        self.navigate.call(this);
    },
    /** onClick defined for Button0b4df4875d95d4f **/
    AS_Button_fce1a3325ffa4f6eab95ad2e730ff3b0: function AS_Button_fce1a3325ffa4f6eab95ad2e730ff3b0(eventobject) {
        var self = this;
        return self.checkLogin.call(this);
    },
    /** preShow defined for authenForm **/
    AS_FlexContainer_ff8c1afdf0724ca4ba6852c80980aff4: function AS_FlexContainer_ff8c1afdf0724ca4ba6852c80980aff4(eventobject) {
        var self = this;
        self.getRegCities.call(this);
        self.navigate.call(this);
    }
});