define(function() {
  var cors = 'http://cors-anywhere.herokuapp.com/';
  var authentificationUserStatus;
  return {   
    registrationUser: {},
    loginUser: {},
    user : {},
    navigateData : {},
    data : {}, 

    navigate: function(){     
      var self = this;
      var textBoxes = ['tbxUserEmailKa', 'tbxUserPasswordKA', 'tbxUserEmailLoginKa','tbxUserPasswordLoginKA'];	
      this.view.tbxUserEmailKa.onKeyUp = function(){
        self.validateEmail('lblEmailWarningKA', 'tbxUserEmailKa');
      };
      this.view.tbxUserPasswordKA.onKeyUp = function(){
        self.validatePassword('lblPasswordWarningKA', 'tbxUserPasswordKA');
      };
      this.view.tbxUserEmailLoginKa.onKeyUp = function(){
        self.validateEmail('lblLoginEmailWarningKA', 'tbxUserEmailLoginKa');
      };
      this.view.tbxUserPasswordLoginKA.onKeyUp = function(){
        self.validatePassword('lblLoginPasswordWarningKA', 'tbxUserPasswordLoginKA');
      };
      this.view.isVisible = false;
    },

    validateEmail: function(label, textBox) {   
      var email = this.view[textBox].text;
      if(this.validateRegexEmail(email)){          
        this.view[label].text = '';
        this.data.validateEmailstatus = true;
        return true;
      }else{
        this.view[label].text = '*Email not valid';
        this.data.validateEmailstatus = false;
      }          

    },

    validatePassword: function(label, textBox) {
      var password = this.view[textBox].text;
      if(this.validateRegexPassword(password)){          
        this.view[label].text = '';
        this.data.validatePasswordstatus = true;
        return true;
      }else{
        this.view[label].text = '*Password not valid';
        this.data.validatePasswordstatus = false;
      }       
    },

    sendNewUser : function(){      
      var self = this;      
      var validateResult = this.validateRegistration();  
      console.log(this.data);
      if(validateResult && this.data.validateEmailstatus && this.data.validatePasswordstatus){
        console.log(this.data);
        this.data.email = validateResult.email;
        this.data.password = validateResult.password;
        this.data.name = validateResult.name;
        this.data.city = parseInt(validateResult.cityId);     
        searchData(this.data);
      }

      function searchData(data){
        var request = new kony.net.HttpRequest();
        request.onReadyStateChange=httpCallbackHandler;
        request.open(constants.HTTP_METHOD_POST, cors + 'https://cityflora.com.ua/connect.php');
        request.setRequestHeader('Content-Type', 'application/json');
        request.setRequestHeader('charset','utf-8');
        var sendData = JSON.stringify(data);      
        request.send(sendData);
      }

      function httpCallbackHandler(){
        if(this.readyState == constants.HTTP_READY_STATE_DONE){        
          if(this.response === 'false'){
            self.view.lblWarningMessageKA.text = '*This user already exist';           
            return;            
          }else if(this.response){             
            var resp = JSON.parse(this.response);
            console.log(resp);
            var navData = {};          
            navData.userId = resp.id;
            navData.cityId = resp.city;	
            authentificationUserStatus = true;
            self.view.isVisible = false;
          }
        }
      }

    },

    getRegCities: function () {
      var data;
      var self = this;
      function searchData(){
        var request = new kony.net.HttpRequest();
        request.onReadyStateChange=httpCallbackHandler;
        request.open(constants.HTTP_METHOD_GET, cors+"http://kino-teatr.ua:8081/services/api/cities?apiKey=pol1kh111&size=100");
        request.setRequestHeader('Content-type','application/json');
        request.send();
      }

      function httpCallbackHandler(){
        if(this.readyState == constants.HTTP_READY_STATE_DONE){   
          var setDataList = [];
          data = JSON.parse(this.response);  
          data = data.content;         

          for (var i = 0; i < data.length; i++) {
            var keyList = [data[i].id, data[i].name];
            setDataList.push(keyList);
          }
          self.view.lbxCitiesKA.masterData = setDataList;         
        }
      }
      searchData();   

    },   

    validateRegistration : function(){
      var city = this.getListBox();
      var cityIndex = 0;
      this.registrationUser.name = this.view.tbxUserNameKA.text;
      this.registrationUser.email = this.view.tbxUserEmailKa.text;
      this.registrationUser.password = this.view.tbxUserPasswordKA.text;      
      this.registrationUser.repeatPassword = this.view.tbxUserConfirmPasswordKA.text;

      if(city !== null){
        this.registrationUser.cityId = city[cityIndex];
      }

      for(var value in this.registrationUser){        
        if(this.registrationUser[value] === ''){
          this.view.lblWarningMessageKA.text = '*Please fill all rows';
          return;
        }
      }

      if (city === null) {
        this.view.lblWarningMessageKA.text = '*Please choose city';

        return;
      } else if(this.registrationUser.password !== this.registrationUser.repeatPassword){
        this.view.lblWarningMessageKA.text = '*Passwords missmatch!';
        return;  
      }   

      this.registrationUser.status = 'true';        
      return this.registrationUser;    
    },   

    checkLogin : function(){      
      var self = this;
      var validateResult = this.validateLogin();    
      if(validateResult.status === 'true'){
        this.user.email = validateResult.email;
        this.user.password = validateResult.password;          
        searchData(this.user);

      }

      function searchData(data){          

        var request = new kony.net.HttpRequest();
        request.onReadyStateChange=httpCallbackHandler;
        request.open(constants.HTTP_METHOD_POST, cors + "https://cityflora.com.ua/login.php");
        request.setRequestHeader("Content-Type", "application/json");
        request.setRequestHeader('charset','utf-8');
        var sendData = JSON.stringify(data);      
        request.send(sendData);
      }

      function httpCallbackHandler(){
        if(this.readyState == constants.HTTP_READY_STATE_DONE){        
          if(this.response === 'null'){
            self.view.lblWarningMessageKA.text = '*This user not exist!';

            return;           
          }else if(this.response){             
            var resp = JSON.parse(this.response);            
            var navData = {};          
            navData.userId = resp.id;
            navData.cityId = resp.city;	
            navData.userName = resp.name;
            authentificationUserStatus = true;
            self.view.isVisible = false;            
            console.log(navData);             
          }
        }
      }

    },
    
     validateLogin : function(){
      this.loginUser.email = this.view.tbxUserEmailLoginKa.text;
      this.loginUser.password = this.view.tbxUserPasswordLoginKA.text;      
      for(var value in this.loginUser){        
        if(this.loginUser[value] === ''){
          this.view.lblWarningMessageKA.text = '*Please fill all rows';
          return;
        }
      }
      this.loginUser.status = 'true';    

      return this.loginUser;      
    },

    validateRegexEmail : function(email){
      var re = /^\w+([\._%+-]*\w+)*@\w+([\.-]*\w+)*\.[a-z]{2,}$/;
      if (re.test(email)){

        return true;
      } 
    },

    validateRegexPassword : function(password){
      var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}$/;
      if (re.test(password)){

        return true;
      }
    },

    authStatus : function(){
      return authentificationUserStatus;
    },

    getListBox: function () {
      try {
        return this.view.lbxCitiesKA.selectedKeyValue;
      } catch (err) {
        return null;
      }
    }
  };
});