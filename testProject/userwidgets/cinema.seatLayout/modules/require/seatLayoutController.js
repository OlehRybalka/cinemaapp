define(function() {
  
  var maxFlexWidth = 0;
  var totalPrice = 0;
  var tickets = [];
  var self;
  
  function resetCart () {
    tickets = [];
    totalPrice = 0;
    self.view.ticketsSeg.removeAll();
    self.view.priceLbl.text = "цена: 0 грн.";
  }
  
  function addToCart (seat) {
    var segObj = {
      "rowLbl" : {"text" : "ряд: " + seat.info.row},
      "placeLbl" : {"text" : "место: " + seat.text},
      "priceLbl" : {"text" : seat.info.price + " грн."}
    };
    tickets.push(segObj);
    self.view.ticketsSeg.setData(tickets);
  }
  
  function removeFromCart (seat) {
    for (var i = 0; i < tickets.length; i++) {
      if (tickets[i].rowLbl.text == "ряд: " + seat.info.row && tickets[i].placeLbl.text == "место: " + seat.text) {
        tickets.splice(i, 1);
      }
    }
    self.view.ticketsSeg.setData(tickets);
  }
  
  function openCloseCart () {
    if (self.view.cartFlex.isVisible === false) {
      if (kony.application.getCurrentBreakpoint() == 992) {
        self.view.cartFlex.width = "250px";
        self.view.cartFlex.setVisibility(true);
      } else if (kony.application.getCurrentBreakpoint() == 768) {
        self.view.cartFlex.width = "200px";
        self.view.cartFlex.setVisibility(true);
      }
      self.view.forceLayout();
    } else {
      self.view.cartFlex.width = "22%";
      self.view.cartFlex.setVisibility(false);
      self.view.forceLayout();
    }
  }
  
  function mainFlexCreate () {
     var FlexbasicConfig = {
      "top" : "40px",
      "id": "mainSeatsFlex",
      "height": "100%",
      "zIndex": 1,
      "isVisible": true,
      "skin" : "slFbox",
      "layoutType" : kony.flex.FLOW_VERTICAL,
    };
    var FlexLayoutConfig = {};
    var FlexPSPConfig = {};
    var flexContainer = new kony.ui.FlexContainer(FlexbasicConfig, FlexLayoutConfig, FlexPSPConfig);
    return flexContainer;
  }
  
  
  function seatCreate (seatNumber, seatId) {
    var LabelbasicConfig = {
      "id":  "seatLabel" + seatId,
      "isVisible": true,
      "skin": "lblSeatFree",
      "text": seatNumber,
    };
    var LabelLayoutConfig= {
      "contentAlignment": constants.CONTENT_ALIGN_CENTER,
    };
    var LabelPSPConfig = {};
    var seat = new kony.ui.Label(LabelbasicConfig, LabelLayoutConfig, LabelPSPConfig);
    seat.width = "25px";
    seat.height = "25px";
    seat.left = "1px";
    seat.right = "1px";
    return seat;
  }


  function flexCreate (flexId) {
    var FlexbasicConfig = {
      "id": "rowFlex" + flexId,
      "height": "40px",
      "width" : "0px",
      "zIndex": 1,
      "isVisible": true,
      "skin" : "slFbox",
      "layoutType" : kony.flex.FLOW_HORIZONTAL
    };
    var FlexLayoutConfig = {};
    var FlexPSPConfig = {};
    var flexContainer = new kony.ui.FlexContainer(FlexbasicConfig, FlexLayoutConfig, FlexPSPConfig);
    return flexContainer;
  }


  function rowNumberCreate (rowId, rowNum) {
    var LabelbasicConfig = {
      "id":  "rowNum" + rowId,
      "isVisible": true,
      "skin": "LblHeader4Yellow",
      "text": rowNum,
    };
    var LabelLayoutConfig= {
      "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
    };
    var LabelPSPConfig = {};
    var seat = new kony.ui.Label(LabelbasicConfig, LabelLayoutConfig, LabelPSPConfig);
    seat.width = "50px";
    seat.height = "25px";
    seat.left = "25px";
    return seat;
  }

  function selectSeatHandler(seat, seatFree) {
    seat.onClick = function () {  
      if (seat.skin == "lblSeatFree" && seatFree) {
        seat.skin = "lblSeatChosen";
        addToCart(seat);
        totalPrice += seat.info.price;
        self.view.priceLbl.text = "цена: " + totalPrice + " грн.";
      } else if (seatFree) {
        seat.skin = "lblSeatFree";
        removeFromCart(seat);
        totalPrice -= seat.info.price;
        self.view.priceLbl.text = "цена: " + totalPrice + " грн.";
      }      
    };
  }

  function checkSeatStatus(status, seat) {
    if (status !== "0") {
      seat.skin = "lblSeatTaken";
      return false;
    } else {
      return true;
    }
  }
  
  function setRowWidth (rowFlex, seatsNum, seatWidth, seatLeft, seatRight, rowNumWidth) {
    var width = (seatsNum * (seatWidth + seatLeft + seatRight)) + rowNumWidth + 50;
    rowFlex.width = width + "px";
    rowFlex.centerX = "50%";
    if (maxFlexWidth < width) {maxFlexWidth = width;}
    return rowFlex;
  }
  
  function showFilmInfo (hall, time) {
    if (hall && time) {
      self.view.cinemaLbl.text = hall;
      self.view.timeLbl.text = time;
    } else {
      self.view.cinemaLbl.text = "";
      self.view.timeLbl.text = "";
    }
  }  
  
	return {
      
      showScheme : function (scheme) {
        self = this;
		resetCart();
        this.view.cartImg.onTouchStart = openCloseCart;
        if (kony.application.getCurrentBreakpoint() == 1200) {this.view.cartFlex.width = "22%";}
        var mainSeatsFlex = mainFlexCreate();
        var seat;
        for (var i = 0; i < scheme.sectors[0].rows.length; i++) {
          var info = { row : i + 1, price : scheme.prices[0].price };
          var seats = scheme.sectors[0].rows[i].places;
          var rowStr = "ряд " + (i + 1) + " ";
          var rowId = rowNumberCreate (i, rowStr);
          var rowFlex = flexCreate(i);
          setRowWidth(rowFlex, seats.length, 25, 1, 1, 50);
          rowFlex.add(rowId);
          for (var j = 0; j < seats.length; j++) {
            seat = seatCreate(seats[j].number, seats[j].ids[0]);
            seat.info = info;
            var seatFree = checkSeatStatus(seats[j].status, seat);
           	selectSeatHandler.call(this, seat, seatFree);
            rowFlex.add(seat);
          }
          mainSeatsFlex.add(rowFlex);
        }
        mainSeatsFlex.width = maxFlexWidth + "px";
        this.view.seatsFlex.add(mainSeatsFlex);
        this.view.seatsFlex.height = ((scheme.sectors[0].rows.length + 1) * 40) + 100 + "px";
        showFilmInfo(scheme.site_name, scheme.date);
      }
    };
});