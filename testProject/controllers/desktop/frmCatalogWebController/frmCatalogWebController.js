define({ 

    onNavigate: function(data) {
      var self = this;
      self.view.sgmFilms.onRowClick = this.getRowFilmDataWeb;
      self.view.sgmCinemas.onRowClick = this.getRowCinemaDataWeb;
      self.view.lblChosenCity.onTouchStart = this.getFilmsByCityWeb;
      self.view.imgHomebtn.onTouchStart = HamburgerWeb;

      if(data) {
        getCinemasWeb(self, data);
        getFilmsWeb(self, data); 
      } else {
        getCinemasWeb(self, 1);
        getFilmsWeb(self, 1); 
      }
    },

  getRowFilmDataWeb: function (){
    try {
      var self = this;
      var segFilmData = self.view.sgmFilms.data;
      var segFilmIndex = self.view.sgmFilms.selectedRowIndex;

      var chosenFilmId = 
          {
            filmId:segFilmData[segFilmIndex[1]].id,                    
            cinemaId: segFilmData[segFilmIndex[1]].cinemaId,
            cityId: chosenCityId || 1
          };
      var ntf = new kony.mvc.Navigation("frmScreeningsWeb");
      ntf.navigate(chosenFilmId);

    } catch (e) {
      alert("error in getRowFilmDataWeb: " + e.message);
    }
  },

  getRowCinemaDataWeb: function (){
    try {
      var self = this;
      var sgm = self.view.sgmCinemas;
      var segCinemaData = self.view.sgmCinemas.data;
      var segCinemaIndex = self.view.sgmCinemas.selectedRowIndex;

      var chosenCinemaId = segCinemaData[segCinemaIndex[1]].id;
      getFilmsWeb(self,null,chosenCinemaId);

    } catch (e) {
      alert("error in getRowCinemaDataWeb: " + e.message);
    }
  },

  getFilmsByCityWeb: function (){
    try {
      var self = this;
      getFilmsWeb(self, chosenCityId);

    } catch (e) {
      alert("error in getFilmsByCityWeb: " + e.message);
    }
  }
});