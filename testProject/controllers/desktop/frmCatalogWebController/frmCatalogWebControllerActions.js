define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for imgBackBtn **/
    AS_Image_c1b7616259474f638ba5068d99495a58: function AS_Image_c1b7616259474f638ba5068d99495a58(eventobject, x, y) {
        var self = this;
        return HamburgerWeb.call(this);
    },
    /** onClick defined for flxAbout **/
    AS_FlexContainer_i477de7a5c7846b391121bea4166ec55: function AS_FlexContainer_i477de7a5c7846b391121bea4166ec55(eventobject) {
        var self = this;
        return showHideEntryForm.call(this);
    },
});