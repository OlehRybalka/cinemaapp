define({ 
  
  proxy: "http://cors-anywhere.herokuapp.com/",
  
  onNavigate : function (data) {
    if (data) {
      var sessionId = data.id;
      this.clearPreviousScheme();
      this.view.backBtn.onClick = this.goToPreviousForm;
      this.view.errorBtn.onClick = this.goToPreviousForm;
      this.view.comHeader.flxCitiesKA.setVisibility(false);
      this.getScheme(sessionId);
    } else {
      this.showErrorMessage();
    }
  },
  
  getScheme : function (sessionId) {
    var self = this;
    var request = new kony.net.HttpRequest();
    request.onReadyStateChange = schemeResponse;
    request.open(constants.HTTP_METHOD_GET, this.proxy + "http://kino-teatr.ua:8081/services/api/showtime/" + sessionId + "/schema?apiKey=pol1kh111");
    request.send();
    
    function schemeResponse () {
      if (this.readyState == constants.HTTP_READY_STATE_DONE) {
        try {
          var responseIsValid;
          var scheme = JSON.parse(this.response);
          responseIsValid = self.validateResponse(scheme);
          if (responseIsValid) {
            self.view.seatLayout.showScheme.call(self, scheme);
            self.view.mainFlex.forceLayout();
          } else {
            self.showErrorMessage();
          }
        } catch (e) {
          self.showErrorMessage();
        }
      }
    }   
  },
  
  clearPreviousScheme : function () {
    this.view.seatLayout.seatsFlex.removeAt(1);
  },
  
  validateResponse : function (scheme) {
    if (scheme.show_name) {
      return true;
    } else {
      return false;
    }
  },
  
  showErrorMessage : function () {
    this.view.errorFlex.height = "800px";
    this.view.mainFlex.forceLayout();
    this.view.errorFlex.height = "0px";
  },
  
  goToPreviousForm : function () {
	var ntf = new kony.mvc.Navigation("frmScreeningsWeb");
    ntf.navigate();
  }

 });