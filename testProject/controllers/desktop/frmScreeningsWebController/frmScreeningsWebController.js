define({ 

  data: {
    filmId: null,
    cinemaId: undefined,
    cityId: 1,

  },

  proxy: "http://cors-anywhere.herokuapp.com/",

  onNavigate: function(content){
    this.setCalendarToday();
    this.view.calSearchDate.onSelection = this.getScreenings;
    this.view.btnBack.onClick = this.navigateBack;
    this.view.comHeader.lbxCitiesKA.onSelection = this.navigateBack;
    this.view.comHeader.flxCitiesKA.setVisibility(false);
    
    if (content) {
      this.data.filmId = content.filmId;
      this.data.cinemaId = content.cinemaId;
      this.data.cityId = content.cityId;
      this.view.comHeader.lbxCitiesKA.selectedKey = content.cityId;
    }
  },

  setCalendarToday: function() {
    var day = new Date();
    this.view.calSearchDate.date = [day.getDate(),(day.getMonth() + 1),day.getFullYear(),0,0,0];
  },

  getSearchDate: function() {
    var searchDate = "" + this.view.calSearchDate.date;
    return searchDate.split("/").reverse().join("-");
  },

  getFilmInfo: function() {
    var self = this;
    var serviceName = "getCinemas";
    var integrationClient = null;
    var operationName;
    var headers = null;
    var params = {
      filmId: this.data.filmId,
      apiKey: "pol1kh111"
    };
    self.clearData();

    try {
      this.view.imgFilmPoster.setVisibility(true);
      this.view.imgFilmPoster.src = "http://kino-teatr.ua:8081/services/api/film/" + this.data.filmId + "/poster?apiKey=pol1kh111&width=300&height=400&ratio=1";
    } catch (e) {
      alert("Error: " + e.message);
    }


    /** Kony Fabric Services */

    //     try {
    //         integrationClient = kony.sdk.getCurrentInstance().getIntegrationService(serviceName);
    //       } catch (e) {
    //         alert("Error: " + e.message);
    //       }

    //     operationName = "getFilmById";

    //     integrationClient.invokeOperation(operationName, headers, params, success, error);

    //     function success(response) {
    //        self.view.lblFilmTitle.text = this.response.title;   
    //         self.view.lblDurationDescription.text = this.response.duration + " мин.";
    //         self.view.LblRaitingDescription.text = this.response.rating.toFixed(1);
    //         self.view.lblYearDescription.text = "" + this.response.year;
    //         self.view.rtxPlot.text = this.response.description;
    //         for (var i = 0; i < this.response.genres.length; i++) {
    //           self.view.lblGenreDescription.text += this.response.genres[i].name + "\n";
    //         }
    //     }
    //     function error(e){
    //       alert("Integration Service Error");
    //     }

    /** HttpRequest */

    searchData();

    function searchData () {
      try {
        var request = new kony.net.HttpRequest();
        request.onReadyStateChange = httpCallbackHandler;
        request.open(constants.HTTP_METHOD_GET, self.proxy + "http://kino-teatr.ua:8081/services/api/film/" + self.data.filmId + "?apiKey=pol1kh111");
        //     request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        request.send();
      } catch(e) {
        alert("Error: " + e.message);
      }

    }

    function httpCallbackHandler () {
      if (this.readyState == constants.HTTP_READY_STATE_DONE) {
        if (this.response != "Film with id '" + self.data.filmId + "' not found") {
          if (this.response) {
            var resp = JSON.parse(this.response);

            self.view.lblFilmTitle.text = resp.title;

            for (var i = 0; i < resp.genres.length; i++) {
              self.view.lblGenreDescription.text += resp.genres[i].name + "\n";
            }
            self.view.lblDurationDescription.text = "" + resp.duration + " мин.";
            self.view.lblRaitingDescription.text = "" + resp.rating.toFixed(1);
            self.view.lblYearDescription.text = "" + resp.year;
            self.view.rtxPlotDescription.text = resp.description;        
          }
        } else {
          self.filmNotFound();
        }
      }
    }

    self.getScreenings();
    self.getPersons();
  },

  getScreenings: function() {

    var self = this;
    self.view.flxSessions.removeAll();
    var date = self.getSearchDate();
    searchData();
    var request;
    function searchData () {
      try {
        request = new kony.net.HttpRequest();
        request.onReadyStateChange = httpCallbackHandler;
        request.open(constants.HTTP_METHOD_GET, self.proxy + "http://kino-teatr.ua:8081/services/api/city/" + self.data.cityId + "/film/" + 
                     self.data.filmId + "/shows?apiKey=pol1kh111&size=10000&date=" + date + "&detalization=FULL");
        request.send();
      } catch(e) {
        alert("Error: " + e.message);
      }
    }

    function httpCallbackHandler () {
      if (this.readyState == constants.HTTP_READY_STATE_DONE) {
        var warning = createLblTitle(0, "На данную дату по фильму сеансов нет");
        if (this.response.numberOfElements !== 0) {
          if (this.response) {
            var resp = JSON.parse(this.response);
            var screenings = [];
            var cinemaId;
            if (self.data.cinemaId === undefined) {
              for (var i = 0; i < resp.cinemas.length; i++) {
                cinemaId = resp.cinemas[i].id;
                screenings = self.getScreeningsByCinema(screenings, resp, cinemaId);
              }
            } else {
              var cinemaIndex = 0;
              cinemaId = self.data.cinemaId;
              screenings = self.getScreeningsByCinema(screenings, resp, cinemaId);
            }
            if (screenings.length > 0) {
              self.addScreenings(screenings);
            } else {
              self.view.flxSessions.addAt(warning,0);
            }
          }

        } else {
          self.view.flxSessions.addAt(warning,0);
        } 

        self.view.flxSessions.height = kony.flex.USE_PREFERED_SIZE;
        self.view.forceLayout();
      }
    }
  },

  getScreeningsByCinema: function (arr, resp, cinemaId) {
    var sessions = getSessionsByCinema(cinemaId, resp);
    sessions = this.filterAvailableTime(sessions);
    sessions = sessions.sort(sortByTime);

    var cinemaName = getCinemaName(resp.cinemas, cinemaId);

    if (sessions.length > 0) {
      arr.push({
        title: cinemaName,
        sessions: sessions
      });
    }
    return arr;
  },

  addScreenings: function(arr) {
    var screenings = buildScreening(arr);
    for (var i = 0; i < screenings.length; i++) {
      this.view.flxSessions.addAt(screenings[i], i);
      this.view.flxSessions.height = kony.flex.USE_PREFERED_SIZE;
      this.view.forceLayout();
    }
  }, 

  filterAvailableTime: function(arr) {
    var searchDate = this.getSearchDate();
    var availSessions = arr.filter(function(elem) {
      return (+getDateFromStr(searchDate, elem.time) > +new Date());
    });
    return availSessions;
  },

  getPersons: function() {
    var self = this;
    searchData();

    function searchData () {
      var request = new kony.net.HttpRequest();
      request.onReadyStateChange = httpCallbackHandler;
      request.open(constants.HTTP_METHOD_GET, self.proxy + "http://kino-teatr.ua:8081/services/api/film/" + self.data.filmId + "/persons?apiKey=pol1kh111&size=100&detalization=FULL");
      request.send();
    }

    function httpCallbackHandler () {
      if (this.readyState == constants.HTTP_READY_STATE_DONE) {
        if (this.response != "Film with id '" + self.data.filmId + "' not found") {
          var resp = JSON.parse(this.response);
          if (!resp) resp = {};
          resp.persons = resp.persons || [];
          resp.content = resp.content || [];
          resp.professions = resp.professions || [];

          resp = addProfessionId(resp);

          var persons = [];
          var nomProf = 5;
          for (var i = 0; i <= nomProf; i++) {
            var profId = i + 1;
            var personsByProf = filterByProfession(resp.persons, profId) || [];
            personsByProf = formatToSegmentData(personsByProf);
            persons.push(personsByProf);
          }
          self.fillUpPersons(persons);

          self.view.flxFilmDescription.height = kony.flex.USE_PREFERED_SIZE;
          self.view.flxFilmInfo.height = kony.flex.USE_PREFERED_SIZE;
          self.view.flxContent.height = kony.flex.USE_PREFERED_SIZE;
          self.view.flxScreenings.height = kony.flex.USE_PREFERED_SIZE;
          self.view.forceLayout();
        }
      }
    }
  },

  fillUpPersons: function(arr) {
    var lblDescriptions = [
      this.view.segDirectorDescription,
      this.view.segActorsDescription,
      this.view.segScenaristDescription,
      this.view.segOperatorDescription,
      this.view.segComposerDescription,
      this.view.segProducerDescription
    ];

    this.clearPersons(lblDescriptions);

    for (var i = 0; i < lblDescriptions.length; i++) {
      lblDescriptions[i].setData(arr[i]);
    }
  },

  clearData: function() {
    this.view.imgFilmPoster.setVisibility(false);
    this.view.lblFilmTitle.text = "";  
    this.view.lblGenreDescription.text = "";
    this.view.lblDurationDescription.text = "";
    this.view.lblRaitingDescription.text = "";
    this.view.lblYearDescription.text = "";
    this.view.rtxPlotDescription.text = "";
    this.view.lblGenreDescription.text = "";
  },

  clearPersons: function(arr) {
    for (var i = 0; i < arr.length; i++) {
      arr[i].removeAll();
    }
  },

  filmNotFound: function() {
    this.view.lblFilmTitle.text = "К сожалению, фильм не найден.";
  },

  navigateBack: function() {
    var backForm = new kony.mvc.Navigation("frmCatalogWeb");
    backForm.navigate(chosenCityId);
  }

});