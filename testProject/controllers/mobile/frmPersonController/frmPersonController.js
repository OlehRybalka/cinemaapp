define({ 
  
  data: {},

  onNavigate: function(context) {
    var frmPerson = _kony.mvc.GetController("frmPerson", true);
    frmPerson.view.sgmMenu.onRowClick = HamburgerMenuNavigation;
    frmPerson.view.imgHomebtn.onTouchStart = Hamburger;
    frmPerson.view.segFilms.onRowClick = frmPerson.goToFrmChosenFilm;
    
    if (context) {
      this.data.id = context.id;
      this.data.cityId = context.cityId;
      try {
        this.clearData();
        getPersonPhotos(this.data.id);
    	getPersonInfo(this.data.id);
        getFilmsByPerson(this.data.id);
      } catch(e) {
        alert("ERROR : " + e.message);
      }
    }	
  },
  
  fillUpSlider: function(personPhotos) {
    this.view.imgSegmentSlider.setVisibility(true);
    this.view.imgSegmentSlider.setData(personPhotos);
  },
  
  fillUpPersonInfo: function(resp) {
    this.view.lblPersonName.text = resp.first_name + " " + resp.last_name;
    this.view.lblPersonOriginName.text = resp.last_name_orig + " " + resp.first_name_orig;
    this.view.lblBirthday.text = "Дата рождения: " + resp.birthdate;
    this.view.lblRating.text = "Рейтинг: " + resp.rating + "/10";
    this.view.lblVotes.text = "Голосов: " + resp.rating_votes;
    this.view.txtBiography.text = resp.biography;
  },
  
  clearData: function() {
    this.view.imgSegmentSlider.removeAll();
    this.view.lblPersonName.text = "";
    this.view.lblPersonOriginName.text = "";
    this.view.lblBirthday.text = "";
    this.view.lblRating.text = "";
    this.view.lblVotes.text = "";
    this.view.txtBiography.text = "";
  },
  
  personNotFound: function() {
    this.view.lblPersonName.text = "Персона не найдена";
  },
  
  hideSlider: function() {
    this.view.imgSegmentSlider.setVisibility(false);
  },
  
  goToFrmChosenFilm: function() {
    var frm = new kony.mvc.Navigation("frmChosenFilm");
    var data = this.getRowData();
    data.cityId = this.data.cityId;
    frm.navigate(data);
  },
  
  getRowData: function() {
    var segFilmsData = this.view.segFilms.data;
    var segRowIndex = this.view.segFilms.selectedRowIndex;
    var rowIndex = 1;
    return segFilmsData[segRowIndex[rowIndex]];
  }

 });