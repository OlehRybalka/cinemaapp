define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for imgbackbtn **/
    AS_Image_a12ffde51ed84df486b6a2b854517d8b: function AS_Image_a12ffde51ed84df486b6a2b854517d8b(eventobject, x, y) {
        var self = this;
        return Hamburger.call(this);
    },
    /** onRowClick defined for sgmMenu **/
    AS_Segment_f3900770e2104b3a81a93c944c7b90ca: function AS_Segment_f3900770e2104b3a81a93c944c7b90ca(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.menuNavigation.call(this);
    },
    /** init defined for frmCatalog **/
    AS_Form_e6736436b20b408ba64b3cd70f78a48c: function AS_Form_e6736436b20b408ba64b3cd70f78a48c(eventobject) {
        var self = this;
        return self.getCities.call(this);
    },
    /** preShow defined for frmCatalog **/
    AS_Form_dd13822d66fe40939fa35c2fd6bc3b06: function AS_Form_dd13822d66fe40939fa35c2fd6bc3b06(eventobject) {
        var self = this;
        return self.formOnload.call(this);
    }
});