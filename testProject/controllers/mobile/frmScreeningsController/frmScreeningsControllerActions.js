define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for imgbackbtn **/
    AS_Image_a12ffde51ed84df486b6a2b854517d8b: function AS_Image_a12ffde51ed84df486b6a2b854517d8b(eventobject, x, y) {
        var self = this;
        return Hamburger.call(this);
    },
    /** onRowClick defined for sgmCollection **/
    AS_Segment_ddb3ae7fa409484db6a0f3d14e6d8a94: function AS_Segment_ddb3ae7fa409484db6a0f3d14e6d8a94(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.chooseSeanse.call(this);
    },
    /** onRowClick defined for sgmMenu **/
    AS_Segment_b5ded837cdbb48238085a3488c5333ed: function AS_Segment_b5ded837cdbb48238085a3488c5333ed(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.menuNavigation.call(this);
    },
    /** postShow defined for frmScreenings **/
    AS_Form_ab3f4f7283f24db48bdcba3a432d87ab: function AS_Form_ab3f4f7283f24db48bdcba3a432d87ab(eventobject) {
        var self = this;
        return self.fillUpScreenings.call(this);
    }
});