define({ 

    data: {},
    
    onNavigate: function(context){
      var frmScreenings = _kony.mvc.GetController("frmScreenings", true);
      frmScreenings.view.sgmMenu.onRowClick = HamburgerMenuNavigation;
      frmScreenings.view.imgHomebtn.onTouchStart = Hamburger;
  
      if (context) {
        this.data = context;
      }
      this.setCalendarToday();
    },
  
    setCalendarToday: function() {
      var day = new Date();
      this.view.calendarDate.date = [day.getDate(),(day.getMonth() + 1),day.getFullYear(),0,0,0];
    },
  
    getSearchDate: function() {
      var searchDate = "" + this.view.calendarDate.date;
      return searchDate.split("/").reverse().join("-");
    },
    
    getPrevForm: function() {
      var previousForm;
      try {
        previousForm = kony.application.getPreviousForm().id;
      } catch (e) {
        alert("Error: " + e.message);
      }
      return previousForm;
    },
         
    
    fillUpScreenings: function() {
      this.view.txtChosenItemScrTitle.text = "";
      this.view.sgmCollection.removeAll();
      sendRequestHead(this);
    },
    
  
    successFilm: function(response) {
      var apiKey = "pol1kh111";
      filmIndex = 0;
      
      if (response.films.length === 0) {
        this.view.txtChosenItemScrTitle.text = "По данному фильму сеансов нет";
        this.view.txtChosenItemScrTitle.left = "15dp";
      } else {
        this.view.txtChosenItemScrTitle.left = "155dp";
        this.view.txtChosenItemScrTitle.text = response.films[filmIndex].title || null;
        this.view.imgChosenItemScr.isVisible = true;
        this.view.imgChosenItemScr.src = "http://kino-teatr.ua:8081/services/api/film/"+
                                          this.data.id + "/poster?apiKey=" + apiKey + "&width=300&height=400&ratio=1";
      }
    },
         
    successCinema: function(response) {
      cinemaIndex = 0;
      this.view.txtChosenItemScrTitle.left = "15dp";
      if (response.cinemas.length === 0) {
        this.view.txtChosenItemScrTitle.text = "В данном кинотеатре сеансов нет";
      } else {
        this.view.txtChosenItemScrTitle.text = response.cinemas[cinemaIndex].name || null;
      }
    },
    
    
    fillUpSegment: function(response, params) {
      this.view.sgmCollection.removeAll();
      var items;
      if (params.successFunc === "successFilm") {
        this.view.sgmCollection.rowTemplate = "flxScreeningsByFilm";
        items = parseFilmResponse(response);
      } else {
        this.view.sgmCollection.rowTemplate = "flxScreeningsByCinema";
        items = parseCinemaResponse(response);
      } 
      if (items.length > 0) {
        this.view.sgmCollection.setData(items);
      } else {
        this.view.sgmCollection.setData([{
          txtSubitemTitle: "Нет сеансов"
        }]);
      }
       
    },
  
    
    chooseSeanse: function() {
      var chooseSessionForm = new kony.mvc.Navigation("frmChooseSession");
      var data = this.getRowData();
      chooseSessionForm.navigate(data);
      HamburgerFechar();
    },
    
    getRowData: function() {
      var segScreeningsData = this.view.sgmCollection.data;
      var segRowIndex = this.view.sgmCollection.selectedRowIndex;
      var rowIndex = 1;
      return segScreeningsData[segRowIndex[rowIndex]];
    }
    
  
    
  //   showScreening: function() {
  //     var self = this;
  //     var previousForm;
  //     var serviceName = "GetFilmsInfo";
  //     var integrationClient = null;
  //     var operationName;
  //     var headers = null;
  //     var apiKey = "pol1kh111";
  //     this.data.id = "49667";
  //     var id = this.data.id;
      
  
  //     var params = {
  //       id: ""+self.data.id,
  //       apiKey: apiKey,
  //       city: "1",
  //       date: "2018-11-16"
  //     };
  //     var success;
  
  //     try {
  //       previousForm = kony.application.getPreviousForm().id;
  //     } catch (e) {
  //       alert("Error: " + e.message);
  //     }
  
  //     try {
  //       integrationClient = kony.sdk.getCurrentInstance().getIntegrationService(serviceName);
  //     } catch (e) {
  //       alert("Error: " + e.message);
  //     }
  // 	operationName = self.getOperationName(previousForm).operationName;
  //     success = getOperationName(previousForm).successFunc;
      
  //     integrationClient.invokeOperation(operationName, headers, params, success, error);
      
    
  //     this.successFilm(response);
  //   },
    
    
  //   getOperationName: function(prevform) {
  //     var params = {
  //       operationName: null,
  //       successFunc: null
  //     };
  //       if (previousForm === "frmChosenFilm") {
  //         params.operationName = "getScreeningByFilmAndDate";
  //         params.success = this.successFilm;
  //       } else {
  //         params.operationName = "getScreeningsByCinemaAndDate";
  //         params.success = this.successCinema;
  //       }
  //       return params;
  //     },
  
  //   successFilm: function(response) {
  //     if (response.films.length === 0) {
  //       this.view.txtChosenItemScrTitle.text = "По данному фильму сеансов нет";
  //     } else {
  //       this.view.txtChosenItemScrTitle.text = response.films[0].title || null;
  //       this.view.imgChosenItemScr.src = "http://kino-teatr.ua:8081/services/api/film/"+
  //         id + "/poster?apiKey=" + apiKey + "&width=300&height=400&ratio=1";
  
  //       var items = this.parseFilmResponse(response);  
  //       this.view.sgmCollection.setData(items);
  //     }
  //   },
  
  //   successCinema: function(response) {
  //     if (response.cinemas.length === 0) {
  //       this.view.txtChosenItemScrTitle.text = "В данном кинотеатре сеансов нет";
  //     } else {
  //       this.view.txtChosenItemScrTitle.text = response.cinemas[0].name || null;
  //       this.view.imgChosenItemScr.isVisible = false;
  
  //       var items = this.parseCinemaResponse(response);  
  //       this.view.sgmCollection.setData(items);
  //     }
  //   },
  
  //   error: function(e){
  //     alert("Integration Service Error");
  //   },
  
  
  //   parseFilmResponse: function(resp) {
  //     var data = [];
  //     for (var c = 0; c < resp.cinemas.length; c++) {
  // 	alert("resp.cinemas[c].name: " +resp.cinemas[c].name);
  //       data.push({
          
  //         txtSubitemTitle: resp.cinemas[c].name || null,
  //         txtTime: this.getSessionsByCinema(resp.cinemas[c].id, resp) || null
  //       });
  //     }
  //     return data;
  //   },
  
  //   getSessionsByCinema: function(sinemaId, resp) {
  //     var sessions = [];
  //     var halls = this.getHalls(resp.halls, sinemaId);
  //     for (var k = 0; k < halls.length; k++) {
       
  //       sessions = sessions.concat(this.getSessionsByHalls(resp.content, halls[k]));
  //     }
  //     var times = sessions.map(function(t){
  //       return t.time.slice(0,5);
  //     });
  //     times = times.join(" ");
  //     return times;
  //   },
  
  //   getHalls: function(arr, id) {
  //     arr = arr.filter(function(elem) {
  //       return elem.cinema_id == id;
  //     });
  //     var hallsList = arr.map(function(elem) {
  //       return elem.id;
  //     });
  //     return hallsList;
  //   },
  
  //   getSessionsByHalls: function(arr, hall) {
  
  //     var filterHall = function(h){ 
  //       return h.hall_id == hall; 
  //     };
  
  //     var mapTimes = function(t){
  //       return {
  //         time: t.time,
  //         id: t.id,
  //         prices: t.prices
  //       };
  //     };
  
  //     var concatTimes = function(timeList, h){ 
  //       return timeList.concat(h.times.map(mapTimes)); 
  //     };
  
  //     var timesByHall = arr.filter(filterHall);
  //     var timeListByHall = timesByHall.reduce(concatTimes, []);
  //     return timeListByHall;
  //   },
  
  
  //   parseCinemaResponse: function(resp) {
  //     var data = [];
  //     for (var c = 0; c < resp.films.length; c++) {
  //       var filmId = resp.films[c].id;
  //       data.push({
  //         txtSubitemTitle: resp.films[c].title || null,
  //         imgSubitem: {src:"http://kino-teatr.ua:8081/services/api/film/"+
  //                      resp.films[c].id + "/poster?apiKey=" + apiKey + "&width=300&height=400&ratio=1"},
  //         txtTime: this.getSessionsByFilms(resp.films[c].id, resp.content) || null
  //       });
  //     }
  //     return data;
  //   },
  
  //   getSessionsByFilms: function(filmId, resp) {
  //     var films = resp.filter(function(elem) {
  //       return elem.film_id == filmId;
  //     });
  //     var times = "";
  //     for (var k = 0; k < films.length; k++) {
  //       for (var l = 0; l < films[k].times.length; l++) {
  //         times = times + films[k].times[l].time.slice(0,5) +" ";
  //       }
  //     }
  //    return times;
  //   }
    
  // data: {},
  
  //   showScreening: function() {
  //     var self = this;
  //     var previousForm;
  //     var serviceName = "GetFilmsInfo";
  //     var integrationClient = null;
  //     var operationName;
  //     var headers = null;
  //     var apiKey = "pol1kh111";
  //     this.data.id = "49667";
  // //     this.data.id = "335";
  //     this.data.apiKey = "pol1kh111";
  //     var id = this.data.id;
  //     var success;
      
     
      
  //     var currday = this.view.FlxContChosenItem.calendarDate.day;
  
  //     alert("Calendar day ::"+this.view.FlxContChosenItem.calendarDate.date); 
      
  //     this.successFilm(responseFilm);
  // //     this.successCinema(responseCinema);
  //   },
    
    
  //   getOperationName: function(prevform) {
  //     var params = {
  //       operationName: null,
  //       successFunc: null
  //     };
  //       if (previousForm === "frmChosenFilm") {
  //         params.operationName = "getScreeningByFilmAndDate";
  //         params.success = this.successFilm;
  //       } else {
  //         params.operationName = "getScreeningsByCinemaAndDate";
  //         params.success = this.successCinema;
  //       }
  //       return params;
  //     },
  
  //   successFilm: function(response) {
  //     if (response.films.length === 0) {
  //       this.view.txtChosenItemScrTitle.text = "По данному фильму сеансов нет";
  //     } else {
  //       this.view.txtChosenItemScrTitle.text = response.films[0].title || null;
  //       this.view.imgChosenItemScr.src = "http://kino-teatr.ua:8081/services/api/film/"+
  //         this.data.id + "/poster?apiKey=" + this.data.apiKey + "&width=300&height=400&ratio=1";
  
  //       var items = this.parseFilmResponse(response);
        
  //       self.view.sgmCollection.setData(items);
  //     }
  //   },
  
  //   successCinema: function(response) {
  //     if (response.cinemas.length === 0) {
  //       this.view.txtChosenItemScrTitle.text = "В данном кинотеатре сеансов нет";
  //     } else {
  //       this.view.txtChosenItemScrTitle.left = "10dp";
  //       this.view.txtChosenItemScrTitle.text = response.cinemas[0].name || null;
  //       this.view.imgChosenItemScr.isVisible = false;
  
  //       var items = this.parseCinemaResponse(response);  
  //       this.view.sgmCollection.setData(items);
  //     }
  //   },
  
  //   error: function(e){
  //     alert("Integration Service Error");
  //   },
  
  
  //   parseFilmResponse: function(resp) {
  //     var data = [];
  //     for (var c = 0; c < resp.cinemas.length; c++) {
  //       data.push({
  //         txtSubitemTitle: resp.cinemas[c].name || null,
  //         txtTime: this.getSessionsByCinema(resp.cinemas[c].id, resp) || null
  //       });
  //     }
  //     alert(data[0].txtSubitemTitle);
  //     return data;
  //   },
  
  //   getSessionsByCinema: function(sinemaId, resp) {
  //     var sessions = [];
  //     var halls = this.getHalls(resp.halls, sinemaId);
  //     for (var k = 0; k < halls.length; k++) {
       
  //       sessions = sessions.concat(this.getSessionsByHalls(resp.content, halls[k]));
  //     }
  //     var times = sessions.map(function(t){
  //       return t.time.slice(0,5);
  //     });
  //     times = times.join(" ");
  //     return times;
  //   },
  
  //   getHalls: function(arr, id) {
  //     arr = arr.filter(function(elem) {
  //       return elem.cinema_id == id;
  //     });
  //     var hallsList = arr.map(function(elem) {
  //       return elem.id;
  //     });
  //     return hallsList;
  //   },
  
  //   getSessionsByHalls: function(arr, hall) {
  
  //     var filterHall = function(h){ 
  //       return h.hall_id == hall; 
  //     };
  
  //     var mapTimes = function(t){
  //       return {
  //         time: t.time,
  //         id: t.id,
  //         prices: t.prices
  //       };
  //     };
  
  //     var concatTimes = function(timeList, h){ 
  //       return timeList.concat(h.times.map(mapTimes)); 
  //     };
  
  //     var timesByHall = arr.filter(filterHall);
  //     var timeListByHall = timesByHall.reduce(concatTimes, []);
  //     return timeListByHall;
  //   },
  
  
  //   parseCinemaResponse: function(resp) {
  //     var data = [];
  //     for (var c = 0; c < resp.films.length; c++) {
  //       var filmId = resp.films[c].id;
  //       data.push({
  //         txtSubitemTitle: resp.films[c].title || null,
  //         imgSubitem: {src:"http://kino-teatr.ua:8081/services/api/film/"+
  //                      resp.films[c].id + "/poster?apiKey=" + apiKey + "&width=300&height=400&ratio=1"},
  //         txtTime: self.getSessionsByFilms(resp.films[c].id, resp.content) || null
  //       });
  //     }
  //     return data;
  //   },
  
  //   getSessionsByFilms: function(filmId, resp) {
  //     var films = resp.filter(function(elem) {
  //       return elem.film_id == filmId;
  //     });
  //     var times = "";
  //     for (var k = 0; k < films.length; k++) {
  //       for (var l = 0; l < films[k].times.length; l++) {
  //         times = times + films[k].times[l].time.slice(0,5) +" ";
  //       }
  //     }
  //    return times;
  //   },
    
    
    
  
  });