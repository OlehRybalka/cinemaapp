define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onSelection defined for ListBoxCities **/
    AS_ListBox_gb7393c879bd4e969bd4c7365ded87d3: function AS_ListBox_gb7393c879bd4e969bd4c7365ded87d3(eventobject) {
        var self = this;
        return self.getListBox.call(this);
    },
    /** onClick defined for dtnSignUp **/
    AS_Button_cbc731501c90450b90180a66efd3ba5a: function AS_Button_cbc731501c90450b90180a66efd3ba5a(eventobject) {
        var self = this;
        return self.sendNewUser.call(this);
    },
    /** onClick defined for txtisRegistered2 **/
    AS_RichText_fa78636225b647d497993e4afd677f5e: function AS_RichText_fa78636225b647d497993e4afd677f5e(eventobject, linktext, attributes) {
        var self = this;
        return self.getCinema.call(this);
    },
    /** init defined for frmSignUp **/
    AS_Form_a9d944e3f6e1447aa475d6f04c948587: function AS_Form_a9d944e3f6e1447aa475d6f04c948587(eventobject) {
        var self = this;
        return self.getCities.call(this);
    }
});