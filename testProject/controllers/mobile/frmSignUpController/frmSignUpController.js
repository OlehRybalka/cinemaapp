define({
  // Registration with remote database
  data : {},  
  user : {},
  navigateData : {},
  
  onNavigate: function() {

    this.view.fldEmail.onEndEditing = this.validateEmail;
    this.view.fldPassword.onEndEditing = this.validatePassword;
    this.view.txtisRegistered2.onTouchEnd = this.navigateToSignIn;
  },
  
  validateEmail: function() {
    var email = this.view.fldEmail.text;
    if (validateRegexEmail(email)) {
      this.view.lblEmailWarning.text = "";
      return true;
    } else if (this.view.fldEmail.text === "") {
      this.view.lblEmailWarning.text = "";
    } else {
      this.view.lblEmailWarning.text = "You have entered incorrect E-mail";
    }
  },
  
  validatePassword: function() {
    var password = this.view.fldPassword.text;
    if (validateRegexPassword(password)) {
      this.view.lblPasswordWarning.text = "";
      return true;
    } else if (this.view.fldPassword.text === "") {
      this.view.lblPasswordWarning.text = "";
    } else {
      this.view.lblPasswordWarning.text = "Password must have 6-15 symbols : at least one number, at least one capital letter and at least one lowercase letter";
    }
  },

  sendNewUser : function(){  
    try {
      var self = this;
      var validateResult = this.validate();    
      if(validateResult.status === 'true'){
        this.data.email = validateResult.email;
        this.data.password = validateResult.password;
        this.data.name = validateResult.name;
        this.data.city = parseInt(validateResult.cityId);     
        searchData(this.data);

      }

      function searchData(data){          

        var request = new kony.net.HttpRequest();
        request.onReadyStateChange=httpCallbackHandler;
        request.open(constants.HTTP_METHOD_POST, "https://cityflora.com.ua/connect.php");
        request.setRequestHeader("Content-Type", "application/json");
        request.setRequestHeader('charset','utf-8');
        var sendData = JSON.stringify(data);      
        request.send(sendData);
      }

      function httpCallbackHandler(){
        if(this.readyState == constants.HTTP_READY_STATE_DONE){        
          if(this.response === 'false'){
            alert('This user already exist');
          }else if(this.response){             
            var resp = JSON.parse(this.response);
            var navData = {};          
            navData.userId = resp.id;
            navData.cityId = resp.city;						
            var nav = new kony.mvc.Navigation('frmCatalog');
            nav.navigate(navData);     
          }
        }
      }
    } catch (e) {     
    }
  },

  getCities: function () {
    var data;
    var self = this;
    function searchData(){
      var request = new kony.net.HttpRequest();
      request.onReadyStateChange=httpCallbackHandler;
      request.open(constants.HTTP_METHOD_GET, "http://kino-teatr.ua:8081/services/api/cities?apiKey=pol1kh111&size=100");
      request.setRequestHeader('Content-type','application/json');
      request.send();
    }

    function httpCallbackHandler(){
      if(this.readyState == constants.HTTP_READY_STATE_DONE){   
        var setDataList = [];

        data = this.response.content;       

        for (var i = 0; i < data.length; i++) {
          var keyList = [data[i].id, data[i].name];
          setDataList.push(keyList);
        }
        self.view.ListBoxCities.masterData = setDataList;         
      }
    }
    searchData();   

  },

  getListBox: function () {
    try {
      return this.view.ListBoxCities.selectedKeyValue;
    } catch (err) {
      return null;
    }
  },

  validate : function(){
    var city = this.getListBox();
    
    this.user.name = this.view.fldName.text;
    this.user.email = this.view.fldEmail.text;
    this.user.password = this.view.fldPassword.text;
    if(city !== null){
      this.user.cityId = city[0];
      this.user.cityName = city[1];
    }

    if (!validateRegexEmail(this.user.email) || !validateRegexPassword(this.user.password)) {
      
      return alert('Please fill up correct email and password');
    } else if (this.user.email === null || this.user.password === null || this.user.name === null) {

      return alert('Please fill all rows');     
    } else if (city === null) {

      return alert('Please choose city');
    }    
    this.user.status = 'true';    
    return this.user;    
  },
  
  navigateToSignIn: function() {
    var signInForm = new kony.mvc.Navigation("frmSignIn");
    signInForm.navigate();
  }

  // Registration with Kony data storage

  //   data : {},
  //   getCities: function () {
  //     var data;
  //     var self = this;
  //     function searchData(){
  //       var request = new kony.net.HttpRequest();
  //       request.onReadyStateChange=httpCallbackHandler;
  //       request.open(constants.HTTP_METHOD_GET, "http://kino-teatr.ua:8081/services/api/cities?apiKey=pol1kh111&size=100");
  //       request.setRequestHeader('Content-type','application/json');
  //       request.send();
  //     }

  //     function httpCallbackHandler(){
  //       if(this.readyState == constants.HTTP_READY_STATE_DONE){   
  //         var setDataList = [];

  //          data = this.response.content;
  //          alert(data);

  //         for (var i = 0; i < data.length; i++) {
  //           var keyList = [data[i].id, data[i].name];
  //           setDataList.push(keyList);
  //         }
  //         self.view.ListBoxCities.masterData = setDataList; 

  //       }
  //     }
  //     searchData();

  //   },   

  //   getNewUser : function(){
  //     if(!this.validate()){
  //       kony.ds.save([ kony.crypto.createHash('sha256', this.data.password), this.data.city], this.data.email);    
  //       var nav = new kony.mvc.Navigation('frmCatalog');
  //       alert('Registration succesfull!');
  //       nav.navigate([this.view.ListBoxCities.masterData, this.data.city]);
  //     } else {
  //       return false;
  //     }
  //   },

  //   getListBox: function () {
  //     try {
  //       return this.view.ListBoxCities.selectedKeyValue;
  //     } catch (err) {
  //       return null;
  //     }
  //   },

  //   validate : function(){
  //     this.data.email = this.view.fldEmail.text;
  //     this.data.password = this.view.fldPassword.text;  
  //     this.data.city = this.getListBox();

  //     if (!this.view.chkAgreement.selectedKeyValues) {

  //       return alert('You need agree with Terms of Usage');      
  //     } else if(this.data.email === null || this.data.password === null) {

  //       return alert('Please fill all rows');     
  //     } else if (this.data.city === null) {

  //       return alert('Please choose city');
  //     }

  //     return false;    
  //   }





  // Connect to Kony Fabric Service

  //   getNewUser: function () {
  //     var data = {};
  //     var flag = true;
  //     data.email = this.view.fldEmail.text;
  //     data.password = this.view.fldPassword.text;
  //     data.city = this.getListBox();
  //     if (data.city === null) {
  //       return alert('Please choose city');
  //     }

  //     if (this.view.chkAgreement.selectedKeyValues) {

  //       if (data.email === null || data.password === null) {
  //         flag = false;
  //         return alert('please fill all rows');
  //       }

  //       var service = kony.sdk.getCurrentInstance().getIntegrationService("connectDb");
  //       service.invokeOperation(
  //         'cinemadb_users_create',
  //         null,
  //         data,

  //         function (response) {
  //           if (response.opstatus === 0) {
  //             var nav = new kony.mvc.Navigation('frmCatalog');
  //             nav.navigate();
  //           }
  //         },

  //         function (e) {
  //           alert('error' + e);
  //         });

  //     } else {
  //       alert('You have to agree with Terms of Usage');

  //     }
  //   },
  //   getCities: function () {
  //     var self = this;
  //     var service = kony.sdk.getCurrentInstance().getIntegrationService("CinemaApp");
  //     service.invokeOperation(
  //       'getCities',
  //       null,
  //       null,
  //       function (response) {
  //         var setDataList = [];
  //         var importantData = response.images;
  //         var data = response.content;

  //         for (var i = 0; i < data.length; i++) {
  //           var keyList = [data[i].id, data[i].name];
  //           setDataList.push(keyList);
  //         }

  //         self.view.ListBoxCities.masterData = setDataList;
  //       },
  //       function (error) {
  //         alert("Error: " + JSON.stringify(error));
  //       });
  //   },

  //   haveAccount: function () {
  //     var nav = new kony.mvc.Navigation('frmSignIn');
  //     nav.navigate();
  //   },

  //   getListBox: function () {
  //     try {
  //       return this.view.ListBoxCities.selectedKey[0];
  //     } catch (err) {
  //       return null;
  //     }
  //   }
});