define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for imgbackbtn **/
    AS_Image_a12ffde51ed84df486b6a2b854517d8b: function AS_Image_a12ffde51ed84df486b6a2b854517d8b(eventobject, x, y) {
        var self = this;
        return Hamburger.call(this);
    },
    /** onRowClick defined for sgmMenu **/
    AS_Segment_dd659c5d2dbf4d0e9632c85cbf3ffc1d: function AS_Segment_dd659c5d2dbf4d0e9632c85cbf3ffc1d(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.menuNavigation.call(this);
    }
});