define({ 
  
  data: {},

  onNavigate: function(context) {
     if (context) {
      this.data = context;
      try {
        this.clearData();
        this.fillUpTitles();
        this.fillUpSessions();
        } catch(e) {
        alert("ERROR : " + e.message);
      }
    }
    //this.view.sgmntSessions.onRowClick = this.goToSeats();
    
    var frmChooseSession = _kony.mvc.GetController("frmChooseSession", true);
    frmChooseSession.view.sgmMenu.onRowClick = HamburgerMenuNavigation;
    frmChooseSession.view.imgHomebtn.onTouchStart = Hamburger;
    this.view.sgmntSessions.onRowClick = this.goToSeats;
  },
  
  fillUpTitles: function() {
    this.view.flxContSeanseTitle.txtTitle.text = this.data.title;
    this.view.flxContSeanseTitle.txtSutitle.text = this.data.txtSubitemTitle;
  },
  
  fillUpSessions: function() {
    var sessions = [];
    var threeD = "3d";
    for (var i = 0; i < this.data.sessions.length; i++) {
      var elem = this.data.sessions[i];
      var threeDParam = (elem[threeD] ? "3d" : "");
      sessions.push({
        lblTime: elem.time.slice(0,5),
        lblHall: elem.hallName,
        lbl3D: threeDParam,
        lblPrices: elem.prices,
        sessionId: elem.id
      });
    }
    this.view.sgmntSessions.setData(sessions);
    
  },
  
  goToSeats: function() {
    try {
    var seatsForm = new kony.mvc.Navigation("frmSeats");
    var data = this.getRowData();
    seatsForm.navigate(data.sessionId);
    } catch (e) {
      alert("error:" + e.message);
    }
  },

  getRowData: function() {
    try {
    var segSessionsData = this.view.sgmntSessions.data;
    var segRowIndex = this.view.sgmntSessions.selectedRowIndex;
    var rowIndex = 1;
    return segSessionsData[segRowIndex[rowIndex]];
    } catch (e) {
      alert("error: " + e.message);
    }
  },
  
  clearData: function() {
    this.view.flxContSeanseTitle.txtTitle.text = "";
    this.view.flxContSeanseTitle.txtSutitle.text = "";
    this.view.sgmntSessions.removeAll();
  }
  

 });