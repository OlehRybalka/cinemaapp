define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for imgbackbtn **/
    AS_Image_a12ffde51ed84df486b6a2b854517d8b: function AS_Image_a12ffde51ed84df486b6a2b854517d8b(eventobject, x, y) {
        var self = this;
        return Hamburger.call(this);
    },
    /** onRowClick defined for sgmCinema **/
    AS_Segment_i4dc18eb42c34c7e91d776b40b626acb: function AS_Segment_i4dc18eb42c34c7e91d776b40b626acb(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.segClick.call(this);
    },
    /** onRowClick defined for sgmMenu **/
    AS_Segment_h10bf52c9a044c5b929a8590c552917c: function AS_Segment_h10bf52c9a044c5b929a8590c552917c(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.menuNavigation.call(this);
    }
});