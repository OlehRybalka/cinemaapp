define({ 

//   data : {},
//   error : {},
  
//   onNavigate: function (context) {
//     if (context) {
//       this.data.id = context;
//       var serviceName = "getCinemas";
//       var getFilm = "getFilmById";
//       var getActors = "getActorsByFilmId";
//       var getImagesId = "getFilmImagesId";
//       var integrationClient = null;
//       var params = {
//         filmId: context,
//         apiKey: "pol1kh111"
//       };
//       var imgParams = {
//         imgId: null,
//         apiKey: "pol1kh111"
//       };

//       try {
//         integrationClient = kony.sdk.getCurrentInstance().getIntegrationService(serviceName);
//         integrationClient.invokeOperation(getFilm, null, params, correctFilmResponse, errorResponse);
//         integrationClient.invokeOperation(getActors, null, params, correctActorsResponse, errorResponse);
//         integrationClient.invokeOperation(getImagesId, null, params, correctImgIdResponse, errorResponse);
//       } catch (e) {
//         alert("Error: " + e.message);
//       }
//     }
    
	
//     function correctFilmResponse (response) {
//       var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
//       var i;      
//       frmChosenFilm.view.genreDescription.text = "";
//       frmChosenFilm.view.filmTitleLabel.text = response.title;   
//       frmChosenFilm.view.durationDescription.text = response.duration;
//       frmChosenFilm.view.ratingDescription.text = response.rating;
//       frmChosenFilm.view.yearDescription.text = response.year;
//       frmChosenFilm.view.plotDescription.text = response.description;
//       for (i = 0; i < response.genre.length; i++) {
//         frmChosenFilm.view.genreDescription.text += response.genre[i] + "\n";
//       }
//     }
    
//     function correctActorsResponse (response) {
//       var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
//       frmChosenFilm.view.actorsDescription.text = "";
//       var i;
//       for (i = 0; i < response.persons.length; i++) {
//         frmChosenFilm.view.actorsDescription.text += response.persons[i].first_name + " " + response.persons[i].last_name + "\n";
//       }
//     }
    
//     function correctImgIdResponse (response) {
//       var i;
//       var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
//       frmChosenFilm.view.imgSegmentSlider.removeAll();
//       for(i = 0; i < response.numberOfElements; i++) {
//         imgParams.imgId = response.content[i].id;
//         frmChosenFilm.view.imgSegmentSlider.addDataAt({
//           filmImg: {src: "http://kino-teatr.ua:8081/services/api/film/image/" + imgParams.imgId + "?apiKey=" + imgParams.apiKey + "&width=300&height=400&ratio=1"}
//         }, i);
//       }
//     }
    
//     function errorResponse (error) {
//       this.error.name = "Response Error";
//       this.error.message = error.message;
//       alert(this.error.name + " : " + this.error.message);
//     }   
//   },
  
//   clearView: function () {
//     var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);   
//   },
  
//   goToScreenings: function () {
//     var ntf = new kony.mvc.Navigation("frmScreenings");
// 	ntf.navigate(this.data);
//   },
  
//   goBack: function () {
//     var ntf = new kony.mvc.Navigation("frmCatalog");
//     ntf.navigate();
//   }
  
  data : {},
  
  onNavigate : function (context) {
    var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
    frmChosenFilm.view.sgmMenu.onRowClick = HamburgerMenuNavigation;
    frmChosenFilm.view.imgHomebtn.onTouchStart = Hamburger;
    frmChosenFilm.view.ticketsBtn.onClick = frmChosenFilm.goToScreenings;
//     frmChosenFilm.hidePersonsInfo();
    
    
    var segments = {
        directorLbl:  "sgmntDirector",
        actorsLbl:    "sgmntActors",
        scenaristLbl: "sgmntScenarist",
        operatorLbl:  "sgmntOperator",
        composerLbl:  "sgmntComposer",
        producerLbl:  "sgmntProducer"
    };
    
    Object.keys(segments).forEach(function(elem) {
      
      var segment = frmChosenFilm.view[segments[elem]];
      
      segment.setVisibility(false);
      
      frmChosenFilm.view[elem].onTouchEnd = function() {
        segment.setVisibility(!segment.isVisible);
      };
      
      segment.onRowClick = function() {
        frmChosenFilm.goToPerson(segment);
      };
    });
    
//     var showHideDirectorInfo = function() {frmChosenFilm.showHidePersonInfo(frmChosenFilm.view.sgmntDirector);};
//     var showHideActorsInfo = function() {frmChosenFilm.showHidePersonInfo(frmChosenFilm.view.sgmntActors);};
//     var showHideScenaristInfo = function() {frmChosenFilm.showHidePersonInfo(frmChosenFilm.view.sgmntScenarist);};
//     var showHideOperatorInfo = function() {frmChosenFilm.showHidePersonInfo(frmChosenFilm.view.sgmntOperator);};
//     var showHideComposerInfo = function() {frmChosenFilm.showHidePersonInfo(frmChosenFilm.view.sgmntComposer);};
//     var showHideProducerInfo = function() {frmChosenFilm.showHidePersonInfo(frmChosenFilm.view.sgmntProducer);};

//     frmChosenFilm.view.directorLbl.onTouchEnd = showHideDirectorInfo;
//     frmChosenFilm.view.actors.onTouchEnd = showHideActorsInfo;
//     frmChosenFilm.view.scenaristLbl.onTouchEnd = showHideScenaristInfo;
//     frmChosenFilm.view.operatorLbl.onTouchEnd = showHideOperatorInfo;
//     frmChosenFilm.view.composerLbl.onTouchEnd = showHideComposerInfo;
//     frmChosenFilm.view.producerLbl.onTouchEnd = showHideProducerInfo;
    
//     var navigateToDirectorInfo = function() {frmChosenFilm.goToPerson(frmChosenFilm.view.sgmntDirector);};
//     var navigateToActorInfo = function() {frmChosenFilm.goToPerson(frmChosenFilm.view.sgmntActors);};
//     var navigateToScenaristInfo = function() {frmChosenFilm.goToPerson(frmChosenFilm.view.sgmntScenarist);};
//     var navigateToOperatorInfo = function() {frmChosenFilm.goToPerson(frmChosenFilm.view.sgmntOperator);};
//     var navigateToComposerInfo = function() {frmChosenFilm.goToPerson(frmChosenFilm.view.sgmntComposer);};
// 	var navigateToProducerInfo = function() {frmChosenFilm.goToPerson(frmChosenFilm.view.sgmntProducer);};

    
//     frmChosenFilm.view.sgmntDirector.onRowClick = navigateToDirectorInfo;
//     frmChosenFilm.view.sgmntActors.onRowClick = navigateToActorInfo;
//     frmChosenFilm.view.sgmntScenarist.onRowClick = navigateToScenaristInfo;
//     frmChosenFilm.view.sgmntOperator.onRowClick = navigateToOperatorInfo;
//     frmChosenFilm.view.sgmntComposer.onRowClick = navigateToComposerInfo;
//     frmChosenFilm.view.sgmntProducer.onRowClick = navigateToProducerInfo;

    
    if (context) {
      this.data.id = context.filmId;
      this.data.cityId = context.cityId;
    }
    try {
      this.clearView();
      getFilmInfo(this.data.id);
      getFilmImages(this.data.id);
      getActors(this.data.id);
    } catch (e) {
      alert("ERROR : " + e.message);
    }
  },
  
  hidePersonsInfo: function() {
    this.view.sgmntDirector.setVisibility(false);
    this.view.sgmntActors.setVisibility(false);
    this.view.sgmntScenarist.setVisibility(false);
    this.view.sgmntOperator.setVisibility(false);
    this.view.sgmntComposer.setVisibility(false);
    this.view.sgmntProducer.setVisibility(false);
  },
  
  fillUpPersons: function(arr) {
    var personDescriptions = [
      this.view.sgmntDirector,
      this.view.sgmntActors,
      this.view.sgmntScenarist,
      this.view.sgmntOperator,
      this.view.sgmntComposer,
      this.view.sgmntProducer
    ];
    
    for (var i = 0; i < arr.length; i++) {
      personDescriptions[i].setData(arr[i]);
    }
//     this.view.sgmntDirector.setData(director);
//     this.view.sgmntActors.setData(actors);
//     this.view.sgmntScenarist.setData(scenarist);
//     this.view.sgmntOperator.setData(operator);
//     this.view.sgmntComposer.setData(composer);
//     this.view.sgmntProducer.setData(producer);
  },
  
  showHidePersonInfo: function(segment) {
    var status = segment.isVisible;
    if (status === true) {
      segment.setVisibility(false);
    } else {
      segment.setVisibility(true);
    }
  },
  
  clearView : function () {
    var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true); 
    frmChosenFilm.view.genreDescription.text = "";
    frmChosenFilm.view.filmTitleLabel.text = "";   
    frmChosenFilm.view.durationDescription.text = "";
    frmChosenFilm.view.ratingDescription.text = "";
    frmChosenFilm.view.yearDescription.text = "";
    frmChosenFilm.view.plotDescription.text = "";
    frmChosenFilm.view.actorsDescription.text = "";
    frmChosenFilm.view.imgSegmentFlex.setVisibility(false);
  },
  
  goToScreenings: function () {
    var ntf = new kony.mvc.Navigation("frmScreenings");
	ntf.navigate(this.data);
  },
  
  goToPerson: function(segment) {
    var ntf = new kony.mvc.Navigation("frmPerson");
    var data = this.getRowData(segment);
    if (data.lblPersoneName !== "Данных нет") {
      ntf.navigate(data);
    }
  },
  
  getRowData: function(segment) {
    var segPersonData = segment.data;
    var segRowIndex = segment.selectedRowIndex;
    var rowIndex = 1;
    return segPersonData[segRowIndex[rowIndex]];
  },
  
  goBack: function () {
    var ntf = new kony.mvc.Navigation("frmCatalog");
    ntf.navigate();
  }
  
});