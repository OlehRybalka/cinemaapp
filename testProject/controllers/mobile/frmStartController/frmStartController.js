define({ 

  onNavigate: function() {
    this.view.btnTest.onClick = this.goToTests;
  },

  showSignUp : function(){
    var nav = new kony.mvc.Navigation('frmSignUp');
	nav.navigate();
  },
  showSignIn : function(){
    var nav = new kony.mvc.Navigation('frmSignIn');
	nav.navigate();
  },
  
  goToTests: function() {
    var nav = new kony.mvc.Navigation('frmTests');
	nav.navigate(); 
  }
  

 });