define({ 
  
  data: {},
  
  onNavigate : function (context) {
    try {
      this.data = context;
      var frmSeats = _kony.mvc.GetController("frmSeats", true);
      this.view.goBackLbl.onTouchStart = this.goBack;   
      if (kony.os.deviceInfo().name == "android" || kony.os.deviceInfo().name == "android Simulator") {
        getSchemeForAndroid(this.data);
      } else {
        getSchemeForIOS(this.data);
      }     
    } catch (e) {
      alert("error in onNavigate: " + e.message);
    }
  },

  goBack: function () {
    try {
    var ntf = new kony.mvc.Navigation("frmChooseSession");
    ntf.navigate();
    } catch (e) {
      alert("error in goBack : " + e.message);
    } 
  }
       
});